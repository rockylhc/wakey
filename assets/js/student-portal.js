jQuery(function($) {
    $(function() {

        $('#video-sidenav,#slide-sidenav').iptOffCanvas({closeOnClickOutside: true});

        $('#video-sidenav').on('opened.video-sidenav@iptOffCanvas', function() {
            $('[data-toggle="tooltip"]').tooltip('hide');
            $('#sticky-icon').addClass('on');
        });
        $('#video-sidenav').on('closed.video-sidenav@iptOffCanvas', function() {
            $('[data-toggle="tooltip"]').tooltip('hide');
            $('#sticky-icon').removeClass('on');
        });

        $('#slide-sidenav').on('opened.slide-sidenav@iptOffCanvas', function() {
            $('[data-toggle="tooltip"]').tooltip('hide');
            $('#sticky-icon').addClass('on');
        });

        $('#slide-sidenav').on('closed.slide-sidenav@iptOffCanvas', function() {
            $('[data-toggle="tooltip"]').tooltip('hide');
            $('#sticky-icon').removeClass('on');
        });

        $('[data-toggle="tooltip"]').tooltip();

        $('.nav-toggle').click(function(evt){
            evt.preventDefault();
        });

        $('.disabled').click(function(evt){
            return false;
        });

        $('.with-hover').hover(
            function(){
                var original = $(this).attr('src');
                $(this).attr('src', original.replace('.svg','-hover.svg?v=1244'));
            },
            function(){
                var original = $(this).attr('src');
                $(this).attr('src', original.replace('-hover.svg','.svg'));
            }
        );

        $('#slide-sidenav').on('click touchstart','a', function(evt){
            evt.preventDefault();
            $('#slide-sidenav').find('li').removeClass('active');
            $(this).parent().addClass('active');
            var slideId = $(this).attr('data-slide');
            selectSlide($('#'+slideId));
        });

        var dragging = false;
        $('#video-sidenav').on('touchmove','li a', function(){
            dragging = true;
        });
        $('#video-sidenav').on("touchstart",'li a', function(){
            dragging = false;
        });
        var newWin;
        $('#video-sidenav').on('click touchend','li a', function(evt){
            evt.preventDefault();
            if (dragging) return;
            $('body').addClass('is-playing');
            $('#video-sidenav').find('li').removeClass('active');
            $(this).parent().addClass('active');
            $vidSource = $(this).attr('data-vid');
            $('.tutorial-video').attr('src', $vidSource);
            $('.tutorial-video')[0].load();
            $('.tutorial-video')[0].play();
            $('.tutorial-video').show();
            $('.cover').hide();
        });

        $('.show-cover').on('click touchstart',function(evt){
            evt.preventDefault();
            $('.cover').show();
            $('.tutorial-video').hide();
            $('.tutorial-video').attr('src', '');
        });


        $('#tutorial-video').on('click touchstart',function(evt){
            evt.preventDefault();
            if($('.tutorial-video')[0].paused){
                $('.tutorial-video')[0].play();
                $('body').addClass('is-playing');
            }else{
                $('.tutorial-video')[0].pause();
                $('body').removeClass('is-playing');
            }
        });
        $(document).idle({
            onIdle: function(){
                if($('body').hasClass('is-playing')){
                    $('.idle-hide').fadeOut();
                    $('#video-sidenav').data('plugin_iptOffCanvas').toggle(false);

                }
            },
            idle: 5000,
            startAtIdle: true
        });

        $(document).on('click touchstart',function(){
            $('.idle-hide').fadeIn();
        });

        $('#show-video').on('click touchstart',function(evt){
            evt.preventDefault();
            $('#video-sidenav').data('plugin_iptOffCanvas').toggle(false);
            $('#slide-sidenav').data('plugin_iptOffCanvas').toggle(false);
            $('.video-visible').show();
            $('.slide-visible').hide();
            $('.tutorial-video').show();
            $('body').removeClass('slide-mode').addClass('video-mode');
        });

        $('#show-slide').on('click touchstart',function(evt){
            evt.preventDefault();
            $('#video-sidenav').data('plugin_iptOffCanvas').toggle(false);
            $('#slide-sidenav').data('plugin_iptOffCanvas').toggle(false);
            $('.video-visible').hide();
            $('.slide-visible').show();
            $('body').removeClass('video-mode').addClass('slide-mode');
            $('body').removeClass('is-playing');
        });
        $('#back').on('click touchstart', function(evt){
            evt.preventDefault();
            back();
        });

        $('#next').on('click touchstart', function(evt){
            evt.preventDefault();
            forward();
        });

        window.enableInlineVideo($('.tutorial-video')[0], {
            everywhere: true
        });
    });
});