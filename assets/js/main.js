
function SVGDDMenu( el, options ) {
    this.el = el;
    this.init();
}

SVGDDMenu.prototype.init = function() {
    this.shapeEl = this.el.querySelector( 'div.morph-shape' );

    var s = Snap( this.shapeEl.querySelector( 'svg' ) );
    this.pathEl = s.select( 'path' );
    this.paths = {
        reset : this.pathEl.attr( 'd' ),
        trans : this.shapeEl.getAttribute( 'data-morph-trans' ),
        open : this.shapeEl.getAttribute( 'data-morph-open' )
    };

    this.isOpen = false;

    this.initEvents();
};

SVGDDMenu.prototype.initEvents = function() {
    this.el.addEventListener( 'click', this.toggle.bind(this) );
    [].slice.call( this.el.querySelectorAll('a') ).forEach( function(el) {
        el.onclick = function() { return false; }
    } );
};
SVGDDMenu.prototype.toggle = function() {
    var self = this;

    if( this.isOpen ) {
        classie.remove( self.el, 'menu--open' );
        self.pathEl.stop().animate( { 'path' : this.paths.reset }, 3000, mina.elastic );
    }
    else {
        classie.add( self.el, 'menu--open' );

        this.pathEl.stop().animate( { 'path' : this.paths.trans }, 320, mina.easeinout, function() {
            self.pathEl.stop().animate( { 'path' : self.paths.open }, 2500, mina.elastic );
        } );

    }
    this.isOpen = !this.isOpen;
};

jQuery(function($) {
    $(function() {

        $('.autoplay').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 2000,
            swipe:false,
            dots:true,
            responsive:[
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });


        if($('.register').length > 0){
            $('.inline-menu').remove();
        }

        $('.image-slider .slider').each(function(){
            $(this).slick({
                autoplay: false,
                swipe:false,
                infinite: false,
                dots:true,
                appendDots:$(this).parent().find('.slider-dots')
            });
        });

        $('.student-works').slick({
            arrows:false,
            swipe:false,
            dots:true
        });


        $('.nav.navbar-nav a').click(function(evt){
            if(lgFlag==0){
                var hash = $(this).attr('href');

            }else{
                $('#mobile-menu').collapse('hide');
            }
        });


        $(".rating").starRating({
            starSize: 15,
            totalStars:5,
            strokeWidth:3,
            readOnly: true,
            hoverColor: '#f1db80',
            activeColor:'#ff9605',
            emptyColor:'#fff',
            strokeColor: '#e2e4e5',
            useGradient:false,
            callback: function(rating, $el) {
                clearSelection();
            }
        });

        $(".user-rating").starRating({
            starSize: 30,
            totalStars:5,
            strokeWidth:25,
            readOnly: false,
            useGradient:false,
            useFullStars: true,
            emptyColor:'#fff',
            hoverColor: '#fd9832',
            activeColor:'#fd9832',
            strokeColor: '#e1e1e1',
            callback: function(rating, $el) {
                clearSelection();
            }
        });
        var heart;
        $('.heart').starRating({
            strokeWidth: 30,
            emptyColor: '#ffffff',
            hoverColor: '#ffe158',
            activeColor: '#ffe158',
            starSize: 25,
            totalStars: 1,
            useFullStars: true,
            useGradient: false,
            disableAfterRate: false,
            starShape: 'heart',
            callback: function(rating, $el) {
                heart ^= 1;
                $('.heart').starRating('setRating', heart);
                clearSelection();
            }
        });

        if($('#curtain-menu').length> 0){
            new SVGDDMenu( document.getElementById( 'curtain-menu' ) );
        }


        $('#curtain-menu li a').click(function(evt){
           evt.preventDefault();
           var selectedTitle = $(this).attr('title');
           $('#curtain-menu .menu__label span.chapter').text(selectedTitle);
        });


        function autoPlayYouTubeModal(){
            var trigger = $("body").find('[data-target="#videoModal"]');
            trigger.click(function() {
                var theModal = $(this).data( "target" ),
                    videoSRC = $(this).attr( "data-video" ),
                    videoSRCauto = videoSRC+"?autoplay=1" ;
                $(theModal+' iframe').attr('src', videoSRCauto);
                $(theModal+' button.close').click(function () {
                    $(theModal+' iframe').attr('src', videoSRC);
                });
            });
        }
        autoPlayYouTubeModal();

        var $window = $(window);
        var lgFlag;



        $("#videoModal").on('hidden.bs.modal', function (e) {
            $("#videoModal iframe").attr("src", '');
        });

        $('*[data-btn="open-modal-ideas-upload"]').on('click', function() {
            var $this = $(this);
            var params = {
                'course_id':'3494',
            };
            $.ajax({
                url: './courses/courseDetailIdeasUpload.php',
                type: "post",
                data: params,
                dataType: 'html',
                async: true,
                error: function(data) {}
            }).done(function(data){
                $("#uploadModal").find('.modal-content').html(data);
                $("#uploadModal").modal('show');
            });
        });

        $('body').on('click', '*[data-btn="back-to-course-ideas"]', function(e) {
            var $this = $(e.target);
            var $block = $('*[data-block="list"][data-block-type="course-ideas"]');
            var $block_data = $('*[data-block="list"][data-block-type="course-ideas-data"]');
            $block.show();
            $block_data.empty();
        });
        function loadDetailIdeasData(course_ideas_id, page){
            var $block = $('*[data-block="list"][data-block-type="course-ideas"]');
            var $block_data = $('*[data-block="list"][data-block-type="course-ideas-data"]');
            $.ajax({
                url: './courses/courseDetailIdeasData.php',
                type: "post",
                data: {
                    'course_ideas_id':course_ideas_id,
                    '__page':page,
                },
                dataType: 'html',
                async: true,
                error: function(data) {}
            }).done(function(data){
                $block.hide();
                $block_data.html(data);
            });
        }
        function showDetailIdeas(student_id){
            $.ajax({
                url: './courses/courseDetailIdeas.php',
                type: "post",
                data: {
                    'school_id':'114',
                    'course_id':'3484',
                    'student_id':student_id,
                    '__page':1,
                },
                dataType: 'html',
                async: true,
                error: function(data) {}
            }).done(function(data){
                $(".worksModal").find('.modal-body').html(data);
                $(".worksModal").modal('show');
            });
        }
        $('#back-to-top').click(function(evt){
           evt.preventDefault();
            $('html, body').animate({
                scrollTop: 0
            }, 800);
        });

        var $document = $(document),
            $element = $('#back-to-top');

        $document.scroll(function() {
            if ($document.scrollTop() >= 4900) {
                $element.fadeIn();
            } else {
                $element.fadeOut();
            }
        }).trigger('scroll');


        $('body').on('click', '*[data-btn="show-course-ideas"]', function(e) {
            var $this = $(e.target);
            var student_id = $this.attr('data-student-id');
            showDetailIdeas(student_id);
        });
        $('body').on('click', '*[data-btn="load-course-ideas-data"]', function(e) {
            var $this = $(e.target);
            var id = $this.attr('data-id');
            loadDetailIdeasData(id, 1);
        });

        $('.back-overview').click(function(evt){
            $('.image-slider').hide();
            $(this).parents('.modal-content').find('.image-list').show();
            $(this).hide();
            $('.image-slider .slider').each(function() {
                $(this).slick('slickGoTo', 0, true);
            });
        });

        $('.image-list a').click(function(evt){
            evt.preventDefault();
            $(this).parents('.modal-body').find('.image-list').hide();
            var thisAlbum = $(this).attr('data-album');
            $('#'+thisAlbum).show();
            $('.worksModal.in').find('.back-overview').show();
            $('.image-slider .slider').each(function(){
                $(this)[0].slick.refresh();
            });
        });

        function clearSelection(){
            if(document.selection && document.selection.empty) {
                document.selection.empty();
            } else if(window.getSelection) {
                var sel = window.getSelection();
                sel.removeAllRanges();
            }
        }
        function updateResize(){
            if ((window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth) <= 992 && lgFlag != 1) {
                lgFlag = 1;
            } else if ((window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth) > 992 && lgFlag != 0) {
                lgFlag = 0;

            }
        }

        $window.resize(function(){
            updateResize();
        });
        if($('.prompt-login').length>0){
            $('.prompt-login').click(function(evt){
                evt.preventDefault();
                $('#userModal').modal({show:'true'});
            });

            var googleUser = {};
            function attachSignin(element) {
                auth2.attachClickHandler(element, {},
                    function(googleUser) {
                        console.log('Image URL: ' + googleUser.getBasicProfile().getImageUrl());
                        //refresh to logged in page
                    }, function(error) {
                        alert(JSON.stringify(error, undefined, 2));
                    });
            }
            gapi.load('auth2', function(){
                auth2 = gapi.auth2.init({
                    client_id: '357276352953-0f6t4l1c22evils6cv83od699bjb2gi0.apps.googleusercontent.com',
                    cookiepolicy: 'single_host_origin'
                });
                attachSignin($('.google-login')[0]);
            });
        }


        $('.facebook-login').click(function(evt){
            evt.preventDefault();
            FB.login(function(response) {
                if (response.authResponse) {
                    accessToken = response.authResponse.accessToken;
                    userId = response.authResponse.userID;
                    console.log('http://graph.facebook.com/'+userId+'/picture?type=large');
                    //refresh to logged in page

                } else {
                    //user hit cancel button
                    console.log('User cancelled login or did not fully authorize.');

                }
            });
        });
        $('.search-btn').click(function(evt){
            evt.preventDefault();
            $('.search-overlay').addClass('active');
        });


        $('.close-search').click(function(evt){
            evt.preventDefault();
            $('.search-overlay').removeClass('active');
        });



        $('#mobile-navbar-menu').on('show.bs.collapse', function () {
            $('.hamburger').addClass('is-active');
            $('.navbar-fixed-top').addClass('on');
            $('.inline-menu').hide();
        });
        $('#mobile-navbar-menu').on('hide.bs.collapse', function (evt) {
            $('.hamburger').removeClass('is-active');
            $('.navbar-fixed-top').removeClass('on');
        });
        $('#mobile-navbar-menu').on('hidden.bs.collapse', function (evt) {
            $('.inline-menu').show();
        });

        function checkCart(){
            if($('#cart li').length==0){
                $('#cart').addClass('empty');
                $('.cart-list').hide();
            }else{
                $('#cart').removeClass('empty');
                $('.cart-list').show();
            }
        }



        if($('#cart').length>0){
            $('#cart').iptOffCanvas({closeOnClickOutside: true});

            $('#cart').on('focusin', '.input-number', function(){
                $(this).data('oldValue', $(this).val());
            });

            $('#cart').on('change', '.input-number', function(){
                minValue =  parseInt($(this).attr('min'));
                valueCurrent = parseInt($(this).val());

                name = $(this).attr('name');
                if(valueCurrent >= minValue) {
                    $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
                } else {
                    alert('Sorry, the minimum value was reached');
                    $(this).val($(this).data('oldValue'));
                }
                if(valueCurrent <= maxValue) {
                    $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
                } else {
                    alert('Sorry, the maximum value was reached');
                    $(this).val($(this).data('oldValue'));
                }
            });

            $('#cart').on('click', '.btn-number', function(evt){
                evt.preventDefault();

                fieldName = $(this).attr('data-field');
                type      = $(this).attr('data-type');
                var input = $("input[name='"+fieldName+"']");
                var currentVal = parseInt(input.val());
                if (!isNaN(currentVal)) {
                    if(type == 'minus') {

                        if(currentVal > input.attr('min')) {
                            input.val(currentVal - 1).change();
                        }
                        if(parseInt(input.val()) == input.attr('min')) {
                            $(this).attr('disabled', true);
                        }

                    } else if(type == 'plus') {

                        if(currentVal < input.attr('max')) {
                            input.val(currentVal + 1).change();
                        }
                        if(parseInt(input.val()) == input.attr('max')) {
                            $(this).attr('disabled', true);
                        }

                    }
                } else {
                    input.val(0);
                }
            });

            $('#cart').on('click','.cart-remove',function(evt){
                evt.preventDefault();
                evt.stopImmediatePropagation();
                $(this).parents('li').remove();
                checkCart();
            });

            $('.add-to-cart').click(function(evt){
                evt.preventDefault();
                evt.stopImmediatePropagation();
                var productId = $(this).parents('.product').attr('data-id');
                var imgUrl = $(this).attr('data-cart-img');
                var productPrice = $(this).attr('data-cart-price');
                var productName = $(this).attr('data-cart-title');

                if($("#cart li[data-id='" + productId + "']").length != 0) return;
                var cartHtml = '<li data-id="'+productId+'"><div class="cart-img"><img src="'+imgUrl+'" alt=""></div><div class="cart-desc">'+productName+'<div class="price">'+productPrice+'</div></div><div class="cart-action"><a href="#" class="btn-number" data-type="plus" data-field="cart['+productId+']">+</a><input type="text" name="cart['+productId+']" class="input-number" value="1" min="1" max="10000"><a href="#" class="btn-number" data-type="minus" data-field="cart['+productId+']">-</a><a href="#" class="cart-remove"><img src="./assets/images/remove.svg" alt=""></a></div></li>';
                $('.cart-list').append(cartHtml);
                checkCart();

            });

        }




        $('.input-number').focusin(function(){
            $(this).data('oldValue', $(this).val());
        });
        $('.input-number').change(function() {
            minValue =  parseInt($(this).attr('min'));
            maxValue =  parseInt($(this).attr('max'));
            valueCurrent = parseInt($(this).val());

            name = $(this).attr('name');
            if(valueCurrent >= minValue) {
                $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
            } else {
                alert('Sorry, the minimum value was reached');
                $(this).val($(this).data('oldValue'));
            }
            if(valueCurrent <= maxValue) {
                $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
            } else {
                alert('Sorry, the maximum value was reached');
                $(this).val($(this).data('oldValue'));
            }
        });
        $('.btn-number').click(function(e){
            e.preventDefault();

            fieldName = $(this).attr('data-field');
            type      = $(this).attr('data-type');
            var input = $("input[name='"+fieldName+"']");
            var currentVal = parseInt(input.val());

            if (!isNaN(currentVal)) {
                if(type == 'minus') {

                    if(currentVal > input.attr('min')) {
                        input.val(currentVal - 1).change();
                    }
                    if(parseInt(input.val()) == input.attr('min')) {
                        $(this).attr('disabled', true);
                    }

                } else if(type == 'plus') {

                    if(currentVal < input.attr('max')) {
                        input.val(currentVal + 1).change();
                    }
                    if(parseInt(input.val()) == input.attr('max')) {
                        $(this).attr('disabled', true);
                    }
                }
            } else {
                input.val(0);
            }
        });

        $('.open-rate-form').click(function(evt){
            evt.preventDefault();
           $(this).hide();
           $('.close-rate-form').show();
           $('.rate-form').slideDown();
           $('.rate-form textarea').val('');
        });
        $('.close-rate-form').click(function(){
            $(this).hide();
            $('.open-rate-form').show();
            $('.rate-form').slideUp();
        });


        $('.comment-form').validate({
            rules:{
                commentMsg:{
                    required:true
                }
            },
            messages:{
                commentMsg:{
                    required:'你還沒輸入文字唷！'
                }
            },
            submitHandler:function(){
                return false;
            }
        });

        $('.rate-form').validate({
           rules:{
               rateComment:{
                   required:true
               }
           },
            messages:{
               rateComment:{
                   required:'你還沒輸入文字唷！'
               }
            },
            submitHandler:function(){
               return false;
            }
        });

        var units = ['b', 'KB', 'MB'];

        function niceBytes(x){
            var l = 0, n = parseInt(x, 10) || 0;
            while(n >= 1024){
                n = n/1024;
                l++;
            }
            return(n.toFixed(n >= 10 || l < 1 ? 0 : 1) + ' ' + units[l]);
        }

        if($('#fileupload').length>0){
            $('#fileupload').fileupload({
                url: './service/upload.php',
                maxFileSize: 100,
                dataType: 'json',
                done: function (e, data) {
                    var responseHtml = '';

                    if(data.messages){
                        responseHtml += '<li><img src="./assets/images/doc.svg" class="inline"><span class="file-info"><p class="filename">'+data.files[0].name+'</p>'+niceBytes(data.files[0].size)+'</span><span class="status"><span class="glyphicon glyphicon-minus-sign"></span>失敗</span><a href="#" class="remove-upload" data-deleteurl="'+data.files[0].deleteUrl+'"><img src="./assets/images/remove.svg" alt=""></a></li>';
                    }else{
                        $.each(data.result.files, function (index, file) {
                            responseHtml += '<li><img src="./assets/images/doc.svg" class="inline"><span class="file-info"><p class="filename">'+file.name+'</p>'+niceBytes(file.size)+'</span><span class="status"><span class="glyphicon glyphicon-ok-sign"></span>成功</span><a href="#" class="remove-upload" data-deleteurl="'+file.deleteUrl+'"><img src="./assets/images/remove.svg" alt=""></a></li>';
                        });
                    }

                    $('#uploadModal ul').append(responseHtml);
                },
                progressall: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    /*
                    $('#progress .progress-bar').css(
                        'width',
                        progress + '%'
                    );
                    */
                }
            });
            $('#uploadModal').on('click','.remove-upload', function(evt){
                evt.preventDefault();
                var list = $(this).parents('li');
                var deleteUrl = $(this).attr('data-deleteurl');
                $.ajax({
                    url:deleteUrl,
                    type:'DELETE'
                }).done(function(){
                    list.remove();
                });
            });
        }
    });


    $('.login-form').validate({
       rules:{
           loginID:{
               required:true
           },
           loginPw:{
               required:true
           }
       },
        messages:{
            loginID:{
                required:'請輸入手機號碼或者EMAIL'
            },
            loginPw:{
                required:'請輸入密碼'
            }
        },
        submitHandler:function(){
           return false;
        }
    });

    $('.forget-pw').click(function(evt){
        $(".password-recovery-wrapper").show();
        $(".login-wrapper").hide();
    });


    if($("#userModal").length>0){
        $("#userModal").on('hidden.bs.modal', function (e) {
            $(".password-recovery-wrapper").hide();
            $(".login-wrapper").show();
        });
    }
    $('.close-recovery').click(function(evt){
        evt.preventDefault();
        $(".password-recovery-wrapper").hide();
        $(".login-wrapper").show();
    });

    $('.sendcode, .pending').click(function(evt){
        evt.preventDefault();
        var display = $('.second');
        startTimer(120, display);
    });

    $('.password-recovery-form').validate({
        rules:{
            recoveryPhone:{
                required: true
            },
            recoveryCode:{
                required: true,
                remote: {
                    url: "./service/code.php",
                    type: "post",
                    data: {
                        recoveryPhone: function(d) {
                            return $( "#recoveryPhone" ).val();
                        }
                    },
                    dataFilter: function(data) {
                        var json = JSON.parse(data);

                        if(data=='true'){
                            if($('#recoveryCode-success').length==0){
                                $('#recoveryCode').parent().append('<label id="recoveryCode-success"></label>');
                            }
                            return true;
                        }else{
                            $('#recoveryCode').parent().append('<label id="recoveryCode-another" class="error"></label>');
                        }
                        $('#recoveryCode-success').remove()
                        return false;

                    }
                }
            },

            recoveryPassword:{
                required: true
            },
            recoveryConfirmPassword:{
                required:true,
                equalTo:'#recoveryPassword'
            }
        },
        messages:{
            recoveryPhone:{
                required:''
            },
            recoveryCode:{
                required: '請輸入手機驗證碼',
                remote:''
            },
            recoveryPassword:{
                required: '請輸入密碼'
            },
            recoveryConfirmPassword:{
                required:'兩次輸入密碼不一致',
                equalTo:'兩次輸入密碼不一致'
            }
        }
    });

    if($('#fullpage').length>0) {
        /* 01/03/2018 */
        /* replace fullpage js */
        var scrollFlag = 0;
        $(window).on('scroll', function() {
            if(scrollFlag == 0 && $(window).scrollTop() > 0){
                scrollFlag = 1;
                $('body').addClass('scrolled');
                $('.navbar-fixed-top').addClass('on');
            }
        });
        $('#sticky-menu .navbar-nav a, .inline-menu .navbar-nav a').click(function(evt){
           evt.preventDefault();
           var hrefSection = $(this).attr('href').replace(/-section/gi, '');

            $('html, body').animate({
                scrollTop: $(hrefSection).offset().top - 150
            }, 1000);
        });
    }

    if($('.homepage').length>0){
        var anchor_offset = $('.homepage').offset().top+50;
        var masthead_offset = $('.homepage-slide').offset().top+100;

        $(window).on('scroll', function() {

            if ( $(window).scrollTop() > anchor_offset ) {
                $('nav').addClass('on');
            }else{
                $('nav').removeClass('on');
            }
            if($(window).scrollTop()>= masthead_offset){
                $('.masthead').addClass('on');
            }else{
                $('.masthead').removeClass('on');
            }
        });

        $('.homepage-slide').slick({
            slidesToShow: 4,
            slidesToScroll: 4,
            autoplay: false,
            swipe:false,
            dots:true,
            responsive:[
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        dots: false,
                        arrows: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        dots: false,
                        arrows: true
                    }
                }
            ]
        });

        $('.lecturer-slide').owlCarousel({
            center: true,
            loop: true,
            responsive:{
                0:{
                    items:1,
                    nav:true,
                    dots:false
                },
                768:{
                    items:3,
                    nav:true,
                    dots:true
                }
            }
        });



        $("#masthead-bg").kenburnsy();
    }

    $('.readAll').click(function(evt){
       evt.preventDefault();
       $('.message-center').hide();
       $('.messages').show();
    });
    $('.moreDel').click(function(evt){
        evt.preventDefault();
        $('.message-center').show();
        $('.messages').hide();
    });


    $('.register-form').validate({
        rules:{
            registerEmail:{
                required:true,
                email:true
            },
            registerPassword:{
                required:true,
                regex: /[a-zA-Z]/gi,
                minlength:6
            },
            registerConfirmPassword:{
                required:true,
                equalTo:'#registerPassword'
            },
            registerPhone:{
                required:true
            },
            registerCode:{
                required:true,
                remote: {
                    url: "./service/code.php",
                    type: "post",
                    data: {
                        registerPhone: function(d) {
                            return $( "#registerPhone" ).val();
                        }
                    },
                    dataFilter: function(data) {
                        var json = JSON.parse(data);

                        if(data=='true'){
                            if($('#registerCode-success').length==0){
                                $('#registerCode').parent().append('<label id="registerCode-success"></label>');
                            }
                            return true;
                        }else{
                            $('#registerCode').parent().append('<label id="registerCode-another" class="error"></label>');
                        }
                        $('#registerCode-success').remove()
                        return false;

                    }
                }
            },
            registerUsername:{
                required:true
            },
            registerTerms:{
                required:true
            }
        },
        messages:{
            registerEmail:{
                required:'請輸入正確的E-Mail地址',
                email: '請輸入正確的E-Mail地址'
            },
            registerPassword:{
                required:'建議密碼含英文字母大小，長度至少為6字元',
                minlength:'建議密碼含英文字母大小，長度至少為6字元'
            },
            registerConfirmPassword:{
                required:'兩次輸入的密碼不一致',
                equalTo: '兩次輸入的密碼不一致'
            },
            registerPhone:{
                required:'請輸入正確的手機號碼'
            },
            registerCode:{
                required:'請輸入手機驗證碼',
                remote:''
            },
            registerUsername:{
                required:'請輸入2-10字元用戶暱稱'
            },
            registerTerms:{
                required:''
            }
        },
        errorPlacement:function(err, el){
            //error.insertAfter(element);
            el.parents('.form-group').find('.validation-col').append(err);
        },
        submitHandler:function(){
            return false;
        }
    });



    $('.contact-form').validate({
        rules:{
            contactSubject:{
                required:true
            },
            contactName:{
                required:true,
            },
            contactEmail:{
                required:true,
                email: true
            },
            contactMsg:{
                required:true
            }
        },
        messages:{
            contactSubject:{
                required:'請輸入相關事項'
            },
            contactName:{
                required:'請輸入姓名'
            },
            contactPhone:{
                required:'請輸入電話'
            },
            contactEmail:{
                required:'E-mail',
                email: '請輸入正確的E-Mail地址'
            },
            contactMsg:{
                required:'請輸入文字'
            }
        },
        errorPlacement:function(err, el){
            //error.insertAfter(element);
            el.parents('.form-group').find('.validation-col').append(err);
        },
        submitHandler:function(){
            return false;
        }
    });


});



function startTimer(seconds, display) {
    var timer =  seconds;

    display.text(seconds);
    $('.original').hide();
    $('.pending').show();
    seconds--;
    setInterval(function () {
        display.text(seconds);
        if (--seconds < 0) {
            $('.original').show();
            $('.pending').hide();
        }
    }, 1000);
}