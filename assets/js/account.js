jQuery(function($) {
    $(function() {

        $('.search-btn').click(function(evt){
            evt.preventDefault();
            $('.search-overlay').addClass('active');
        });


        $('.close-search').click(function(evt){
            evt.preventDefault();
            $('.search-overlay').removeClass('active');
        });

        $('#mobile-navbar-menu').on('show.bs.collapse', function () {
            $('.hamburger').addClass('is-active');
            $('.inline-menu').hide();
        });
        $('#mobile-navbar-menu').on('hide.bs.collapse', function (evt) {
            $('.hamburger').removeClass('is-active');
        });
        $('#mobile-navbar-menu').on('hidden.bs.collapse', function (evt) {
            $('.inline-menu').show();
        });
        $('.reset-form').click(function(evt){
            evt.preventDefault();
            $(this).parents('form')[0].reset();
        });

        $('.sendcode, .pending').click(function(evt){
            evt.preventDefault();
            var display = $(this);
            startTimer(120, display);
        });

        $('#accordion .panel-collapse').on('hide.bs.collapse', function () {
            $('a[href="#'+ $(this).attr('id') +'"]').find('.glyphicon').removeClass('glyphicon-triangle-top').addClass('glyphicon-triangle-bottom');
        });
        $('#accordion .panel-collapse').on('show.bs.collapse', function () {
            $('a[href="#'+ $(this).attr('id') +'"]').find('.glyphicon').removeClass('glyphicon-triangle-bottom').addClass('glyphicon-triangle-top');
        });

        if($('#profileCity').length > 0){
            $('#profileCity').twzipcode();
        }


        $('.toggle-btn').click(function(evt){
            evt.preventDefault();
            $(this).hide();
            if($(this).hasClass('open-btn')){
                $(this).siblings('.close-btn').show();
                $(this).parents('tr').next().find('td').removeClass('collapsed').addClass('expand');
                $(this).parents('tr').next().find('td').slideDown();
            }else{
                $(this).siblings('.open-btn').show();
                $(this).parents('tr').next().find('td').removeClass('expand').addClass('collapsed');
                $(this).parents('tr').next().find('td').slideUp();
            }
        });

        $('.form-wrapper').on('click','.pagination a', function(evt){
            evt.preventDefault();
            var elementId = $(this).attr('href');
            $('.pagination li').removeClass('active');
            $(this).parent().addClass('active');
            $('html, body').animate({
                scrollTop: $(elementId).offset().top-100
            }, 500);

        });

        $('.personal-info-form').validate({
            rules:{
                profileName: 'required',
                profileGender: 'required'
            },
            messages:{
                profileName: '請輸入姓名',
                profileGender: '請選擇性別'
            },
            errorPlacement:function(err, el){
                el.parents('.form-group').find('.validation-col').append(err);
            },
            submitHandler: function(form){
                return false;
            }
        });

        $('.change-password').validate({
            rules:{
                changeOldPassword: {
                    required:true,
                    minlength:6
                },
                changeNewPassword: {
                    required:true,
                    minlength:6
                },
                changeConfirmPassword: {
                    required:true,
                    equalTo:'#changeNewPassword'
                }
            },
            messages:{
                changeOldPassword: {
                    required:'建議密碼含英文字母大小，長度至少為6字元',
                    minlength:'建議密碼含英文字母大小，長度至少為6字元'
                },
                changeNewPassword: {
                    required:'建議密碼含英文字母大小，長度至少為6字元',
                    minlength:'建議密碼含英文字母大小，長度至少為6字元'
                },
                changeConfirmPassword:{
                    required: '兩次輸入的密碼不一致',
                    equalTo: '兩次輸入的密碼不一致'
                }
            },
            errorPlacement:function(err, el){
                el.parents('.form-group').find('.validation-col').append(err);
            },
            submitHandler: function(form){
                $('#successModal').modal('show');
                return false;
            }
        });

        $('.change-email').validate({
            rules:{
                emailOld: {
                    required:true,
                    email:true,
                    remote: {
                        url: "./service/code.php",
                        type: "post",
                        data: {
                            emailOld: function(d) {
                                return $( "#emailOld" ).val();
                            }
                        }
                    }
                },
                emailNew: {
                    required:true,
                    email:true
                }
            },
            messages:{
                emailOld: {
                    required:'請輸入正確的E-Mail地址',
                    email: '請輸入正確的E-Mail地址',
                    remote: '找不到此Email用戶'
                },
                emailNew: {
                    required:'請輸入正確的E-Mail地址',
                    email: '請輸入正確的E-Mail地址'
                }
            },
            errorPlacement:function(err, el){
                el.parents('.form-group').find('.validation-col').append(err);
            },
            submitHandler: function(form){
                $('#successModal').modal('show');
                return false;
            }
        });


        $('.change-phone').validate({
            rules:{
                phoneOld: {
                    required:true,
                    remote: {
                        url: "./service/code.php",
                        type: "post",
                        data: {
                            phoneOld: function(d) {
                                return $( "#phoneOld" ).val();
                            }
                        }
                    }
                },
                phoneNew: {
                    required:true
                }
            },
            messages:{
                phoneOld: {
                    required:'請輸入舊手機號碼',
                    remote: '找不到此Email用戶'
                },
                phoneNew: {
                    required:'請輸入手機號碼'
                }
            },
            errorPlacement:function(err, el){
                el.parents('.form-group').find('.validation-col').append(err);
            },
            submitHandler: function(form){
                $('#successModal').modal('show');
                return false;
            }
        });
    });
});



function startTimer(seconds, display) {
    var timer =  seconds;
    display.find('.second').text(seconds);
    display.find('.original').hide();
    display.find('.pending').show();
    seconds--;
    setInterval(function () {
        display.find('.second').text(seconds);
        if (--seconds < 0) {
            display.find('.original').show();
            display.find('.pending').hide();
        }
    }, 1000);
}