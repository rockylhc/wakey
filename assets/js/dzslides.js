var friendWindows = [];
var idx = 1;
var slides;
var activeSlide;
/* main() */

window.onload = function() {
    initSlide();
    $('body').addClass('loaded');
    setupTouchEvents();
}
function initSlide(){
    slides = $('.slide-wrapper').first().find('section');
    $('.slide-wrapper').hide();
    $('.slide-wrapper').first().show();
    setSlide($('.slide-wrapper').first());
    activeSlide = $('.slide-wrapper').first();
}

window.onkeydown = function(e) {
    // Don't intercept keyboard shortcuts
    if (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) {
        return;
    }
    if ( e.keyCode == 37 // left arrow
        || e.keyCode == 38 // up arrow
        || e.keyCode == 33 // page up
    ) {
        e.preventDefault();
        back();
    }
    if ( e.keyCode == 39 // right arrow
        || e.keyCode == 40 // down arrow
        || e.keyCode == 34 // page down
    ) {
        e.preventDefault();
        forward();
    }

    if ( e.keyCode == 32) { // space
        e.preventDefault();
        toggleContent();
    }
}

/* Touch Events */

function setupTouchEvents() {
    var orgY, newY;
    var tracking = false;

    var body = document.body;

    var slider = document.getElementsByClassName("slide-wrapper");


    for (var i = 0; i < slider.length; i++) {
        slider[i].addEventListener('touchstart', start, false);
        slider[i].addEventListener('touchmove', move, false);
    }

    function start(e) {
        e.preventDefault();
        tracking = true;
        orgY = e.changedTouches[0].pageY;
    }

    function move(e) {
        if (!tracking) return;
        newY = e.changedTouches[0].pageY;
        if (orgY - newY > 100) {
            tracking = false;
            forward();
        } else {
            if (orgY - newY < -100) {
                tracking = false;
                back();
            }
        }
    }
}

/* Adapt the size of the slides to the window */


function getDetails(idx) {
    var s = document.querySelector("section:nth-of-type("+ idx +")");
    var d = s.querySelector("details");
    return d?d.innerHTML:"";
}

function selectSlide(el){
    idx =1;
    slides = el.find('section');
    activeSlide = el;
    $('.slide-wrapper').hide();
    activeSlide.show();
    setSlide(activeSlide);

}

/* Slide controls */

function back() {
    if (idx != 1){
        idx--;
        setSlide(activeSlide);
    }

}
function forward() {
    if (idx < slides.length) {
        idx++;
        setSlide(activeSlide);
    }
}
function setSlide(el) {

    var old = el.find("section[aria-selected]");
    var next = el.find("section:nth-child("+(idx)+")");

    if (old) {
        old.removeAttr("aria-selected");
    }
    if (next) {
        next.attr("aria-selected", "true");
    } else {
        console.warn("No such slide: " + idx);
        idx = 0;

        for (var i = 0; i < slides.length; i++) {
            if (slides[i].hasAttribute("aria-selected")) {
                idx = i + 1;
            }
        }
    }
    $('#current').text((idx));
    $('#count').text(slides.length);
}
