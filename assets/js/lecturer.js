jQuery(function($) {
    $(function() {

        $('.search-btn').click(function(evt){
            evt.preventDefault();
            $('.search-overlay').addClass('active');
        });


        $('.close-search').click(function(evt){
            evt.preventDefault();
            $('.search-overlay').removeClass('active');
        });

        $('#mobile-navbar-menu').on('show.bs.collapse', function () {
            $('.hamburger').addClass('is-active');
            $('.inline-menu').hide();
        });
        $('#mobile-navbar-menu').on('hide.bs.collapse', function (evt) {
            $('.hamburger').removeClass('is-active');
        });
        $('#mobile-navbar-menu').on('hidden.bs.collapse', function (evt) {
            $('.inline-menu').show();
        });


        $('.sendcode, .pending').click(function(evt){
            evt.preventDefault();
            var display = $(this);
            startTimer(120, display);
        });



        $('.toggle-btn').click(function(evt){
            evt.preventDefault();
            $(this).hide();
            if($(this).hasClass('open-btn')){
                $(this).siblings('.close-btn').show();
                $(this).parents('tr').next().find('td').removeClass('collapsed').addClass('expand');
                $(this).parents('tr').next().find('td').slideDown();
            }else{
                $(this).siblings('.open-btn').show();
                $(this).parents('tr').next().find('td').removeClass('expand').addClass('collapsed');
                $(this).parents('tr').next().find('td').slideUp();
            }
        });
        var owl = $('.picture-slide').owlCarousel({
            center: true,
            loop: true,
            responsive:{
                0:{
                    items:1,
                    nav:true,
                    dots:false,
                    touchDrag:true,
                    mouseDrag:true
                },
                768:{
                    items:3,
                    nav:false,
                    dots:false,
                    autoWidth:true,
                    touchDrag:false,
                    mouseDrag:false
                }
            }
        });

        owl.on('resized.owl.carousel', function(event) {
            //console.log(event)
            if($(window).width()>= 768){
                //owl.trigger('destroy.owl.carousel');
            }else{
                //owl.trigger('initialize.owl.carousel');
            }

        })




    });
});



function startTimer(seconds, display) {
    var timer =  seconds;
    display.find('.second').text(seconds);
    display.find('.original').hide();
    display.find('.pending').show();
    seconds--;
    setInterval(function () {
        display.find('.second').text(seconds);
        if (--seconds < 0) {
            display.find('.original').show();
            display.find('.pending').hide();
        }
    }, 1000);
}