<?php include('templates/header.php'); ?>
<body class="register">

<?php include('templates/nav.php'); ?>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 register-bg">
            <h2>
                學習不該有所受限– <br />
                Wakey打破線上學習的互動限制
            </h2>
            <p>
                拉近線上與線下的距離<br />
                讓學習貼近每個人的真生活
            </p>
        </div>
        <div class="col-md-9 register-form-wrapper">
            <div class="row">
                <div class="col-xs-12 col-md-push-1 col-md-11 register-inner">
                    <div class="row">
                        <div class="col-md-5">
                            <h3>註冊</h3><a href="#" class="prompt-login pull-right">我已有帳號 <img src="./assets/images/right-arrow.svg" alt=""></a>
                        </div>
                    </div>

                    <form class="register-form">
                        <div class="form-group row">
                            <div class="col-md-5">
                                <input type="email" class="form-control" id="registerEmail" name="registerEmail" placeholder="EMAIL 帳號">
                            </div>
                            <div class="col-md-7 validation-col"></div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-5">
                                <input type="password" class="form-control" id="registerPassword" name="registerPassword" placeholder="密碼">
                            </div>
                            <div class="col-md-7 validation-col"></div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-5">
                                <input type="password" class="form-control" id="registerConfirmPassword" name="registerConfirmPassword" placeholder="確認密碼">
                            </div>
                            <div class="col-md-7 validation-col"></div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-5">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-8">
                                        <input type="text" class="form-control" id="registerPhone" name="registerPhone" placeholder="手機號碼">
                                    </div>

                                    <div class="col-xs-12 col-sm-4 no-pad-left-on-sm">
                                        <a href="#" class="blue-button sendcode">
                                            <span class="original">發送驗證碼</span>
                                            <span class="pending" style="display:none;">重新發送(<span class="second"></span>)</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-7 validation-col"></div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-5">
                                <input type="text" class="form-control" id="registerCode" name="registerCode" placeholder="簡訊驗證碼">
                            </div>
                            <div class="col-md-7 validation-col"></div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-5">
                                <input type="text" class="form-control" id="registerUsername" name="registerUsername" placeholder="你的用戶暱稱">
                            </div>
                            <div class="col-md-7 validation-col"></div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-5">

                                <div class="checkbox">
                                    <input type="checkbox" id="registerTerms" name="registerTerms">
                                    <label for="registerTerms">
                                        <span></span>了解並同意接受本<a href="terms.php">服務條款</a>之所有內容。
                                    </label>
                                </div>

                            </div>
                            <div class="col-md-7 validation-col"></div>
                        </div>

                        <button type="submit" class="blue-button">註冊</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="push"></div>
</div>

<?php include('templates/footer.php'); ?>
<div class="modal fade user-modal" id="userModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-alignment-center">
            <div class="modal-content">
                <div class="modal-body">
                    <a href="#" class="close" data-dismiss="modal" aria-hidden="true"><img src="./assets/images/close-login.svg" alt=""></a>
                    <div class="row">
                        <div class="col-sm-6">

                            <div class="login-side">
                                <div class="login-wrapper">
                                    <h2>歡迎來到Wakey</h2>
                                    <p>微課讓線上學習更貼近每個人的真實生活！<br />
                                        利用片段時間學習，累積屬於你的價值！
                                    </p>
                                </div>

                                <div class="password-recovery-wrapper" style="display:none">
                                    <h2>找回密碼 <a href="#" class="close close-recovery" aria-hidden="true"><img src="./assets/images/close-search.svg" alt=""></a></h2>
                                    <form class="password-recovery-form">
                                        <h4>別擔心，我們將會協助您取回密碼。</h4>
                                        <div class="form-group clearfix">
                                            <div class="col-xs-12 col-sm-7 ">
                                                <input type="text" class="form-control" id="recoveryPhone" name="recoveryPhone" placeholder="手機號碼">
                                            </div>
                                            <div class="col-xs-12 col-sm-5  no-pad-left-on-sm">
                                                <a href="#" class="blue-button sendcode">
                                                    <span class="original">發送驗證碼</span>
                                                    <span class="pending" style="display:none;">重新發送(<span class="second"></span>)</span>
                                                </a>
                                            </div>


                                        </div>
                                        <div class="form-group clearfix">
                                            <div class="col-sm-12">
                                                <input type="text" class="form-control" id="recoveryCode" name="recoveryCode" placeholder="輸入驗證碼">
                                            </div>
                                        </div>

                                        <div class="form-group clearfix">
                                            <div class="col-sm-12">
                                                <input type="password" class="form-control" id="recoveryPassword" name="recoveryPassword" placeholder="新密碼">
                                            </div>

                                        </div>

                                        <div class="form-group clearfix">
                                            <div class="col-sm-12">
                                                <input type="password" class="form-control" id="recoveryConfirmPassword" name="recoveryConfirmPassword" placeholder="確認密碼">
                                            </div>
                                        </div>
                                        <button type="submit" class="blue-button">送出</button>
                                    </form>
                                </div>

                            </div>

                        </div>
                        <div class="col-sm-6">
                            <div class="login-form-wrapper">

                                    <h2>登入</h2>
                                    <form class="login-form">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="loginID" name="loginID" placeholder="手機或電子信箱">
                                        </div>

                                        <div class="form-group">
                                            <input type="password" class="form-control" id="loginPw" name="loginPw" placeholder="密碼">
                                        </div>

                                        <button type="submit" class="blue-button pull-right">登入</button>
                                        <a href="#" class="forget-pw">忘記密碼？</a>
                                    </form>
                                    <a href="#" class="facebook-login"><img src="./assets/images/facebook-login.svg" alt=""></a>
                                    <a href="#" class="google-login"><img src="./assets/images/google-login.svg" alt=""></a>
                                    <a href="register.php" class="register pull-right">快速註冊</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="search-overlay">
    <a href="#" class="close-search"><img src="./assets/images/close-search.svg" alt=""></a>
    <div class="vertical-alignment-helper text-center">
        <div class="search-wrapper vertical-alignment-center">
            <form class="search-form">
                <input type="text" value="" class="search-text" placeholder="查詢關鍵字...">
                <input type="image" src="./assets/images/search.svg"  />
            </form>
        </div>
    </div>
</div>
<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '298301200659355',
            cookie     : true,
            xfbml      : true,
            version    : 'v2.11'
        });

        FB.AppEvents.logPageView();

    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/zh_TW/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<script src="https://apis.google.com/js/platform.js?onload=onLoadCallback" async defer></script>
<script src="https://code.jquery.com/jquery-2.1.4.min.js"
        integrity="sha384-R4/ztc4ZlRqWjqIuvf6RX5yb/v90qNGx6fS48N0tRxiGkqveZETq72KgDVJCp2TC
sha256-8WqyJLuWKRBVhxXIL1jBDD7SDxU936oZkCnxQbWwJVw="
        crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="assets/js/jquery.js"><\/script>')</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script src="assets/js/snap.svg.min.js"></script>
<script src="assets/js/classie.min.js?v=<?php echo time(); ?>"></script>
<script src="assets/js/bootstrap.min.js?v=<?php echo time(); ?>"></script>
<script src="assets/js/slick.min.js"></script>
<script src="assets/js/jquery.starRating.js?v=<?php echo time(); ?>"></script>
<script src="assets/js/jquery.easings.min.js"></script>
<script src="assets/js/scrolloverflow.min.js"></script>
<script src="assets/js/jquery.fullpage.min.js"></script>
<script src="assets/js/main.js?v=<?php echo time(); ?>"></script>

<script>
    window.ga=function(){ga.q.push(arguments)};ga.q=[];ga.l=+new Date;
    ga('create','UA-XXXXX-Y','auto');ga('send','pageview')
</script>
<script src="https://www.google-analytics.com/analytics.js" async defer></script>

</body>
</html>