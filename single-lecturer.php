<body class="profile-body">
<?php include('templates/profile-header.php'); ?>
<?php include('templates/nav-profile.php'); ?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.2.4/assets/owl.carousel.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.2.4/assets/owl.theme.default.css">
<div class="profile">
    <div class="full-container section lecturer-top" style="background-image:url(./assets/images/single-lecturer-banner.jpg);">
        <div class="banner-center">
            <div class="title yellow">惡夜中的Spotlight，反差萌的屍控彩妝之后</div>
            <p>— Finn</p>
        </div>

    </div>
</div>
<div class="full-background has-background">
    <div class="single-lecturer-section container ">
        <div class="row">
            <div class="col-md-push-1 col-md-10 col-lg-push-1 col-lg-11 background-inner">
                <div class="row">
                    <div class="col-sm-6 col-sm-push-5">
                        <div class="yellow-bg">
                            <img src="./assets/images/lecturer-picture.jpg" alt="">
                        </div>
                    </div>
                    <div class="col-sm-6 col-sm-pull-6">
                        <h6 class="line orange">老師小檔案</h6>
                        <h2 class="diagonal2 orange">彩妝特效大師－ Finn</h2>
                        <div class="social-links">
                            <a href="#" target="_blank"><img src="./assets/images/f.svg" alt=""></a>
                            <a href="#" target="_blank"><img src="./assets/images/ig.svg" alt=""></a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <h3>關於老師</h3>
                        <p>對於研究殭屍、喪屍及各種特效傷妝超感興趣，多種傷口形成的階段與屍變過程<br>
                            有深入研究的她，自小原想當「法醫」，沒想到成為了特效彩妝師！<br>
                            在加拿大多年除了心理系畢業更有專業彩妝學院背景<br>
                            在多場時裝週上更有豐富的後台彩妝師經驗。</p>

                        <p>從偶像劇的打架瘀傷、電影英雄角色，到魔戒裡的半獸人，這些讓人驚豔驚嚇的妝容
                            都是特效化妝。學會這項超酷時尚技能，讓你不管Cosplay、校慶鬼屋、
                            萬聖節Party或自製影片，裝神弄鬼裝可憐都擬真度百分百！</p>

                        <p>電影級超屍控彩妝/不夠逼真不嚇人的初階課─基礎觀念以及材料使用說明，本課程教你6種傷妝，
                            包括瘀傷、擦傷、破皮、皮膚蠟、割傷、打鬥傷妝等技巧。</p>

                        <p>電影級超屍控彩妝/進階的角色設計與殭屍妝─從毒癮、槍傷、液態乳膠的使用方法、咬痕到殭屍角
                            色的發想構思，以及完整的角色實體化。本課程教你更多進階技巧並學習如何做出完整的殭屍角色。</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 mt-30">
                        <h3>經歷</h3>
                        <p>國立交通大學 應用化學 博士<br>
                            國際NAHA芳療證照<br>
                            國家美容乙丙級證照 講師<br>
                            新娘秘書團隊 保養顧問<br>
                            大專院校及社團 特聘彩妝及保養講師<br>
                            12年彩妝、保養品 研發顧問經驗</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="single-lecturer-section container">
    <div class="row">
        <div class="col-md-push-1 col-md-10 col-lg-push-1 col-lg-11">
            <div class="row">
                <div class="overlay-bg-wrapper">
                    <div class="overlay-bg">
                        <img src="./assets/images/lecturer-feature-img.jpg" alt="">
                    </div>
                </div>
                <div class="speech-bubble-wrapper">
                    <div class="speech-bubble">
                        <h1 class="yellow">老師想對學生說的話</h1>
                        <p>
                            「咖啡是任性的，在舌尖散發出的不同口感，承載了許多不同的故事」「相信很多人都曾經想開一家咖啡店，也有許多人是在咖啡店裡找到夢想，你是哪一種呢？我都是，我是Seki，喜歡旅行，更喜歡咖啡，我出發的原因，是咖啡。留下的原因，也是咖啡。」我都是，我是Seki，喜歡旅行，更喜歡咖啡，我出發的原因，是咖啡。留下的原因，也是咖啡。」
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="full-container with-slider">
    <div class="single-lecturer-section container-fluid">
        <div class="row">
            <div class="picture-slide col-sm-5">
                <img src="./assets/images/single-lecturer-1.jpg" alt="">
                <img src="./assets/images/single-lecturer-2.jpg" alt="">
                <img src="./assets/images/single-lecturer-3.jpg" alt="">
            </div>
            <div class="col-sm-5 col-md-5 col-md-push-1 qna">
                <h4>Q:眼線要怎麼畫？要拉多長呢？</h4>
                <h5>seki：</h5>
                <p>
                    「咖啡是任性的，在舌尖散發出的不同口感，承載了許多不同的故事」「相信很多人都曾經想開一家咖啡店，也有許多人是在咖啡店裡找到夢想，你是哪一種呢？我都是，我是Seki，喜歡旅行，更喜歡咖啡，我出發的原因，是咖啡。留下的原因，也是咖啡。」我都是，我是Seki，喜歡旅行，更喜歡咖啡，我出發的原因，是咖啡。留下的原因，也是咖啡。」
                </p>

                <h4>Q:眼線要怎麼畫？要拉多長呢？</h4>
                <h5>seki：</h5>
                <p>
                    「咖啡是任性的，在舌尖散發出的不同口感，承載了許多不同的故事」「相信很多人都曾經想開一家咖啡店，也有許多人是在咖啡店裡找到夢想，你是哪一種呢？我都是，我是Seki，喜歡旅行，更喜歡咖啡，我出發的原因，是咖啡。留下的原因，也是咖啡。」我都是，我是Seki，喜歡旅行，更喜歡咖啡，我出發的原因，是咖啡。留下的原因，也是咖啡。」
                </p>


                <h4>Q:眼線要怎麼畫？要拉多長呢？</h4>
                <h5>seki：</h5>
                <p>
                    「咖啡是任性的，在舌尖散發出的不同口感，承載了許多不同的故事」「相信很多人都曾經想開一家咖啡店，也有許多人是在咖啡店裡找到夢想，你是哪一種呢？我都是，我是Seki，喜歡旅行，更喜歡咖啡，我出發的原因，是咖啡。留下的原因，也是咖啡。」我都是，我是Seki，喜歡旅行，更喜歡咖啡，我出發的原因，是咖啡。留下的原因，也是咖啡。」
                </p>
                <h4>Q:眼線要怎麼畫？要拉多長呢？</h4>
                <h5>seki：</h5>
                <p>
                    「咖啡是任性的，在舌尖散發出的不同口感，承載了許多不同的故事」「相信很多人都曾經想開一家咖啡店，也有許多人是在咖啡店裡找到夢想，你是哪一種呢？我都是，我是Seki，喜歡旅行，更喜歡咖啡，我出發的原因，是咖啡。留下的原因，也是咖啡。」我都是，我是Seki，喜歡旅行，更喜歡咖啡，我出發的原因，是咖啡。留下的原因，也是咖啡。」
                </p>

                <h4>Q:眼線要怎麼畫？要拉多長呢？</h4>
                <h5>seki：</h5>
                <p>
                    「咖啡是任性的，在舌尖散發出的不同口感，承載了許多不同的故事」「相信很多人都曾經想開一家咖啡店，也有許多人是在咖啡店裡找到夢想，你是哪一種呢？我都是，我是Seki，喜歡旅行，更喜歡咖啡，我出發的原因，是咖啡。留下的原因，也是咖啡。」我都是，我是Seki，喜歡旅行，更喜歡咖啡，我出發的原因，是咖啡。留下的原因，也是咖啡。」
                </p>
                <div class="share-section">
                    <p class="share-txt">透過分享讓更多人知道這位好老師！</p>
                    <div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse">Share</a></div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="full-container">

    <div class="container single-lecturer-section">
        <div class="row">
            <div class="col-md-push-1 col-md-10 col-lg-push-2 col-lg-8">
                <div class="content">
                    <h2 class="diagonal dull-yellow">也許你會喜歡</h2>

                </div>

                <ul class="news-list clearfix">

                    <li class="col-xs-12 col-md-6">
                        <a href="#" class="tag">有感生活</a>
                        <div class="area">
                            <div class="thumb"><a href="#"><img src="./assets/images/post-header.jpg" alt=""></a></div>
                            <div class="content">
                                <h3 class="title">在家打造你的咖啡館-遇見我的夏日咖啡 單元1</h3>
                                <div class="desc">
                                    <div class="post-left">
                                        <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg">
                                    </div>
                                    <div class="post-center">
                                        Seki <br>
                                         如是創意咖啡品牌創始人
                                    </div>
                                    <div class="post-right">
                                        <div class="price">$4,900</div>
                                    </div>
                                </div>
                                <a href="#" class="yellow-button">閱讀更多</a>
                            </div>
                        </div>
                    </li>
                    <li class="col-xs-12 col-md-6">
                        <a href="#" class="tag">有感生活</a>
                        <div class="area">
                            <div class="thumb"><a href="#"><img src="./assets/images/post-header.jpg" alt=""></a></div>
                            <div class="content">
                                <h3 class="title">在家打造你的咖啡館-遇見我的夏日咖啡 單元1</h3>
                                <div class="desc">
                                    <div class="post-left">
                                        <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg">
                                    </div>
                                    <div class="post-center">
                                        Seki <br>
                                         如是創意咖啡品牌創始人
                                    </div>
                                    <div class="post-right">
                                        <div class="price">$4,900</div>
                                    </div>
                                </div>
                                <a href="#" class="yellow-button">閱讀更多</a>
                            </div>
                        </div>
                    </li>

                    <li class="col-xs-12 col-md-6">
                        <a href="#" class="tag">有感生活</a>
                        <div class="area">
                            <div class="thumb"><a href="#"><img src="./assets/images/post-header.jpg" alt=""></a></div>
                            <div class="content">
                                <h3 class="title">在家打造你的咖啡館-遇見我的夏日咖啡 單元1</h3>
                                <div class="desc">
                                    <div class="post-left">
                                        <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg">
                                    </div>
                                    <div class="post-center">
                                        Seki <br>
                                         如是創意咖啡品牌創始人
                                    </div>
                                    <div class="post-right">
                                        <div class="price">$4,900</div>
                                    </div>
                                </div>
                                <a href="#" class="yellow-button">閱讀更多</a>
                            </div>
                        </div>
                    </li>
                    <li class="col-xs-12 col-md-6">
                        <a href="#" class="tag">有感生活</a>
                        <div class="area">
                            <div class="thumb"><a href="#"><img src="./assets/images/post-header.jpg" alt=""></a></div>
                            <div class="content">
                                <h3 class="title">在家打造你的咖啡館-遇見我的夏日咖啡 單元1</h3>
                                <div class="desc">
                                    <div class="post-left">
                                        <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg">
                                    </div>
                                    <div class="post-center">
                                        Seki <br>
                                         如是創意咖啡品牌創始人
                                    </div>
                                    <div class="post-right">
                                        <div class="price">$4,900</div>
                                    </div>
                                </div>
                                <a href="#" class="yellow-button">閱讀更多</a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>

    </div>

</div>

<?php include('templates/footer.php'); ?>

<div class="search-overlay">
    <a href="#" class="close-search"><img src="./assets/images/close-search.svg" alt=""></a>
    <div class="vertical-alignment-helper text-center">
        <div class="search-wrapper vertical-alignment-center">
            <form class="search-form">
                <input type="text" value="" class="search-text" placeholder="查詢關鍵字...">
                <input type="image" src="./assets/images/search.svg"  />
            </form>
        </div>
    </div>
</div>

<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.11&appId=965493053524008&autoLogAppEvents=1';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

<script src="https://code.jquery.com/jquery-2.1.4.min.js"
        integrity="sha384-R4/ztc4ZlRqWjqIuvf6RX5yb/v90qNGx6fS48N0tRxiGkqveZETq72KgDVJCp2TC
sha256-8WqyJLuWKRBVhxXIL1jBDD7SDxU936oZkCnxQbWwJVw="
        crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="assets/js/jquery.js"><\/script>')</script>
<script src="assets/js/bootstrap.min.js?v=<?php echo time(); ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2//2.0.0-beta.2.4/owl.carousel.min.js"></script>
<script src="assets/js/lecturer.js?v=<?php echo time(); ?>"></script>
</body>
</html>