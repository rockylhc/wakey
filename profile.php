<body class="profile-body">
<?php include('templates/profile-header.php'); ?>
<?php include('templates/nav-profile.php'); ?>
<div class="profile">
    <div class="full-container section profile-top">
        <img src="./assets/images/user-photo.jpg" class='img-circle' alt="">
        <div class="username">昭渝</div>
        <a href="#" class="logout">登出</a>
    </div>
</div>
<ul class="user-inventory">
    <li>
        <div class="amount">14</div>
        已學習的課程
    </li>
    <li>
        <div class="amount">14</div>
        收藏的課程
    </li>
    <li>
        <div class="amount">14</div>
        關注的導師
    </li>
</ul>

<div class="profile-section container">
    <div class="row">
        <div class="col-sm-4 col-md-push-1 col-md-3 col-lg-push-1 col-lg-2 sidebar">

            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" id="headingOne">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseAccount">
                                <img src="./assets/images/user-icon.svg" alt=""> 我的帳戶
                                <span class="glyphicon glyphicon-triangle-top pull-right"></span>
                            </a>
                        </h4>
                    </div>
                    <div id="collapseAccount" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <a href="#" class="active">編輯個人資料</a>
                            <a href="/profile-settings.php">帳號設定</a>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                        <h4 class="panel-title">
                            <a href="/profile-course.php">
                                <img src="./assets/images/purchased-icon.svg" alt=""> 購買記錄
                            </a>
                        </h4>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                        <h4 class="panel-title">
                            <a href="/my-courses.php">
                                <img src="./assets/images/book-icon.svg" alt=""> 我的課程
                            </a>
                        </h4>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-sm-8 col-md-push-1 col-md-6 col-lg-push-1 col-lg-7 form-wrapper">
            <div class="edit-user-photo">
                <div class="original-img">
                    <img src="./assets/images/user-photo.jpg" class='img-circle' alt="">
                </div>
                <div class="edit-photo-btn">
                    <a href="#" class="upload-photo">
                        <img src="./assets/images/camera.svg" alt=""> <span>上傳照片</span>
                    </a>
                    <a href="#" class="alert">刪除</a>
                </div>
            </div>

            <form class="personal-info-form">
                <div class="form-group row">
                    <div class="col-md-6">
                        <label for="profileName">姓名</label>
                        <input type="text" class="form-control" id="profileName" name="profileName">
                    </div>
                    <div class="col-md-6 validation-col"></div>
                </div>

                <div class="form-group row">
                    <div class="col-md-7 col-lg-6">
                        <label for="profileName" style="display:block;">性別</label>
                        <label class="radio-inline">
                            <input type="radio" name="profileGender" id="profileFemale" value="1"> 女生
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="profileGender" id="profileMale" value="2"> 男生
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="profileGender" id="profileNeutral" value="3"> 保密
                        </label>
                    </div>
                    <div class="col-md-5 col-lg-6 validation-col"></div>
                </div>


                <div class="form-group row">
                    <div class="col-md-6">
                        <label for="profileName">居住城市</label>
                        <div class="row">
                            <div id="profileCity"></div>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-6">
                        <label for="profileOccupation">職業
                        </label>
                        <input type="text" class="form-control" id="profileOccupation" name="profileOccupation">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-6">
                        <label for="profileOccupation">學校
                        </label>
                        <input type="text" class="form-control" id="profileEducation" name="profileEducation">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-8">
                        <label for="profileSummary">個人簡介
                        </label>
                        <textarea class="form-control" rows="4" id="profileSummary" name="profileSummary"></textarea>
                    </div>
                </div>
                <a href="#" class="reset-form">取消</a>
                <button type="submit" class="blue-button">完成</button>
            </form>
        </div>
    </div>
</div>
<ul class="mobile-footer-nav">
    <li><a href="#">
            <img src="./assets/images/course-mobile.svg" alt="">
            我的課程
        </a>
    </li>
    <li><a href="#" data-offcanvas-toggle="cart">
            <img src="./assets/images/cart-mobile.svg" alt="">
            購物車
        </a>
    </li>
    <li class="dropup">
        <a href="#" data-toggle="dropdown">
            <img src="./assets/images/account-mobile.svg" alt="">
            我的帳戶
        </a>

        <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
            <li><a href="#">我的帳戶</a></li>
            <li><a href="#">購買紀錄</a></li>
            <li><a href="#">登出</a></li>
        </ul>

    </li>
</ul>
<?php include('templates/footer.php'); ?>

<div class="search-overlay">
    <a href="#" class="close-search"><img src="./assets/images/close-search.svg" alt=""></a>
    <div class="vertical-alignment-helper text-center">
        <div class="search-wrapper vertical-alignment-center">
            <form class="search-form">
                <input type="text" value="" class="search-text" placeholder="查詢關鍵字...">
                <input type="image" src="./assets/images/search.svg"  />
            </form>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-2.1.4.min.js"
        integrity="sha384-R4/ztc4ZlRqWjqIuvf6RX5yb/v90qNGx6fS48N0tRxiGkqveZETq72KgDVJCp2TC
sha256-8WqyJLuWKRBVhxXIL1jBDD7SDxU936oZkCnxQbWwJVw="
        crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="assets/js/jquery.js"><\/script>')</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script src="assets/js/bootstrap.min.js?v=<?php echo time(); ?>"></script>
<script src="assets/js/jquery.twzipcode.min.js"></script>
<script src="assets/js/account.js?v=<?php echo time(); ?>"></script>
</body>
</html>