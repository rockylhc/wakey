<body>
<?php include('templates/header.php'); ?>
<?php include('templates/nav-live-stream.php'); ?>
<div id="fullpage" class="live-stream">
    <div class="full-container white-bg section fp-noscroll" data-anchor="trailer-section">
        <div class="container">
            <section class="row" id="trailer">
                <div class="col-md-12">
                    <div class="video-frame">
                        <div class="video-responsive">
                            <video width="100%" controls="" controlslist="nodownload" preload="none" poster="https://doctrans-tw.eletang.com/184/184F15154608136144.jpg" style="position:absolute;">
                                <source src="https://outputtw.eletang.com/vproduce001/184/0bf11b46e6af480e92bd72e0bb366a1f/act-ss-mp4-ld/184Vs1515481123364.mp4" type="video/mp4">
                            </video>
                        </div>
                    </div>
                    <div class="hero-wrapper">
                        <div class="hero">
                            <div class="shape-wrap">
                                <div class="shape blue">
                                    <h6 class="line">有感生活</h6>
                                    <div class="pull-right"><div class="heart"></div></div>
                                    <h2>
                                        在家打造你的咖啡館-<br />
                                        遇見我的夏日咖啡
                                    </h2>
                                    <h6>本課程將提撥50%捐贈給慈善基金會</h6>
                                    <div class="rating" data-rating="3"></div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="info col-xs-12">
                                    <p><span class="label blue">學習人數</span><span>40</span></p>
                                    <p><span class="label blue">章節課程</span><span>5</span></p>
                                    <p><span class="label blue">預購倒數</span><span class="orange-txt">99 天</span></p>
                                    <p class="progress-label">預購人數 </p>
                                    <div class="progress-wrap">
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" style="width: 80%;">
                                            </div>
                                        </div>
                                        <div class="bright-blue">75%</div>
                                    </div>

                                    <p class="price">$4,500</p>
                                </div>

                                <ul class="call-to-action col-xs-12">
                                    <li><a href="#" class="primary btn prompt-login">立即購買</a></li>
                                    <li><a href="#" class="secondary btn prompt-login">試聽課程</a></li>
                                </ul>
                            </div>
                            <ul class="helper">
                                <li class="yellow">
                                    <a href="#">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20"><g fill="none" fill-rule="evenodd"><circle cx="10" cy="10" r="10" /><path fill="#FFF" fill-rule="nonzero" d="M10 9.17V10h.83c0-.46-.37-.83-.83-.83m0-2.5v1.1c1.23 0 2.22 1 2.22 2.23h1.1c0-1.84-1.48-3.33-3.32-3.33m0-2.5v1.16c2.57 0 4.67 2.1 4.67 4.67h1.16c0-3.22-2.6-5.83-5.83-5.83M12.54 15c-.05.12-.06.14 0 0M12 13.38c-.38-.2-.77-.45-1.17-.6-.78-.33-.73.6-1.3.92-.35.2-.86-.17-1.18-.36C7.8 13 7.3 12.54 6.9 12c-.2-.27-.66-.78-.6-1.16.08-.6.8-.63.6-1.37-.12-.4-.3-.78-.46-1.16-.2-.5-.27-.8-.84-.8-.42.05-.7.2-.94.56-.7.9-.58 2.1-.12 3.07.65 1.4 1.77 2.7 2.96 3.55.8.58 1.88 1.1 2.87 1.2.72.05 1.72-.34 1.98-1.12l-.04.13.08-.14.05-.14-.04.14c.22-.7.25-1.02-.35-1.34m.5.87c.04-.13.04-.14 0 0"/></g></svg>
                                        電話諮詢</a>
                                    <p class="hide-desktop"><a href="#" href="tel:0222458800">02-2245-8800</a></p>
                                </li>
                                <li class="blue">
                                    <a href="#">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20"><g fill="none" fill-rule="evenodd"><circle cx="10" cy="10" r="10" fill="#387FD7"/><path fill="#FFF" fill-rule="nonzero" d="M11.95 8.02c-1.12 0-2.04-.9-2.04-2C9.9 4.9 10.86 4 11.98 4S14 4.9 14 6c0 1.12-.92 2.02-2.05 2.02zm0-3.22c-.67 0-1.22.54-1.22 1.2 0 .67.55 1.2 1.22 1.2.68 0 1.23-.53 1.23-1.2 0-.66-.55-1.2-1.23-1.2zm-4.9 6.86c-1.13 0-2.05-.9-2.05-2 0-1.12.92-2.02 2.05-2.02 1.12 0 2.04.9 2.04 2 0 1.12-.95 2.02-2.07 2.02zm0-3.2c-.68 0-1.23.53-1.23 1.2 0 .66.55 1.2 1.23 1.2.67 0 1.22-.54 1.22-1.2 0-.67-.55-1.2-1.22-1.2zm4.9 6.8c-1.12 0-2.04-.9-2.04-2.02 0-1.1.95-2 2.07-2 1.13 0 2.05.9 2.05 2s-.9 2-2.04 2zm0-3.22c-.67 0-1.22.54-1.22 1.2 0 .67.55 1.2 1.22 1.2.68 0 1.23-.53 1.23-1.2 0-.66-.55-1.2-1.23-1.2zm-3.47-1.9l2.33 1.95-.5.6-2.33-1.97.53-.6zm1.82-3.58l.54.6-2.36 2-.53-.6 2.35-2z"/></g></svg>
                                        課程分享</a>
                                    <p class="hide-desktop">
                                        <a href="#" target="_blank"><img src="./assets/images/link.svg" alt=""></a>
                                        <a href="#" target="_blank"><img src="./assets/images/f.svg" alt=""></a>
                                    </p>
                                </li>

                            </ul>


                        </div>
                    </div>

                </div>
            </section>

        </div>
    </div>
    <div class="full-container white-bg section fp-noscroll" data-anchor="lecturer-section">
        <div class="full-container">
            <div class="container-fluid">
                <section class="row" id="lecturer">
                    <div class="col-md-6 feature-cover">
                        <div class="responsive-img-wrapper cover-image"><img class="responsive-img" src="./assets/images/cover-seiki.jpg?v=1"></div>
                    </div>

                    <div class="col-md-6" style="max-width:670px;">
                        <div class="content">
                            <h6 class="line orange">誰來開講</h6>
                            <h2 class="polygon orange">OmMaNi Café創辦人－Seki</h2>
                            <p>「咖啡是任性的，在舌尖散發出的不同口感，承載了許多不同的故事」「相信很多人都曾經想開一家咖啡店，也有許多人是在咖啡店裡找到夢想，你是哪一種呢？我都是，我是Seki，喜歡旅行，更喜歡咖啡，我出發的原因，是咖啡。留下的原因，也是咖啡。」我都是，我是Seki，喜歡旅行，更喜歡咖啡，我出發的原因，是咖啡。留下的原因，也是咖啡。」</p>
                            <p class="button-wrapper"><a href="#" class="tertiary btn">查看老師小檔案</a></p>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <div class="full-container diagonal-bg section" data-anchor="courses-section">
        <div class="container">
            <section class="row" id="courses">
                <div class="col-md-offset-1 col-md-5">
                    <div class="content">
                        <h2 class="diagonal yellow">適合族群</h2>
                        <p>對於咖啡沖泡知識與技巧有興趣之初學者</p>
                    </div>
                </div>

                <div class="col-md-6 col-lg-5 pen-bg">
                    <div class="content">
                        <h2 class="diagonal green">需準備的工具/軟體</h2>
                        <p>課程附送咖費豆材料包。如需得到最佳學習體驗，需另行購買或自備磨豆機等相關器具，詳見各單元說明。</p>
                    </div>
                </div>
                <div class="col-sm-12 col-md-offset-1 col-md-11">
                    <div class="content">
                        <h1 class="blue"><img src="./assets/images/course-icon.svg" alt="">透過這堂課你會學到...</h1>
                        <img src="./assets/images/course.jpg" alt="" id="course-img">
                    </div>
                </div>
            </section>
        </div>

    </div>
    <div class="full-container pattern section fp-noscroll" data-anchor="videos-section">

        <div class="container">
            <section class="row" id="videos">
                <div class="rounded-corner col-xs-12">
                    <div class="row">
                        <div class="col-xs-12 col-sm-4 curtain-menu-wrapper">
                            <div id="curtain-menu">
                                <div class="morph-shape" data-morph-open="M158.5,0H0v53.1c0,0,19.6-4.6,66-0.2s60.5-3.8,92.5-0.1V0z" data-morph-trans="M158.5,0H0v53.1c0,0,35.4,15.4,82,13.8s76.5-14.1,76.5-14.1V0z">
                                    <svg width="100%" height="100%" viewBox="0 0 150 60" preserveAspectRatio="none">
                                        <path fill="none" d="M158.5,0H0v55.6c20.9-12.8,38.5,19.5,73.5-1.9s73.2-7.2,85,0V0z" />
                                    </svg>
                                </div>
                                <button class="menu__label"><span class="chapter">單元1：認識精品咖啡</span><span class="pull-right"><img src="./assets/images/curtain-arrow.svg" alt=""></span></button>
                                <ul class="menu__inner">
                                    <li><a href="#" id="section1" class="active" title="單元1：認識精品咖啡">單元1：認識精品咖啡</a></li>
                                    <li><a href="#" id="section2" title="單元2：認識精品咖啡">單元2：認識精品咖啡</a></li>
                                    <li><a href="#" id="section3" title="單元3：認識精品咖啡">單元3：認識精品咖啡</a></li>
                                    <li><a href="#" id="section4" title="單元4：認識精品咖啡">單元4：認識精品咖啡</a></li>
                                    <li><a href="#" id="section5" title="單元5：認識精品咖啡">單元5：認識精品咖啡</a></li>
                                    <li><a href="#" id="section6" title="單元6：認識精品咖啡">單元6：認識精品咖啡</a></li>
                                </ul>
                            </div>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci aliquid corporis impedit odio officiis perferendis placeat, reprehenderit temporibus. Ab alias animi consectetur eos explicabo numquam pariatur sint ut voluptatem voluptatibus.
                            </p>
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="false">
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <a role="button" data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                <span class="blue-circle"></span> 1.檸檬氣泡冰咖啡
                                            </a>
                                            <a href="#" class="pull-right launch-vid" data-toggle="modal" data-target="#videoModal" data-video="http://www.youtube.com/embed/vbszCHMtpuY">
                                                <span class="playing-time">0:05:59</span><img src="./assets/images/play_20x20.svg" />
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel">
                                        <div class="panel-body">
                                            程內容說明文字，課程內容說明文字，課程內容說明文字，課程內容說明文字，課程內容說明文字，課程內容說明文字，課程內容說明文字，課程內容說明文字，課程內容說明文字。
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse"  href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                <span class="blue-circle"></span> 2.奶酒咖啡
                                            </a>
                                            <a href="#" class="pull-right launch-vid" data-toggle="modal" data-target="#videoModal" data-video="http://www.youtube.com/embed/DoEBQPa9acg">
                                                <span class="playing-time">0:05:59</span><img src="./assets/images/play_20x20.svg" />
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            程內容說明文字，課程內容說明文字，課程內容說明文字，課程內容說明文字，課程內容說明文字，課程內容說明文字，課程內容說明文字，課程內容說明文字，課程內容說明文字。
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse"  href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                <span class="blue-circle"></span> 3.肯亞冰咖啡
                                            </a>
                                            <a href="#" class="pull-right launch-vid" data-toggle="modal" data-target="#videoModal" data-video="http://www.youtube.com/embed/vbszCHMtpuY">
                                                <span class="playing-time">0:05:59</span><img src="./assets/images/play_20x20.svg" />
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            程內容說明文字，課程內容說明文字，課程內容說明文字，課程內容說明文字，課程內容說明文字，課程內容說明文字，課程內容說明文字，課程內容說明文字，課程內容說明文字。
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse"  href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                                <span class="blue-circle"></span> 4.手沖壺的訣竅
                                            </a>
                                            <a href="#" class="pull-right launch-vid" data-toggle="modal" data-target="#videoModal" data-video="http://www.youtube.com/embed/vbszCHMtpuY">
                                                <span class="playing-time">0:05:59</span><img src="./assets/images/play_20x20.svg" />
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" >
                                        <div class="panel-body">
                                            程內容說明文字，課程內容說明文字，課程內容說明文字，課程內容說明文字，課程內容說明文字，課程內容說明文字，課程內容說明文字，課程內容說明文字，課程內容說明文字。
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
        </div>

    </div>
    <div class="full-container diagonal-bg2 section" data-anchor="equipment-section">
        <div class="container">
            <section class="row" id="equipment" >
                <div class="col-md-offset-1 col-md-10">
                    <div class="content">
                        <h2 class="diagonal orange">購買教材</h2>
                        <ul class="slider autoplay">
                            <li>
                                <div class="product">
                                    <img src="./assets/images/products/product1.png" alt="">
                                    <div class="product-title">kitta咖啡壺</div>
                                    <div class="price">$2,025</div>
                                    <div class="product-action">
                                        <a href="#" class="btn-number" data-type="plus" data-field="quant[1]">+</a>
                                        <input type="text" name="quant[1]" class="input-number" value="1" min="1" max="10000">
                                        <a href="#" class="btn-number" data-type="minus" data-field="quant[1]">-</a>
                                        <a href="#" class='add-to-cart prompt-login'>加入購物車</a>
                                    </div>
                                </div>

                            </li>
                            <li>
                                <div class="product">
                                    <img src="./assets/images/products/product2.png" alt="">
                                    <div class="product-title">kitta咖啡壺</div>
                                    <div class="price">$2,025</div>
                                    <div class="product-action">
                                        <a href="#" class="btn-number" data-type="plus" data-field="quant[2]">+</a>
                                        <input type="text" name="quant[2]" class="input-number" value="1" min="1" max="10000">
                                        <a href="#" class="btn-number" data-type="minus" data-field="quant[2]">-</a>
                                        <a href="#" class='add-to-cart prompt-login'>加入購物車</a>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="product">
                                    <img src="./assets/images/products/product3.png" alt="">
                                    <div class="product-title">kitta咖啡壺</div>
                                    <div class="price">$2,025</div>
                                    <div class="product-action">
                                        <a href="#" class="btn-number" data-type="plus" data-field="quant[3]">+</a>
                                        <input type="text" name="quant[3]" class="input-number" value="1" min="1" max="10000">
                                        <a href="#" class="btn-number" data-type="minus" data-field="quant[3]">-</a>
                                        <a href="#" class='add-to-cart prompt-login'>加入購物車</a>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="product">
                                    <img src="./assets/images/products/product4.png" alt="">
                                    <div class="product-title">kitta咖啡壺</div>
                                    <div class="price">$2,025</div>
                                    <div class="product-action">
                                        <a href="#" class="btn-number" data-type="plus" data-field="quant[4]">+</a>
                                        <input type="text" name="quant[4]" class="input-number" value="1" min="1" max="10000">
                                        <a href="#" class="btn-number" data-type="minus" data-field="quant[4]">-</a>
                                        <a href="#" class='add-to-cart prompt-login'>加入購物車</a>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="product">
                                    <img src="./assets/images/products/product5.png" alt="">
                                    <div class="product-title">kitta咖啡壺</div>
                                    <div class="price">$2,025</div>
                                    <div class="product-action">
                                        <a href="#" class="btn-number" data-type="plus" data-field="quant[5]">+</a>
                                        <input type="text" name="quant[5]" class="input-number" value="1" min="1" max="10000">
                                        <a href="#" class="btn-number" data-type="minus" data-field="quant[5]">-</a>
                                        <a href="#" class='add-to-cart prompt-login'>加入購物車</a>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="product">
                                    <img src="./assets/images/products/product6.png" alt="">
                                    <div class="product-title">kitta咖啡壺</div>
                                    <div class="price">$2,025</div>
                                    <div class="product-action">
                                        <a href="#" class="btn-number" data-type="plus" data-field="quant[6]">+</a>
                                        <input type="text" name="quant[6]" class="input-number" value="1" min="1" max="10000">
                                        <a href="#" class="btn-number" data-type="minus" data-field="quant[6]">-</a>
                                        <a href="#" class='add-to-cart prompt-login'>加入購物車</a>
                                    </div>
                                </div>
                            </li>

                        </ul>

                    </div>
                </div>
            </section>
        </div>
    </div>
    <div class="full-container light-blue section comment-div" data-anchor="comment-section">
        <div class="container">
            <section class="row" id="comment" >
                <div class="col-md-offset-1 col-md-10">
                    <div class="content">
                        <h2 class="diagonal light-blue">討論區</h2>
                        <form class="comment-form">
                            <textarea placeholder="發表言論..." class="form-control" rows="3"></textarea>
                            <input type="submit" class="blue-button prompt-login" value="送出">
                        </form>
                        <ul class="comments-list">
                            <li class="comment">
                                <div class="comment-wrapper">
                                    <div class="comment-left">

                                        <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg"  style="width: 60px; height: 60px;">

                                    </div><!--
                                    --><div class="comment-body">
                                        <h4 class="comment-heading">Anthony</h4>
                                        <p>老師風趣幽默，孩子學習很輕鬆。</p>
                                        <datetime>2017-03-16 17:35</datetime>
                                    </div>
                                </div>
                                <ul class="comments-list nested-comment">
                                    <li class="comment">
                                        <div class="comment-wrapper">
                                            <div class="comment-left">

                                                <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg"  style="width: 60px; height: 60px;">

                                            </div><!--
                                            --><div class="comment-body">
                                                <h4 class="comment-heading">老師回覆：</h4>
                                                <p>喜歡老師帶動氣氛，讓孩子學習意願更高</p>
                                                <datetime>2017-03-16 17:35</datetime>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <li class="comment">
                                <div class="comment-wrapper">
                                    <div class="comment-left">

                                        <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg" style="width: 60px; height: 60px;">

                                    </div><!--
                                    --><div class="comment-body">
                                        <h4 class="comment-heading">劉雯</h4>
                                        <p>孩子在學校英文突飛猛進，說寫樣樣行！謝謝老師</p>
                                        <datetime>2017-03-16 17:35</datetime>
                                    </div>
                                </div>
                            </li>
                            <li class="comment">
                                <div class="comment-wrapper">
                                    <div class="comment-left">

                                        <img class="media-object img-circle" src="./assets/images/avatar/avatar3.jpg" style="width: 60px; height: 60px;">

                                    </div><!--
                                    --><div class="comment-body">
                                        <h4 class="comment-heading">劉雯</h4>
                                        <p>孩子在學校英文突飛猛進，說寫樣樣行！謝謝老師</p>
                                        <datetime>2017-03-16 17:35</datetime>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <p class="text-center"><a href="#" class="blue-btn btn">更多留言</a></p>
                    </div>
                </div>

            </section>

        </div>
    </div>

    <div class="full-container section fp-noscroll" id="rate-container" data-anchor="rate-section">

        <div class="container">
            <section class="row" id="rate" >
                <div class="col-md-offset-1 col-md-10">
                    <div class="content">
                        <p>滿意這次的課程嗎？幫我們打個分數吧！ </p>
                        <div class="right-align">
                            <span class="user-rating"></span>
                            <a href="#" class="blue-button">填寫評論</a>

                        </div>
                    </div>
                </div>
            </section>
        </div>

    </div>

    <div class="full-container section white-bg suggest-div" data-anchor="suggest-section" id="suggest">

        <div class="container" >
            <div class="row">
                <div class="col-md-offset-1 col-md-10">
                    <div class="content">
                        <h2 class="diagonal dull-yellow">也許你會喜歡</h2>

                    </div>

                    <ul class="news-list clearfix">

                        <li class="col-xs-12 col-md-6">
                            <a href="#" class="tag">有感生活</a>
                            <div class="area">
                                <div class="thumb"><a href="#"><img src="./assets/images/post-header.jpg" alt=""></a></div>
                                <div class="content">
                                    <h3 class="title">在家打造你的咖啡館-遇見我的夏日咖啡 單元1</h3>
                                    <div class="row">
                                        <div class="col-xs-3 light-grey">募資預購</div>
                                        <div class="col-xs-7">
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" style="width: 80%;"></div>
                                            </div>
                                        </div>
                                        <div class="col-xs-2 bright-blue">75%</div>
                                    </div>
                                    <div class="desc">
                                        <div class="post-left">
                                            <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg">
                                        </div>
                                        <div class="post-center">
                                            Seki <br>
                                             如是創意咖啡品牌創始人
                                        </div>
                                        <div class="post-right">
                                            <div class="price">$4,900</div>
                                        </div>
                                    </div>
                                    <a href="#" class="yellow-button">閱讀更多</a>
                                </div>
                            </div>
                        </li>
                        <li class="col-xs-12 col-md-6">
                            <a href="#" class="tag">有感生活</a>
                            <div class="area">
                                <div class="thumb"><a href="#"><img src="./assets/images/post-header.jpg" alt=""></a></div>
                                <div class="content">
                                    <h3 class="title">在家打造你的咖啡館-遇見我的夏日咖啡 單元1</h3>
                                    <div class="row">
                                        <div class="col-xs-3 light-grey">募資預購</div>
                                        <div class="col-xs-7">
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" style="width: 80%;"></div>
                                            </div>
                                        </div>
                                        <div class="col-xs-2 bright-blue">75%</div>
                                    </div>
                                    <div class="desc">
                                        <div class="post-left">
                                            <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg">
                                        </div>
                                        <div class="post-center">
                                            Seki <br>
                                             如是創意咖啡品牌創始人
                                        </div>
                                        <div class="post-right">
                                            <div class="price">$4,900</div>
                                        </div>
                                    </div>
                                    <a href="#" class="yellow-button">閱讀更多</a>
                                </div>
                            </div>
                        </li>

                        <li class="col-xs-12 col-md-6">
                            <a href="#" class="tag">有感生活</a>
                            <div class="area">
                                <div class="thumb"><a href="#"><img src="./assets/images/post-header.jpg" alt=""></a></div>
                                <div class="content">
                                    <h3 class="title">在家打造你的咖啡館-遇見我的夏日咖啡 單元1</h3>
                                    <div class="row">
                                        <div class="col-xs-3 light-grey">募資預購</div>
                                        <div class="col-xs-7">
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" style="width: 80%;"></div>
                                            </div>
                                        </div>
                                        <div class="col-xs-2 bright-blue">75%</div>
                                    </div>
                                    <div class="desc">
                                        <div class="post-left">
                                            <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg">
                                        </div>
                                        <div class="post-center">
                                            Seki <br>
                                             如是創意咖啡品牌創始人
                                        </div>
                                        <div class="post-right">
                                            <div class="price">$4,900</div>
                                        </div>
                                    </div>
                                    <a href="#" class="yellow-button">閱讀更多</a>
                                </div>
                            </div>
                        </li>
                        <li class="col-xs-12 col-md-6">
                            <a href="#" class="tag">有感生活</a>
                            <div class="area">
                                <div class="thumb"><a href="#"><img src="./assets/images/post-header.jpg" alt=""></a></div>
                                <div class="content">
                                    <h3 class="title">在家打造你的咖啡館-遇見我的夏日咖啡 單元1</h3>
                                    <div class="row">
                                        <div class="col-xs-3 light-grey">募資預購</div>
                                        <div class="col-xs-7">
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" style="width: 80%;"></div>
                                            </div>
                                        </div>
                                        <div class="col-xs-2 bright-blue">75%</div>
                                    </div>
                                    <div class="desc">
                                        <div class="post-left">
                                            <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg">
                                        </div>
                                        <div class="post-center">
                                            Seki <br>
                                             如是創意咖啡品牌創始人
                                        </div>
                                        <div class="post-right">
                                            <div class="price">$4,900</div>
                                        </div>
                                    </div>
                                    <a href="#" class="yellow-button">閱讀更多</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <p class="button-wrapper row text-center"><a href="#" class="tertiary btn">更多課程</a></p>
        </div>

    </div>
    <div class="full-container section faq-div" data-anchor="faq-section" id="faq">

        <div class="container" >
            <div class="row">
                <div class="col-md-offset-1 col-md-10">
                    <div class="content">
                        <h2><img src="./assets/images/faq.png" alt="" />常見問題</h2>

                    </div>
                    <div class="panel-group row" id="faq-accordion" role="tablist" aria-multiselectable="false">
                        <div class="col-md-6 panel panel-default">
                            <div class="panel-wrap">
                                <div class="panel-heading" role="tab">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" href="#faqOne" aria-expanded="true" aria-controls="faqOne">
                                            上課有次數或期間限制嗎？
                                        </a>

                                    </h4>
                                </div>
                                <div id="faqOne" class="panel-collapse collapse" role="tabpanel">
                                    <div class="panel-body">
                                        錄播課程於購買完成後，會保留在「我的課程」中，並可無限期觀看，就跟買書一樣喔~
                                        直播課程於購買完成後，請務必於指定期間上線參與。除特定課程並於課前公告外，課程結束後不保留上課影片，
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-6 panel panel-default">
                            <div class="panel-wrap">
                                <div class="panel-heading" role="tab">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" href="#faqTwo" aria-expanded="true" aria-controls="faqTwo">
                                            材料購買配送問題
                                        </a>

                                    </h4>
                                </div>
                                <div id="faqTwo" class="panel-collapse collapse" role="tabpanel">
                                    <div class="panel-body">
                                        若您購買的課程有附贈材料，Wakey將依「我的帳戶」中的基本資料如姓名、電話、E-mail與地址進行配送，請務必確認您的資料正確；若因您提供之資料不完全無法完成配送，Wakey不負任何責任；若資料有誤，Wakey有權請您支付額外配送費用。
                                    </div>
                                </div>

                            </div>

                        </div>

                        <div class="col-md-6 panel panel-default">
                            <div class="panel-wrap">
                                <div class="panel-heading" role="tab">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" href="#faqThree" aria-expanded="true" aria-controls="faqThree">
                                            以FB或Google登入之注意事項
                                        </a>

                                    </h4>
                                </div>
                                <div id="faqThree" class="panel-collapse collapse" role="tabpanel">
                                    <div class="panel-body">
                                        以FB或Google登入的學員，請務必在購買課程後完整補充您的基本會員資料，以便Wakey助教和老師與您聯繫！
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-6 panel panel-default">
                            <div class="panel-wrap">
                                <div class="panel-heading" role="tab">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" href="#faqFour" aria-expanded="true" aria-controls="faqFour">
                                            電子發票
                                        </a>

                                    </h4>
                                </div>
                                <div id="faqFour" class="panel-collapse collapse" role="tabpanel">
                                    <div class="panel-body">
                                        Wakey使用電子發票系統，當你購課完成繳費後，發票會在收到銀行端入帳通知後7天（不含例假日）開立，並透過E-mail方式寄送「電子發票」給您。
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 panel panel-default">
                            <div class="panel-wrap">
                                <div class="panel-heading" role="tab">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" href="#faqFive" aria-expanded="true" aria-controls="faqFive">
                                            我要退費可以嗎？
                                        </a>

                                    </h4>
                                </div>
                                <div id="faqFive" class="panel-collapse collapse" role="tabpanel">
                                    <div class="panel-body">
                                        錄播課程：學員購買錄播課程後，七日內「未啟用」該課程，可提出退費申請，經Wakey審核確認始可退費。<br>
                                        直播課程：直播課程由於訂有開課人數門檻與報名截止時間，為保障開課品質與學習成效，於「報名截止日」後無法退費。可退費情形詳見退費說明頁面。
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 panel panel-default">
                            <div class="panel-wrap">
                                <div class="panel-heading" role="tab">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" href="#faqSix" aria-expanded="true" aria-controls="faqSix">
                                            線上講義可以下載嗎？
                                        </a>

                                    </h4>
                                </div>
                                <div id="faqSix" class="panel-collapse collapse" role="tabpanel">
                                    <div class="panel-body">
                                        線上講義原則上僅供線上閱覽，請尊重Wakey與老師們付出的心力，切勿自行重製、改作、散佈等違反智慧財產權之一切行為。
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
            <p class="button-wrapper row text-center"><a href="#" class="blue-btn btn">其它問題</a></p>
        </div>
        <a id='back-to-top' href="#trailer-section" style="margin-right:5%;float:right;"><img src="./assets/images/top.svg" alt=""></a>
    </div>
    <?php include('templates/footer.php'); ?>
</div>


<div class="modal fade worksModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!--
            <div class="modal-header">
                <a href="#" class="close" data-dismiss="modal" aria-hidden="true"><img src="./assets/images/close-search.svg" alt=""></a>
            </div>
            -->
            <div class="modal-body container"></div>
            <!--
            <div class="modal-header">
                <a href="#" class="back-overview"><span class="glyphicon glyphicon-arrow-left"></span></a>
                <h4 class="modal-title">hamonlery</h4>
                <a href="#" class="close" data-dismiss="modal" aria-hidden="true"><img src="./assets/images/close-search.svg" alt=""></a>
            </div>
            <div class="modal-body container">
                <div class="row image-list">
                    <div class="col-md-3">
                        <a href="#" data-album="gsgfdg">
                            <img src="./assets/images/work/album-a1.jpg" alt="">
                        </a>
                    </div>

                    <div class="col-md-3">
                        <a href="#" data-album="ojb">
                            <img src="./assets/images/work/album-b1.jpg" alt="">
                        </a>
                    </div>
                    <div class="col-md-3">
                        <a href="#" data-album="aaaa">
                            <img src="./assets/images/work/album-c1.jpg" alt="">
                        </a>
                    </div>
                </div>

                <div class="row image-slider" id="gsgfdg" style="display:none;">
                    <ul class="slider">
                        <li>
                            <div class="work-img">
                                <img src="./assets/images/work/album-a1.jpg" alt="">
                            </div>
                        </li>
                        <li>
                            <div class="work-img">
                                <img src="./assets/images/work/album-a2.jpg" alt="">
                            </div>
                        </li>
                    </ul>
                    <div class="slider-dots"></div>
                </div>
                <div class="row image-slider" id="ojb" style="display:none;">
                    <ul class="slider">
                        <li>
                            <div class="work-img">
                                <img src="./assets/images/work/album-b1.jpg" alt="">
                            </div>
                        </li>
                    </ul>
                    <div class="slider-dots"></div>
                </div>
                <div class="row image-slider" id="aaaa" style="display:none;">
                    <ul class="slider">
                        <li>
                            <div class="work-img">
                                <img src="./assets/images/work/album-c1.jpg" alt="">
                            </div>
                        </li>
                        <li>
                            <div class="work-img">
                                <img src="./assets/images/work/album-c2.jpg" alt="">
                            </div>
                        </li>
                        <li>
                            <div class="work-img">
                                <img src="./assets/images/work/album-c3.jpg" alt="">
                            </div>
                        </li>
                        <li>
                            <div class="work-img">
                                <img src="./assets/images/work/album-c4.jpg" alt="">
                            </div>
                        </li>

                    </ul>
                    <div class="slider-dots"></div>
                </div>

            </div>-->
        </div>
    </div>
</div>
<!--
<div class="modal fade worksModal" id="worksModal-2" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <a href="#" class="back-overview pull-left"><span class="glyphicon glyphicon-arrow-left"></span></a>
                <h4 class="modal-title">SamMee514</h4>
                <a href="#" class="close" data-dismiss="modal" aria-hidden="true"><img src="./assets/images/close-search.svg" alt=""></a>
            </div>
            <div class="modal-body container">
                <div class="row image-list">
                    <div class="col-md-3">
                        <a href="#">
                            <img src="./assets/images/work/114F1511871627431-2image-1.jpg" alt="">
                        </a>
                    </div>

                    <div class="col-md-3">
                        <a href="#">
                            <img src="./assets/images/work/114F15117356013582-2image-1.jpg" alt="">
                        </a>
                    </div>
                    <div class="col-md-3">
                        <a href="#">
                            <img src="./assets/images/work/114F15117356013582-2image-1.jpg" alt="">
                        </a>
                    </div>
                </div>

                <div class="row image-slider">
                    <ul class="slider">
                        <li>
                            <div class="work-img">
                                <img src="./assets/images/work/114F1511871627431-2image-1.jpg" alt="">
                            </div>
                        </li>
                        <li>
                            <div class="work-img">
                                <img src="./assets/images/work/114F1511871627431-2image-1.jpg" alt="">
                            </div>
                        </li>
                        <li>
                            <div class="work-img">
                                <img src="./assets/images/work/114F1511871627431-2image-1.jpg" alt="">
                            </div>
                        </li>
                    </ul>
                </div>

            </div>

        </div>
    </div>
</div>
-->


<div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-alignment-center">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal" aria-hidden="true"><img src="./assets/images/close-search.svg" alt=""></a>
                <div class="modal-body">
                    <div class="videoWrapper">
                        <iframe width="100%" src=""></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade user-modal" id="userModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-alignment-center">
            <div class="modal-content">
                <div class="modal-body">
                    <a href="#" class="close" data-dismiss="modal" aria-hidden="true"><img src="./assets/images/close-login.svg" alt=""></a>
                    <div class="row">
                        <div class="col-sm-6">

                            <div class="login-side">
                                <div class="login-wrapper">
                                    <h2>歡迎來到Wakey</h2>
                                    <p>微課讓線上學習更貼近每個人的真實生活！<br />
                                        利用片段時間學習，累積屬於你的價值！
                                    </p>
                                </div>

                                <div class="password-recovery-wrapper" style="display:none">
                                    <h2>找回密碼 <a href="#" class="close close-recovery" aria-hidden="true"><img src="./assets/images/close-search.svg" alt=""></a></h2>
                                    <form class="password-recovery-form">
                                        <h4>別擔心，我們將會協助您取回密碼。</h4>
                                        <div class="form-group clearfix">
                                            <div class="col-xs-12 col-sm-7 ">
                                                <input type="text" class="form-control" id="recoveryPhone" name="recoveryPhone" placeholder="手機號碼">
                                            </div>
                                            <div class="col-xs-12 col-sm-5  no-pad-left-on-sm">
                                                <a href="#" class="blue-button sendcode">
                                                    <span class="original">發送驗證碼</span>
                                                    <span class="pending" style="display:none;">重新發送(<span class="second"></span>)</span>
                                                </a>
                                            </div>


                                        </div>
                                        <div class="form-group clearfix">
                                            <div class="col-sm-12">
                                                <input type="text" class="form-control" id="recoveryCode" name="recoveryCode" placeholder="輸入驗證碼">
                                            </div>
                                        </div>

                                        <div class="form-group clearfix">
                                            <div class="col-sm-12">
                                                <input type="password" class="form-control" id="recoveryPassword" name="recoveryPassword" placeholder="新密碼">
                                            </div>

                                        </div>

                                        <div class="form-group clearfix">
                                            <div class="col-sm-12">
                                                <input type="password" class="form-control" id="recoveryConfirmPassword" name="recoveryConfirmPassword" placeholder="確認密碼">
                                            </div>
                                        </div>
                                        <button type="submit" class="blue-button">送出</button>
                                    </form>
                                </div>

                            </div>

                        </div>
                        <div class="col-sm-6">
                            <div class="login-form-wrapper">
                                <h2>登入</h2>
                                <form class="login-form">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="loginID" name="loginID" placeholder="手機或電子信箱">
                                    </div>

                                    <div class="form-group">
                                        <input type="password" class="form-control" id="loginPw" name="loginPw" placeholder="密碼">
                                    </div>

                                    <button type="submit" class="blue-button pull-right">登入</button>
                                    <a href="#" class="forget-pw">忘記密碼？</a>
                                </form>
                                <a href="#" class="facebook-login"><img src="./assets/images/facebook-login.svg" alt=""></a>
                                <a href="#" class="google-login"><img src="./assets/images/google-login.svg" alt=""></a>
                                <a href="register.php" class="go-register pull-right">快速註冊</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="search-overlay">
    <a href="#" class="close-search"><img src="./assets/images/close-search.svg" alt=""></a>
    <div class="vertical-alignment-helper text-center">
        <div class="search-wrapper vertical-alignment-center">
            <form class="search-form">
                <input type="text" value="" class="search-text" placeholder="查詢關鍵字...">
                <input type="image" src="./assets/images/search.svg"  />
            </form>
        </div>
    </div>
</div>
<ul class="mobile-footer-nav">
    <li><a href="#" class="prompt-login">
            <img src="./assets/images/course-mobile.svg" alt="">
            我的課程
        </a>
    </li>
    <li><a href="#" class="prompt-login">
            <img src="./assets/images/cart-mobile.svg" alt="">
            購物車
        </a>
    </li>
    <li class="dropup">
        <a href="#" data-toggle="dropdown" class="prompt-login">
            <img src="./assets/images/account-mobile.svg" alt="">
            我的帳戶
        </a>
    </li>
</ul>
<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '298301200659355',
            cookie     : true,
            xfbml      : true,
            version    : 'v2.11'
        });

        FB.AppEvents.logPageView();

    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/zh_TW/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<script src="https://apis.google.com/js/platform.js?onload=onLoadCallback" async defer></script>
<script src="https://code.jquery.com/jquery-2.1.4.min.js"
        integrity="sha384-R4/ztc4ZlRqWjqIuvf6RX5yb/v90qNGx6fS48N0tRxiGkqveZETq72KgDVJCp2TC
sha256-8WqyJLuWKRBVhxXIL1jBDD7SDxU936oZkCnxQbWwJVw="
        crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="assets/js/jquery.js"><\/script>')</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script src="assets/js/snap.svg.min.js"></script>
<script src="assets/js/classie.min.js?v=<?php echo time(); ?>"></script>
<script src="assets/js/bootstrap.min.js?v=<?php echo time(); ?>"></script>
<script src="assets/js/slick.min.js"></script>
<script src="assets/js/jquery.SuperSlide.2.1.1.js"></script>
<script src="assets/js/jquery.starRating.js?v=<?php echo time(); ?>"></script>
<script src="assets/js/jquery.easings.min.js"></script>
<script src="assets/js/scrolloverflow.min.js"></script>
<script src="assets/js/jquery.fullpage.min.js"></script>
<script src="assets/js/main.js?v=<?php echo time(); ?>"></script>
</body>
</html>