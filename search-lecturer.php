<body class="profile-body">
<?php include('templates/profile-header.php'); ?>
<?php include('templates/nav-profile.php'); ?>
<div class="profile">
    <div class="full-container section lecturers-top">
        <div class="title yellow">超優師資</div>
        <p>
            Wakey用心尋找具有熱情的老師，並且燃燒自己協助老師規劃課程，<br>
            期望讓學員隔著時空的距離也能感受到老師們彭湃的靈魂。

        </p>
    </div>
</div>
<div class="courses-section container">
    <div class="row mt-60">
        <div class="result-keyword col-md-6">搜尋：蔡家尹</div>
        <div class="pull-right col-xs-12 col-md-3">
            <form class="search-item">
                <div class="form-group has-feedback">
                    <input type="text" placeholder="搜尋老師" class="form-control">
                    <span class="glyphicon glyphicon-search form-control-feedback"></span>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <ul class="row news-list lecturer-news-list">
                <?php for ($x = 0; $x <= 1; $x++) { ?>
                    <li class="col-xs-12 col-md-6 col-lg-3 lecturer-card">
                        <div class="area content thumb">
                            <img src="./assets/images/lecturer<?php echo rand(1, 3); ?>.jpg" alt="">

                            <a href="#">
                            </a>
                        </div>
                        <div class="lecture-info">

                            <div class="name">Grace蔡佳穎</div>
                        </div>
                    </li>
                <?php } ?>
            </ul>


        </div>
    </div>

</div>

<?php include('templates/footer.php'); ?>

<div class="search-overlay">
    <a href="#" class="close-search"><img src="./assets/images/close-search.svg" alt=""></a>
    <div class="vertical-alignment-helper text-center">
        <div class="search-wrapper vertical-alignment-center">
            <form class="search-form">
                <input type="text" value="" class="search-text" placeholder="查詢關鍵字...">
                <input type="image" src="./assets/images/search.svg"  />
            </form>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-2.1.4.min.js"
        integrity="sha384-R4/ztc4ZlRqWjqIuvf6RX5yb/v90qNGx6fS48N0tRxiGkqveZETq72KgDVJCp2TC
sha256-8WqyJLuWKRBVhxXIL1jBDD7SDxU936oZkCnxQbWwJVw="
        crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="assets/js/jquery.js"><\/script>')</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script src="assets/js/bootstrap.min.js?v=<?php echo time(); ?>"></script>
<script src="assets/js/jquery.twzipcode.min.js"></script>
<script src="assets/js/account.js?v=<?php echo time(); ?>"></script>
</body>
</html>