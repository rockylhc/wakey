<body class="profile-body">
<?php include('templates/profile-header.php'); ?>
<?php include('templates/nav-profile.php'); ?>

<div class="profile-course-section container">
    <div class="row">
        <div class="col-sm-4 col-md-4 col-lg-3 side-nav">
            <div class="profile-info">
                <img src="./assets/images/user-photo.jpg" class="img-circle" alt="">
                <div class="realname">
                    昭渝
                </div>
            </div>

            <ul class="course-nav">
                <li><a href="#" class="active">課程訂單</a></li>
                <li><a href="profile-merchant.php">課程商品訂單</a></li>
                <li><a href="company-member.php">企業會員訂單</a></li>
            </ul>
        </div>
        <div class="col-sm-8 col-md-8 col-lg-9 form-wrapper">
            <div class="h4">課程訂單</div>
            <div>
                <div class="row">
                    <ul class="pagination col-md-6">
                        <li class="active"><a href="#pending" role="tab" >未付款</a></li>
                        <li><a href="#paid" role="tab" >已付款</a></li>
                        <li><a href="#cancelled" role="tab">已取消</a></li>
                    </ul>
                    <div class="pull-right col-md-6">
                        <form class="search-item">
                            <div class="form-group has-feedback">
                                <input type="text" placeholder="搜尋課程" class="form-control">
                                <span class="glyphicon glyphicon-search form-control-feedback"></span>
                            </div>
                        </form>
                    </div>
                </div>


                <!--<div class="tab-content">-->
                <div>
                    <!--<div role="tabpanel" class="tab-pane active" id="pending">-->
                    <div id="pending">
                        <div class="alert alert-danger in"><img src="./assets/images/error-icon.svg" alt=""> 未付款 </div>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>課程名稱</th>
                                        <th>原價</th>
                                        <th>優惠價</th>
                                        <th>創建時間</th>
                                        <th>操作</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Unity 遊戲開發- [進階篇] 打造堆疊遊戲技巧分析一個遊戲的完成</td>
                                        <td>$1030</td>
                                        <td>599</td>
                                        <td>2017-07-14 14:23:45</td>
                                        <td>
                                            <a href="#" class="pay-btn">付款</a>
                                            <a href="#" class="glyphicon glyphicon-trash remove-btn"></a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Python 資料分析＆機器學習入門</td>
                                        <td>$550</td>
                                        <td>599</td>
                                        <td>2017-07-14 14:23:45</td>
                                        <td>
                                            <a href="#" class="pay-btn">付款</a>
                                            <a href="#" class="glyphicon glyphicon-trash remove-btn"></a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!--<div role="tabpanel" class="tab-pane" id="paid">-->
                    <div id="paid">
                        <div class="alert alert-info in"><img src="./assets/images/check-icon.svg" alt=""> 已付款 </div>
                        <div class="table-responsive">
                            <table class="table zebra">
                                <thead>
                                <tr>
                                    <th>課程名稱</th>
                                    <th>原價</th>
                                    <th>優惠價</th>
                                    <th>創建時間</th>
                                    <th>操作</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Unity 遊戲開發- [進階篇] 打造堆疊遊戲技巧分析一個遊戲的完成</td>
                                    <td>$1030</td>
                                    <td>599</td>
                                    <td>2017-07-14 14:23:45</td>
                                    <td>
                                        <a href="#" class="open-btn toggle-btn">查看</a>
                                        <a href="#" class="close-btn toggle-btn" style="display:none;">關閉</a>
                                        <a href="#" class="refund-btn">申請退款</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5" class="collapsed">
                                        <div>
                                            <p>訂單編號：112233445566<br>付款方式：信用卡</p>
                                        </div>

                                    </td>
                                </tr>

                                <tr>
                                    <td>Python 資料分析＆機器學習入門</td>
                                    <td>$550</td>
                                    <td>599</td>
                                    <td>2017-07-14 14:23:45</td>
                                    <td>
                                        <a href="#" class="open-btn toggle-btn">查看</a>
                                        <a href="#" class="close-btn toggle-btn" style="display:none;">關閉</a>
                                        <a href="#" class="refund-btn">申請退款</a>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="5" class="collapsed">
                                        <div>
                                            <p>訂單編號：112233445566<br>付款方式：信用卡</p>
                                        </div>

                                    </td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!--<div role="tabpanel" class="tab-pane" id="cancelled">-->
                    <div id="cancelled">
                        <div class="alert alert-cancelled in"><img src="./assets/images/cancelled-icon.svg" alt=""> 已取消訂單 </div>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>課程名稱</th>
                                    <th>原價</th>
                                    <th>優惠價</th>
                                    <th>創建時間</th>
                                    <th>操作</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Unity 遊戲開發- [進階篇] 打造堆疊遊戲技巧分析一個遊戲的完成</td>
                                    <td>$1030</td>
                                    <td>599</td>
                                    <td>2017-07-14 14:23:45</td>
                                    <td>
                                        <a href="#" class="glyphicon glyphicon-trash remove-btn"></a>
                                    </td>
                                </tr>

                                <tr>
                                    <td>Python 資料分析＆機器學習入門</td>
                                    <td>$550</td>
                                    <td>599</td>
                                    <td>2017-07-14 14:23:45</td>
                                    <td>
                                        <a href="#" class="glyphicon glyphicon-trash remove-btn"></a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<ul class="mobile-footer-nav">
    <li><a href="#">
            <img src="./assets/images/course-mobile.svg" alt="">
            我的課程
        </a>
    </li>
    <li><a href="#" data-offcanvas-toggle="cart">
            <img src="./assets/images/cart-mobile.svg" alt="">
            購物車
        </a>
    </li>
    <li class="dropup">
        <a href="#" data-toggle="dropdown">
            <img src="./assets/images/account-mobile.svg" alt="">
            我的帳戶
        </a>

        <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
            <li><a href="#">我的帳戶</a></li>
            <li><a href="#">購買紀錄</a></li>
            <li><a href="#">登出</a></li>
        </ul>

    </li>
</ul>
<?php include('templates/footer.php'); ?>

<div class="search-overlay">
    <a href="#" class="close-search"><img src="./assets/images/close-search.svg" alt=""></a>
    <div class="vertical-alignment-helper text-center">
        <div class="search-wrapper vertical-alignment-center">
            <form class="search-form">
                <input type="text" value="" class="search-text" placeholder="查詢關鍵字...">
                <input type="image" src="./assets/images/search.svg"  />
            </form>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-2.1.4.min.js"
        integrity="sha384-R4/ztc4ZlRqWjqIuvf6RX5yb/v90qNGx6fS48N0tRxiGkqveZETq72KgDVJCp2TC
sha256-8WqyJLuWKRBVhxXIL1jBDD7SDxU936oZkCnxQbWwJVw="
        crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="assets/js/jquery.js"><\/script>')</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script src="assets/js/bootstrap.min.js?v=<?php echo time(); ?>"></script>
<script src="assets/js/account.js?v=<?php echo time(); ?>"></script>
</body>
</html>