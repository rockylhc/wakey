<body class="profile-body">
<?php include('templates/profile-header.php'); ?>
<?php include('templates/nav-profile.php'); ?>
<div class="profile">
    <div class="full-container section courses-top">
        <div class="title">有感生活</div>
    </div>
</div>
<div class="courses-section container">
    <div class="row">
        <div class="col-sm-4 col-md-push-1 col-md-3 col-lg-push-1 col-lg-2">
            <div class="panel-group">
                <div class="panel panel-default">
                    <h4 class="panel-title">
                        課程分類
                    </h4>
                    <div class="panel-body">
                        <a href="#" class="active">所有課程</a>
                        <a href="#">有感生活</a>
                        <a href="#">多一份收入</a>
                        <a href="#">職場生活</a>
                    </div>
                </div>
                <div class="panel panel-default">
                    <h4 class="panel-title">
                        學習類型
                    </h4>
                    <div class="panel-body">
                        <a href="#" class="active">全部類型</a>
                        <a href="#">線上</a>
                        <a href="#">直播</a>
                    </div>
                </div>
                <div class="panel panel-default">
                    <h4 class="panel-title">
                        排序方式
                    </h4>
                    <div class="panel-body">
                        <a href="#" class="active">近期上架</a>
                        <a href="#">人氣推薦</a>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-sm-8 col-md-push-1 col-md-8 col-lg-push-1 col-lg-9">
            <ul class="row news-list">
                <?php for ($x = 0; $x <= 12; $x++) { ?>
                <li class="col-xs-12 col-md-6 col-lg-4">
                    <a href="#" class="tag">有感生活</a>
                    <div class="area">
                        <div class="thumb"><a href="#"><img src="./assets/images/post-header.jpg" alt=""></a></div>
                        <div class="content">
                            <h3 class="title">在家打造你的咖啡館-遇見我的夏日咖啡 單元1</h3>
                            <div class="desc">
                                <div class="post-left">
                                    <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg">
                                </div>
                                <div class="post-center">
                                    Seki <br>
                                     如是創意咖啡品牌創始人
                                </div>
                                <div class="post-right">
                                    <div class="price">$4,900</div>
                                </div>
                            </div>
                            <a href="#" class="yellow-button">閱讀更多</a>
                        </div>
                    </div>
                </li>
                <?php } ?>
            </ul>

            <div class="row text-center">
                <nav aria-label="Page navigation" class="pagination-wrapper">
                    <ul class="pagination clearfix">
                        <li>
                            <a href="#" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>
                        <li><a href="#" class="active">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li>
                            <a href="#" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>

</div>

<?php include('templates/footer.php'); ?>

<div class="search-overlay">
    <a href="#" class="close-search"><img src="./assets/images/close-search.svg" alt=""></a>
    <div class="vertical-alignment-helper text-center">
        <div class="search-wrapper vertical-alignment-center">
            <form class="search-form">
                <input type="text" value="" class="search-text" placeholder="查詢關鍵字...">
                <input type="image" src="./assets/images/search.svg"  />
            </form>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-2.1.4.min.js"
        integrity="sha384-R4/ztc4ZlRqWjqIuvf6RX5yb/v90qNGx6fS48N0tRxiGkqveZETq72KgDVJCp2TC
sha256-8WqyJLuWKRBVhxXIL1jBDD7SDxU936oZkCnxQbWwJVw="
        crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="assets/js/jquery.js"><\/script>')</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script src="assets/js/bootstrap.min.js?v=<?php echo time(); ?>"></script>
<script src="assets/js/jquery.twzipcode.min.js"></script>
<script src="assets/js/account.js?v=<?php echo time(); ?>"></script>
</body>
</html>