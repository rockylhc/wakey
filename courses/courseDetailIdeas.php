<style type="text/css">
    /* 本例子css */
    .layui-layer{
        top:3% !important;
        position: fixed !important;
    }
    #layer-ideas-overlay{
        background-color: #2bc0be!important;
        width:auto;
    }
    .layer-ideas-slider {
        height: auto;
        overflow: hidden;
        position: relative;
        /*border: 1px solid #ddd;*/
        text-align:center;
    }

    .layer-ideas-slider .hd {
        /*height: 15px;*/
        /*overflow: hidden;*/
        /*position: absolute;*/
        /*right: 5px;*/
        /*bottom: 5px;*/
        /*z-index: 1;*/
        text-align:center;
        /*display: inline-block;*/
        margin-top: 10px;
        margin-bottom: 10px;
    }

    .layer-ideas-slider .hd ul {
        overflow: hidden;
        /*zoom: 1;*/
        /*float: left;*/
        text-align:center;
        display:inline-block;
    }

    .layer-ideas-slider .hd ul li {
        float: left;
        margin-right: 2px;
        /*width: 15px;*/
        /*height: 15px;*/

        line-height: 14px;
        text-align: center;
        background: transparent;
        cursor: pointer;
        font-size:20px;
        padding:10px;
        color: #fff;
        font-weight: bold;
    }

    .layer-ideas-slider .hd ul li.on {
        background: transparent;
        color: red;
    }

    .layer-ideas-slider .bd {
        position: relative;
        height: 100%;
        z-index: 0;
        display: inline-block;
        padding-left:30px;
        padding-right:30px;
    }

    .layer-ideas-slider .bd ul {
        text-align:center;
    }

    .layer-ideas-slider .bd li {
        zoom: 1;
        vertical-align: middle;
    }

    .layer-ideas-slider .bd img {
        width: 100%;
        height: auto;
        display: block;
    }
    /* 下面是前/后按钮代码，如果不需要删除即可 */

    .layer-ideas-slider .prev,
    .layer-ideas-slider .next {
        position: absolute;
        left: 3%;
        top: 48%;
        margin-top: -50px;
        display: block;
        width: 32px;
        height: 40px;
        /*filter: alpha(opacity=50);*/
        /*opacity: 0.5;*/
    }

    .layer-ideas-slider .prev {
        /*left: auto;*/
        left: -10px;
        background-position: 8px 5px;
    }

    .layer-ideas-slider .next {
        left: auto;
        right: 10px;
        background-position: 8px 5px;
    }

    .layer-ideas-slider .prev:hover,
    .layer-ideas-slider .next:hover {
        filter: alpha(opacity=100);
        opacity: 1;
    }

    .layer-ideas-slider .prevStop {
        display: none;
    }

    .layer-ideas-slider .nextStop {
        display: none;
    }
</style>
<div data-block="overlay" data-block-type="course-ideas"
     style="
width: 1000px;
background-color: #2e3d49!important;
overflow-y:auto;
">
    <div style="top: 10px;right: 10px;position: absolute; z-index: 100;">
        <a type="button" href="#" data-dismiss="modal">
            <i class="mdi mdi-window-close" style="font-size:30px; color:#FFF; cursor:pointer; "></i>
        </a>
    </div>
    <div data-block="list" data-block-type="course-ideas"
         data-student-id="1"
         style="
    padding:20px;
    ">

        <div data-rows="ideas">
            <div class="layui-row" style="">
                <div class="layui-col-md3" style=" padding: 10px;">
                    <div class="img" style="
            border: 1px solid #ccc;
            ">
                        <i class="mdi mdi-folder-open text-30 abs" style="color:#888"></i>
                        <img src="http://via.placeholder.com/350x150/b7b7b7"
                             data-btn="load-course-ideas-data"
                             data-id="1194"
                             class=""
                             style="
                cursor:pointer;
                width:100%;
                height:auto;"
                        >
                    </div>
                    <div class="topic text-center text-16 p-10" style="
            word-wrap: break-word;
            ">
                        data1            </div>
                </div>
                <div class="layui-col-md3" style=" padding: 10px;">
                    <div class="img" style="
            border: 1px solid #ccc;
            ">
                        <i class="mdi mdi-folder-open text-30 abs" style="color:#888"></i>
                        <img src="http://via.placeholder.com/350x150/888888"
                             data-btn="load-course-ideas-data"
                             data-id="1184"
                             class=""
                             style="
                cursor:pointer;
                width:100%;
                height:auto;"
                        >
                    </div>
                    <div class="topic text-center text-16 p-10" style="
            word-wrap: break-word;
            ">
                        data            </div>
                </div>
                <div class="layui-col-md3" style=" padding: 10px;">
                    <div class="img" style="
            border: 1px solid #ccc;
            ">
                        <i class="mdi mdi-folder-open text-30 abs" style="color:#888"></i>
                        <img src="http://via.placeholder.com/350x150/1f1f1f"
                             data-btn="load-course-ideas-data"
                             data-id="1163"
                             class=""
                             style="
                cursor:pointer;
                width:100%;
                height:auto;"
                        >
                    </div>
                    <div class="topic text-center text-16 p-10" style="
            word-wrap: break-word;
            ">
                        data            </div>
                </div>
                <div class="layui-col-md3" style=" padding: 10px;">
                    <div class="img" style="
            border: 1px solid #ccc;
            ">
                        <i class="mdi mdi-folder-open text-30 abs" style="color:#888"></i>
                        <img src="http://via.placeholder.com/350x150"
                             data-btn="load-course-ideas-data"
                             data-id="1154"
                             class=""
                             style="
                cursor:pointer;
                width:100%;
                height:auto;"
                        >
                    </div>
                    <div class="topic text-center text-16 p-10" style="
            word-wrap: break-word;
            ">
                        data            </div>
                </div>

            </div>
        </div>
        <div style="clear:both;"></div>
        <div data-paginator="ideas" style="padding: 10px; text-align:center;">

            <a type="button"
               nouse-class="layui-btn layui-btn-primary btn-no-bd text-18"
               class="text-18"
               data-topic="1"
               data-path="/?school_id=114&course_id=3484&student_id=1&__orderBy=id DESC&page=1&m=Html&c=CourseIdeas&a=courseDetailIdeas"
               data-paging="1"
               data-btn="load-course-ideas"
            >
                1    </a>


        </div>

    </div>
    <div data-block="list" data-block-type="course-ideas-data"
         data-student-id="1"
         nouse-style="
    padding:20px;
    ">
    </div>
</div>