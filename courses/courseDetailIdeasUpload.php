<style type="text/css">
    .layui-layer{
        top:17% !important;
        position: fixed !important;
    }
    .layui-layer-content, .overlay-inner{

        height: auto !important;
    }

    @media(min-width:768px){
        .layui-layer-content,.overlay-inner{

        }
    }
</style>
<div data-block="overlay" data-block-type="course-ideas" style="background-color: #FFF!important;overflow-y:auto;">
    <div class="overlay-inner">

        <div style="
        float:left;padding-top: 15px;padding-left: 15px;font-size: 18px;
        ">
            上傳文件        </div>
        <div style="
        float:right;padding-top:10px;padding-right: 10px;
        ">
            <a type="button" href="#" data-dismiss="modal">
                <i class="mdi mdi-window-close" style="
                font-size:30px; color:#666; cursor:pointer; "></i>
            </a>
        </div>
        <div class="layui-row" data-block="" style="
        padding: 15px;
        padding-bottom: 5px;
        ">
            <div class="layui-col-xs2" style="
                padding-top: 10px;
            ">
                <iframe id="" class="" src="https://wakeydev.eletang.com.tw/?m=Html&c=CourseIdeas&a=courseDetailIdeasUploadButton&__params=ZGF0YS11cGxvYWQtdHlwZT1zdHVkZW50LWNvdXJzZS1pZGVhcyZzY2hvb2xfaWQ9MTE0JmNvdXJzZV9pZD0zNDk0" style="width:110px;height:50px;" marginheight="0" marginwidth="0" frameborder="0" scrolling="no" allowtransparency="true"></iframe>

            </div>
            <div class="layui-col-xs10 pt-20 pl-10" style="

            ">
                <div style="color:#666;">
                    檔案格式：pdf、doc、docx、xls、xlsx、pptx、ppt、jpg、png、gif                </div>
            </div>
        </div>
        <div
            style="
        border:1px solid #ccc;
        border-radius:8px;
        margin-left: 15px;
        margin-right: 15px;
        ">
            <!-- -->
            <div class="layui-row" data-block="" style="
            padding: 10px 5px;
            background: #d2d2d2;
            border-top-left-radius:8px;
            border-top-right-radius:8px;
            ">
                <div class="layui-col-xs7" style="
                padding-left:10px; text-align:left;">
                    檔案名稱                </div>
                <div class="layui-col-xs1" style="
                padding-left:10px; text-align:left;">
                    格式                </div>
                <div class="layui-col-xs3" style="
                padding-left:10px; text-align:left;">
                    大小                </div>
                <div class="layui-col-xs1" style="
                padding-left:10px; text-align:left;">
                </div>
            </div>
            <!-- -->
            <div data-block="list" data-block-type="course-ideas">


            </div>
        </div>
        <!-- -->
    </div>
    <div style="clear:both; margin-top: 20px;"></div>
    <div data-loading="overlay" style="
display:none;
position: absolute;
z-index:90000000;
/*background: rgba(101, 101, 101, 0.2);*//* background-color:rgba(255,255,255,0.8); *//*background:rgba(0,0,0,0.8);*/
width: 100%;
height: 100%;
left: 0%;
top: 0%;
background: rgba(255,255,255,0.6);
">
        <div style="
    margin: 0 auto;
    text-align: center;
    /* top: 40%; */
    height: 100%;
    top: 40%;
    position: absolute;
    left: 47%;
    ">
            <img src="./Public/images/loading/default.svg" style="
        width:40px;
        height:40px;
        ">
        </div>
    </div>
</div>

</div>

<div style="display:none;">
    <input type="text"
           data-upload-type="student-course-ideas"
           data-field="input-ref-type"
           value=""
    />
    <input type="text"
           data-upload-type="student-course-ideas"
           data-field="input-ref-id"
           value=""
    />
    <button type="button"
            data-upload-type="student-course-ideas"
            data-callback=""
            data-course-id="3494">
    </button>
</div>

<script>
    $(function() {
        $block = $('div[data-block="overlay"][data-block-type="course-ideas"]');
        $block.off('input', '*[data-input="topic"]');
        $block.on('input', '*[data-input="topic"]', function(e) {
            var $this = $(e.target);
            var id = $this.attr('data-id');
            var field_name = $this.attr('data-field');
            var field_value = $this.val();
            $.ajax({
                url: '?m=Api&c=CourseIdeas&a=updateTopic',
                type: "post",
                data: {
                    'id':id,
                    'field_value':field_value,
                },
                dataType: 'json',
                async: true,
                success: function(data) {
                    if(data.status == '1'){
                    }
                },
                error: function(data) {
                }
            });
        });
    });
</script>