<style type="text/css">
    /* 本例子css */
    .layui-layer{
        top:3% !important;
        position: fixed !important;
    }
    #layer-ideas-overlay{
        background-color: #2bc0be!important;
        width:auto;
    }
    .layer-ideas-slider {
        height: auto;
        overflow: hidden;
        position: relative;
        /*border: 1px solid #ddd;*/
        text-align:center;
    }

    .layer-ideas-slider .hd {
        /*height: 15px;*/
        /*overflow: hidden;*/
        /*position: absolute;*/
        /*right: 5px;*/
        /*bottom: 5px;*/
        /*z-index: 1;*/
        text-align:center;
        /*display: inline-block;*/
        margin-top: 10px;
        margin-bottom: 10px;
    }

    .layer-ideas-slider .hd ul {
        overflow: hidden;
        /*zoom: 1;*/
        /*float: left;*/
        text-align:center;
        display:inline-block;
    }

    .layer-ideas-slider .hd ul li {
        float: left;
        margin-right: 2px;
        /*width: 15px;*/
        /*height: 15px;*/

        line-height: 14px;
        text-align: center;
        background: transparent;
        cursor: pointer;
        font-size:20px;
        padding:10px;
        color: #fff;
        font-weight: bold;
    }

    .layer-ideas-slider .hd ul li.on {
        background: transparent;
        color: red;
    }

    .layer-ideas-slider .iteams {
        position: relative;
        width:100%;
        z-index: 0;
        display: inline-block;
        padding-left:30px;
        padding-right:30px;
    }

    .layer-ideas-slider .iteams ul {
        text-align:center;
    }

    .layer-ideas-slider .iteams li {
        zoom: 1;
        vertical-align: middle;
    }

    .layer-ideas-slider .iteams img {
        width: 100%;
        height: auto;
        display: block;
    }
    /* 下面是前/后按钮代码，如果不需要删除即可 */

    .layer-ideas-slider .prev,
    .layer-ideas-slider .next {
        position: absolute;
        left: 3%;
        top: 48%;
        margin-top: -50px;
        display: block;
        width: 32px;
        height: 40px;
        /*filter: alpha(opacity=50);*/
        /*opacity: 0.5;*/
    }

    .layer-ideas-slider .prev {
        /*left: auto;*/
        left: -10px;
        background-position: 8px 5px;
    }

    .layer-ideas-slider .next {
        left: auto;
        right: 10px;
        background-position: 8px 5px;
    }

    .layer-ideas-slider .prev:hover,
    .layer-ideas-slider .next:hover {
        filter: alpha(opacity=100);
        opacity: 1;
    }

    .layer-ideas-slider .prevStop {
        display: none;
    }

    .layer-ideas-slider .nextStop {
        display: none;
    }
</style>
<div style="
width: 100%;
">
    <div style="top: 10px;left: 10px;position: absolute; z-index: 100;">
        <a type="button" data-btn="back-to-course-ideas"
           style="color:#FFF;"
        >
            <i class="mdi mdi-arrow-left text-30"></i>
        </a>
    </div>
    <div style="top: 10px;right: 10px;position: absolute; z-index: 100;">
        <a type="button" href="#" data-dismiss="modal">
            <i class="mdi mdi-window-close" style="font-size:30px; color:#FFF; cursor:pointer; "></i>
        </a>
    </div>
    <div data-rows="ideas-data">
        <div class="layui-row" data-block="" style="
        padding: 20px;
        ">
            <div class="layui-col-md6" style="
            padding-right:10px; text-align:right;">
                <div data-text="student-name" style="
                font-size: 18px;
                font-weight: bold;
                color: #FFF;
                ">
                    TataYang2                </div>
            </div>
            <div class="layui-col-md6" style="
            padding-left:10px; text-align:left;">
                <div data-text="file-name" style="
                font-size: 18px;
                font-weight: bold;
                color: #FFF;
                ">
                    data1
                </div>
            </div>
        </div>
        <div id="ideas-data-carousel" class="layer-ideas-slider">
            <div class="iteams">
                <ul>
                    <li>
                        <img src="http://via.placeholder.com/350x150/f3f3f3" data-btn="show-ideas" data-id="4764" class="" style="
                        cursor:pointer;
                        width:100%;
                        height:auto;">
                    </li>
                    <li>
                        <img src="http://via.placeholder.com/350x150/e6e6e6" data-btn="show-ideas" data-id="4774" class="" style="
                        cursor:pointer;
                        width:100%;
                        height:auto;">
                    </li>
                </ul>
            </div>
            <div class="hd">
                <ul>
                    <li class="">1</li>
                    <li class="">2</li>
                </ul>
            </div>
            <a class="prev" href="javascript:void(0)">
                <i class="mdi mdi-menu-left" style="font-size:55px; color:rgba(0,0,0,0.8);"></i>
            </a>
            <a class="next" href="javascript:void(0)">
                <i class="mdi mdi-menu-right" style="font-size:55px; color:rgba(0,0,0,0.8);"></i>
            </a>
        </div>
    </div>
    <div style="clear:both;"></div>
</div>
<script>
    $(function(){

        jQuery('#ideas-data-carousel').slide({
            mainCell:".iteams ul",
            effect:"leftLoop",
            autoPlay:false
        });
        // __scroll.register($('#ideas-data-carousel div.iteams ul li'));
        // $('#ideas-data-carousel div.hd ul li').on('click', function(e){
        //     __scroll.register($('#ideas-data-carousel div.iteams ul li'));
        // })
        // $('#ideas-data-carousel a.prev').on('click', function(e){
        //     __scroll.register($('#ideas-data-carousel div.iteams ul li'));
        // })
        // $('#ideas-data-carousel a.next').on('click', function(e){
        //     __scroll.register($('#ideas-data-carousel div.iteams ul li'));
        // })
    })

</script>