<footer class="full-container section fp-auto-height" data-anchor="footer-section">
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-md-6">
            <div class="row">
                <div class="col-xs-4 col-md-3">
                    <h6>關於微課</h6>
                    <ul>
                        <li><a href="#" title="團隊介紹">團隊介紹</a></li>
                        <li><a href="#" title="聯絡我們">聯絡我們</a></li>
                    </ul>
                </div>
                <div class="col-xs-4 col-md-3">
                    <h6>合作方式</h6>
                    <ul>
                        <li><a href="#" title="開課服務">開課服務</a></li>
                        <li><a href="#" title="企業方案">企業方案</a></li>
                    </ul>
                </div>
                <div class="col-xs-4 col-md-4">
                    <h6>如何使用</h6>
                    <ul>
                        <li><a href="#" title="常見問題">常見問題</a></li>
                        <li><a href="#" title="使用規則與隱私權政策">使用規則與隱私權政策</a></li>
                    </ul>

                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6">

            <div class="social">
                <ul>
                    <li>
                        <a href="https://www.facebook.com/wakey.in" target="_blank" title="Wakey微課臉書">
                            <img src="./assets/images/facebook.svg"  alt="">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.instagram.com/wakey.in" target="_blank" title="Wakey微課 Instagram">
                            <img src="./assets/images/instagram.svg"  alt="">
                        </a>
                    </li>
                </ul>
            </div>
            <p class="copyright">&copy 2017 @Wakey 日行學旅股份有限公司 All rights reserve.</p>
        </div>
    </div>

</div>
</footer>

