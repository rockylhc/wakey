
<nav class="navbar navbar-default navbar-fixed-top">


    <div class="container">
        <ul class="pull-left hidden-xs hidden-sm course-dropdown">
            <li>
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">線上課程</a>
                <ul class="dropdown-menu">
                    <li><a href="#">有感生活</a></li>
                    <li><a href="#">多一分收入</a></li>
                    <li><a href="#">職場生存</a></li>
                </ul>

            </li>
            <li><a href="#">直播課程</a></li>
            <li><a href="#">師資總覽</a></li>
        </ul>
        <div class="navbar-header text-center">
            <button type="button" class="hamburger hamburger--collapse js-hamburger navbar-toggle collapsed" data-toggle="collapse" data-target="#mobile-navbar-menu" aria-expanded="false">
                <div class="hamburger-box">
                    <div class="hamburger-inner"></div>
                </div>
            </button>

            <a href="/" class="navbar-brand"><img src="./assets/images/logo.svg" alt=""></a>

            <a href="#" class="pull-right search-btn"><img src="./assets/images/search.svg" alt=""></a>

            <a href="#" class="pull-right user hidden-sm hidden-xs" data-target="#userModal" data-toggle="modal" data-backdrop="static">
                <img src="./assets/images/user.svg" alt="">
                <span class="hidden-xs hidden-sm">登入/註冊</span>
            </a>
            
        </div>
    </div>
    <div class="secondary inline-menu" >
        <ul class="nav navbar-nav" >
            <li data-menuanchor="trailer-section"><a href="#trailer-section">課程預告片</a></li><!--
            --><li data-menuanchor="lecturer-section"><a href="#lecturer-section">老師簡介</a></li><!--
            --><li data-menuanchor="courses-section"><a href="#courses-section">課程介紹</a></li><!--
            --><li data-menuanchor="videos-section"><a href="#videos-section">課程單元</a></li><!--
            --><li data-menuanchor="equipment-section"><a href="#equipment-section">適合族群</a></li><!--
            --><li data-menuanchor="comment-section"><a href="#comment-section">討論區</a></li><!--
            --><li data-menuanchor="rate-section"><a href="#rate-section">評價課程</a></li><!--
            --><li class="pull-right pull-right-last"><a href="#" class="price">$4,500</a></li><!--
            --><li class="pull-right"><a href="#" class="orange-nav prompt-login"  data-toggle="modal" data-target="#buyModal">立即購買</a></li><!--
            --><li class="pull-right m-l-m-15"><a href="#" class="grey-nav prompt-login">試聽</a></li>
        </ul>
    </div>
    <div class="container">

        <div class="secondary " id="sticky-menu">
            <ul class="container nav navbar-nav" >
                <li data-menuanchor="trailer-section"><a href="#trailer-section">課程預告片</a></li>
                <li data-menuanchor="lecturer-section"><a href="#lecturer-section">老師簡介</a></li>
                <li data-menuanchor="courses-section"><a href="#courses-section">課程介紹</a></li>
                <li data-menuanchor="videos-section"><a href="#videos-section">課程單元</a></li>
                <li data-menuanchor="equipment-section"><a href="#equipment-section">購買教材</a></li>
                <li data-menuanchor="comment-section"><a href="#comment-section">討論區</a></li>
                <li data-menuanchor="work-section"><a href="#work-section">成果發表</a></li>
                <li data-menuanchor="rate-section"><a href="#rate-section">評價課程</a></li>
                <li class="pull-right"><a href="#" class="grey-nav prompt-login">試聽</a></li>
                <li class="pull-right"><a href="#" class="orange-nav prompt-login">立即購買</a></li>
                <li class="price pull-right">$4,500</li>
            </ul>
        </div>
        <div class="collapse navbar-collapse" id="mobile-navbar-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        線上課程
                        <img class='open pull-right' src="./assets/images/menu-plus.svg" alt="">
                        <img class='close pull-right' src="./assets/images/menu-minus.svg" alt="">
                    </a>

                    <ul class="dropdown-menu">
                        <li><a href="#">有感生活</a></li>
                        <li><a href="#">多一分收入</a></li>
                        <li><a href="#">職場生存</a></li>
                    </ul>

                </li>
                <li><a href="#">直播課程</a></li>
                <li><a href="#">師資總覽</a></li>
            </ul>
        </div>
    </div>
</nav>
