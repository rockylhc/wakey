<!doctype html>
<html lang="zh-Hant">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>微課 Wakey</title>
    <meta name="description" content="">
    <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height" />

    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/iptools-jquery-offcanvas.css">
    <link rel="stylesheet" href="assets/css/dzslides.css">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/student-portal-style.css">

</head>
<body class="video-mode">

<header class="site-header idle-hide">
    <img src="../assets/images/white-logo.svg" alt="" id="logo">
    <span>動畫師私藏系列｜從轉場學會拆解 Motion Graphics</span>
</header>