
<nav class="navbar navbar-default navbar-fixed-top">


    <div class="container">
        <ul class="pull-left hidden-xs hidden-sm course-dropdown">
            <li>
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">線上課程</a>
                <ul class="dropdown-menu">
                    <li><a href="#">有感生活</a></li>
                    <li><a href="#">多一分收入</a></li>
                    <li><a href="#">職場生存</a></li>
                </ul>

            </li>
            <li><a href="#">直播課程</a></li>
            <li><a href="#">師資總覽</a></li>
        </ul>
        <div class="navbar-header text-center">
            <button type="button" class="hamburger hamburger--collapse js-hamburger navbar-toggle collapsed" data-toggle="collapse" data-target="#mobile-navbar-menu" aria-expanded="false">
                <div class="hamburger-box">
                    <div class="hamburger-inner"></div>
                </div>
            </button>

            <a href="/" class="navbar-brand"><img src="./assets/images/logo.svg" alt=""></a>

            <a href="#" class="pull-right search-btn"><img src="./assets/images/search.svg" alt=""></a>
            <a href="#" class="pull-right right-nav hidden-xs hidden-sm" id="show-cart" data-offcanvas-toggle="cart"><img src="./assets/images/cart.svg" alt=""></a>
            <a href="#" data-toggle="modal" data-target="#msgModal" class="pull-right right-nav hidden-xs hidden-sm"><img src="./assets/images/notice.svg" alt=""></a>
            <span class="dropdown pull-right user user-dropdown hidden-xs hidden-sm">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                    <img src="./assets/images/useravatar.png" class="circle user-avatar" alt="">
                    <span class="hidden-xs hidden-sm">用戶名</span>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="#">我的帳戶</a></li>
                    <li><a href="#">我的課程</a></li>
                    <li><a href="#">購買紀錄</a></li>
                    <li><a href="#">登出</a></li>
                </ul>
            </span>

        </div>

    </div>
    <div class="container">
       <div class="collapse navbar-collapse" id="mobile-navbar-menu">
           <ul class="nav navbar-nav">
               <li class="dropdown">
                   <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                       線上課程
                       <img class='open pull-right' src="./assets/images/menu-plus.svg" alt="">
                       <img class='close pull-right' src="./assets/images/menu-minus.svg" alt="">
                   </a>

                   <ul class="dropdown-menu">
                       <li><a href="#">有感生活</a></li>
                       <li><a href="#">多一分收入</a></li>
                       <li><a href="#">職場生存</a></li>
                   </ul>

               </li>
               <li><a href="#">直播課程</a></li>
               <li><a href="#">師資總覽</a></li>
           </ul>
       </div>
   </div>

</nav>
