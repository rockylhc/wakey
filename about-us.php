<?php include('templates/header.php'); ?>
<body>
<?php include('templates/nav-aboutus.php'); ?>
<div id="fullpage" class="about-us">
    <div class="full-container diagonal-bg section blue-on-white">
        <div class="container">
            <section class="row" id="first-row">
                <div class="col-md-6 col-lg-push-1 col-lg-5 banner-txt">
                    <h1 class="yellow-title">
                        為什麼每個人都需要學習?
                    </h1>
                    <p>
                        我們的社會一直在強調教育與學習的重要性，並善於知識理論的灌輸，但卻很少在意為何學習，<br>
                        除了考試，好像剩下的還是考試，它的確可能是種目的，卻不一定是我們所嚮往的目的地。
                    </p>
                    <p>
                        「如果今天人生突然到了盡頭，對這個世界你了解有多少？」
                    </p>
                    <p>
                        與其說是學習，不如說是探索，是一種方式向內去描繪自己，向外去勾勒世界。
                        學習，可以讓我們與知識有更深的互動，可以讓我們跟自己有更真實的對話，
                        學習不是迫於繁華，而是不甘平凡。
                    </p>
                    <p class="pinkish">往下看更多</p>
                </div>

                <div class="col-md-6 col-lg-push-1  overlap-img">
                    <img src="./assets/images/coffee-img.jpg" alt="">
                </div>
            </section>
        </div>
        <div class="container">
            <div class="row" id="second-row">
                <div class="col-sm-6 col-sm-push-6">
                    <h2 class="orangy-title">
                        有別於其他線上教育平台 我們做到了什麼?
                    </h2>
                    <h3 class="grey-caption">
                        突破環境限制，讓學習透過互動有趣加倍！
                    </h3>
                    <p>
                        Wakey不僅擁有高品質高水準的課程與老師，更有讓你大開眼界的主題內容，我們樂於挑戰各種不可能與嘗試新型的學習模式，努力打破線上與線下之間的距離隔閡，透過獨特的直播課程讓問與答直接正面碰撞，結合材料的物流配送讓手與腦交互應用。於是線上學習可以開始有不同以往的想像。
                    </p>
                </div>
                <div class="col-sm-6 col-sm-pull-6">
                    <div class="responsive-img-wrapper">
                        <img src="./assets/images/image-with-pattern.png" class="responsive-img" alt="">
                    </div>

                </div>

            </div>
        </div>
    </div>


    <div class="full-container go-register-banner">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h3 class="blue-title">一起加入微課學習大家庭吧! </h3>
                    <a href="register.php">前往註冊</a>
                </div>
            </div>
        </div>
    </div>

    <div class="full-contaner members">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <div class="content">
                        <h2 class="diagonal dull-yellow">團隊介紹</h2>
                        <p>WAKEY，由一群勇於嘗試與樂於探索的人們組成，各個專業分工卻也合作無間，辦公室裡什麼沒有只有滿天飛揚的鬼點子，<br>
                            我們想讓學習更有效率，我們想讓零碎創造價值，我們想讓生活有更多可能，我們想讓教育喚醒人們的靈魂。</p>
                    </div>
                </div>
            </div>
            <div class="row circles">
                <div class="col-xs-12 col-sm-6 col-md-3 member">
                    <div class="responsive-img-wrapper">
                    <img src="./assets/images/members/boss.gif" class="animate img-circle responsive-img">
                    <img src="./assets/images/members/boss.jpg" class="static img-circle responsive-img">
                        <div class="blue-pill">主理人</div>
                    </div>
                    <h3>楊仁豪</h3>
                    <p>
                        Wakey創辦者 <br>
                        喜歡勇者與英雄，但立志當個魔王；為了喚醒更多的靈魂可以不斷燃燒生命的中二大叔。
                    </p>
                </div>


                <div class="col-xs-12 col-sm-6 col-md-3 member">
                    <div class="responsive-img-wrapper">
                        <img src="./assets/images/members/program-manager.gif?v=2" class="animate img-circle responsive-img">
                        <img src="./assets/images/members/program-manager.jpg?v=2" class="static img-circle responsive-img">
                        <div class="blue-pill">企劃總監</div>
                    </div>
                    <h3>黃姍儀</h3>
                    <p>
                        擔任Wakey大姊大 <br>
                        負責課程開發進度管控、企劃提案與資源連結。曾任職於上海安博教育集團（上海寰宇利人教育培訓）副總經理。具有國民中學特殊教育專任教師、NGH國際催眠治療師、兒童發展師等專業資格。專長教材設計、教育訓練規劃與執行、專注力訓練、兒童行為觀察與分析。
                    </p>
                </div>

                <div class="col-xs-12 col-sm-6 col-md-3 member">
                    <div class="responsive-img-wrapper">
                        <img src="./assets/images/members/operating-director.gif" class="animate img-circle responsive-img">
                        <img src="./assets/images/members/operating-director.jpg" class="static img-circle responsive-img">
                        <div class="blue-pill">營運總監</div>
                    </div>
                    <h3>邱志豪</h3>
                    <p>
                        擔任Wakey大哥大 <br>
                        負責Wakey營運及商業模式。多次創業，小孟塔羅創辦人、泓斈心智圖創意學院創辦人。對於商業市場敏銳，擅長教育培訓與教學。
                    </p>
                </div>

                <div class="col-xs-12 col-sm-6 col-md-3 member">
                    <div class="responsive-img-wrapper">
                        <img src="./assets/images/members/creative-director.gif" class="animate img-circle responsive-img">
                        <img src="./assets/images/members/creative-director.jpg" class="static img-circle responsive-img">
                        <div class="blue-pill">創意總監</div>
                    </div>
                    <h3>盧友萱</h3>
                    <p>
                        擔任Wakey之魔力寶貝 <br>
                        除了睡覺、吃飯以外都在說話的霸氣總監！喉嚨裡是不是有裝麥克風阿？順帶一提品味不錯的總監隨便穿都不太會出差錯，典型愛美獅子座女生無誤。負責Wakey平面設計與創意策劃。善於表達與溝通，整合能力強，專長為品牌規劃與文案撰寫，熱愛設計，對於流行趨勢敏感，對美的事物充滿熱忱與執著。
                    </p>
                </div>

                <div class="col-xs-12 col-sm-6 col-md-3 member">
                    <div class="responsive-img-wrapper">
                        <img src="./assets/images/members/executive.gif" class="animate img-circle responsive-img">
                        <img src="./assets/images/members/executive.jpg" class="static img-circle responsive-img">
                        <div class="blue-pill">產品企劃</div>
                    </div>
                    <h3>黃昭渝</h3>
                    <p>
                        擔任Wakey之花 <br>
                        執著於流行美妝穿搭，堅持連上班都要一日一造型，信仰著白天書店晚上夜店的生活style。負責市場分析、客群分析及趨勢追蹤；社群平台經營與活動設計、課程開發、課程後製協助與公司內部行政事務。
                    </p>
                </div>

                <div class="col-xs-12 col-sm-6 col-md-3 member">
                    <div class="responsive-img-wrapper">
                        <img src="./assets/images/members/project-manager.gif" class="animate img-circle responsive-img">
                        <img src="./assets/images/members/project-manager.jpg" class="static img-circle responsive-img">
                        <div class="blue-pill">專案經理</div>
                    </div>
                    <h3>陳怡安</h3>
                    <p>
                        擔任Wakey之草 <br>
                        太陽、上升、月亮都是射手的男子，一顆容易融化的冰塊，易戀體質，熱愛健身與騎腳踏車，有著清爽好相處的暖男性格，永遠的男二，現正徵友中～負責課程開發、處理相關階段流程、平面攝影，對視覺影像有著極高的敏銳度。
                    </p>
                </div>

                <div class="col-xs-12 col-sm-6 col-md-3 member">
                    <div class="responsive-img-wrapper">
                        <img src="./assets/images/members/sales.gif" class="animate img-circle responsive-img">
                        <img src="./assets/images/members/sales.jpg" class="static img-circle responsive-img">
                        <div class="blue-pill">行銷戰鬥員</div>
                    </div>
                    <h3>江明遙</h3>
                    <p>
                        擔任Wakey行銷支援 <br>
                        樂於從做中學的慢熟牡羊男，拿了好幾年的小六法，才發現自己更喜歡實踐創意的過程。擅長以「諧音思考」作為一切企劃的開頭，目前主責行銷企劃。哦！對了！他還是一位食記部落客（嗝～）。
                    </p>
                </div>

                <div class="col-xs-12 col-sm-6 col-md-3 member">
                    <div class="responsive-img-wrapper">
                        <img src="./assets/images/members/intern.gif" class="animate img-circle responsive-img">
                        <img src="./assets/images/members/intern.jpg" class="static img-circle responsive-img">
                        <div class="blue-pill">被選召的孩子</div>
                    </div>
                    <h3>莊少維</h3>
                    <p>
                        擔任Wakey實習生 <br>
                        想要成為海賊王的男人，一無所有就只有一個鬼腦袋，喜歡跑步游泳還有投入大自然，總可以在那裡找到不少靈感，擅長活動企劃、文案撰寫、行銷策略、組織規劃，夢想是買下一座島和養一隻哈士奇。
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid bottom-container">
            <div class="row">
                <div class="col-md-6 col-lg-5 responsive-img-wrapper-bg">
                </div>
                <div class="col-md-6 col-lg-7">
                    <h1 class="yellow-title">
                        最有趣的線上平台!
                    </h1>
                    <p>不受時間地點限制，隨時隨地輕鬆學習或開課！</p>
                    <a href="./register.php">前往註冊</a>
                </div>
            </div>
    </div>

    <?php include('templates/footer.php'); ?>
</div>

<div id="cart" class="offcanvas sidenav empty">
    <div class="side-nav-wrap clearfix">
        <header>購物車<a href="#" class="pull-right" data-offcanvas-close="cart"><img src="./assets/images/close-cart.svg" alt=""></a></header>
        <ul class="cart-list">

        </ul>
        <a href="#" class="checkout">結帳</a>
        <p class="cart-hint">
            <img src="./assets/images/cart-hint.svg" alt="">
            哎呀！購物車還是空的 <br>
            快去課程教材瞧瞧
        </p>
        <a href="register.php">前往註冊</a>
    </div>
</div>


<div class="modal fade worksModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body container"></div>

        </div>
    </div>
</div>

<div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-alignment-center">
            <div class="modal-content">

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-alignment-center">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal" aria-hidden="true"><img src="./assets/images/close-search.svg" alt=""></a>
                <div class="modal-body">
                    <div class="videoWrapper">
                        <iframe width="100%" src=""></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade afterLoginModal" id="profileModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-alignment-center">
            <div class="modal-content">
                <div class="modal-header">
                   <h4 class="modal-title">用戶名</h4>
                    <a href="#" class="close" data-dismiss="modal" aria-hidden="true"><img src="./assets/images/close-search.svg" alt=""></a>
                </div>
                <div class="modal-body ">
                    <div class="row">
                        <div class="col-xs-12 large-padding">
                            <h3 class="text-center"><img src="https://source.eletang.com/photo_default.png" width="100" height="100" data-params="?imageMogr2/thumbnail/!100x100r/gravity/Center/crop/100x100/blur/1x0/quality/75|imageslim"></h3>
                            <h4>USERNAME</h4>
                            <p>找魚</p>
                            <h4>ROLE</h4>
                            <p>STUDENT</p>
                            <h4>EMAIL</h4>
                            <p>zxcvbn840416@gmail.com</p>
                            <h4>PHONE</h4>
                            <p>0963831736</p>
                            <h4>REMARKS</h4>
                            <p></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade afterLoginModal" id="msgModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-alignment-center">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title message-center">HAVE0MEG</h4>
                    <h4 class="messages">
                        <i class="fa fa-square-o" id="allCheck" style=""></i>
                        <span class="moreDel">MORE_DEL</span>
                    </h4>
                    <a href="#" class="close" data-dismiss="modal" aria-hidden="true"><img src="./assets/images/close-search.svg" alt=""></a>
                </div>
                <div class="modal-body message-center">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="bor selfmegContent selfmegContentXX">
                                <!--未读消息-->
                                <p class="readAll"><span>ALL_MEG</span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-body messages">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="selfmegContent allMegq selfmegContentXX1"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="buyModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-alignment-center">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">CONFIRM_ORDER</h4>
                    <a href="#" class="close" data-dismiss="modal" aria-hidden="true"><img class="fr close" src="//source.eletang.com/close_ion.png"></a>
                </div>
                <div class="modal-body ">
                    <div class="row">
                        <div class="col-xs-12 large-padding">
                            <div class="payInfo"><img src="https://doctrans-tw.eletang.com/doctrans-tw/114/114M15125272379898.jpg?x-oss-process=image/resize,m_mfit,w_80,h_80,limit_0/auto-orient,0/quality,q_90" style="width:auto; height:80px;"></div>
                            <div class="payCon choseInfo">
                                <h4 style="height:auto;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;">
                                    商品名稱
                                    <span>1206 test</span>
                                </h4>
                                <h4>訂單原價<span>500元</span></h4>
                                <h4>訂單優惠<select name="pType" id="seletVal"><option value="0">無優惠</option></select></h4>
                                <h4>實付款<span id="sp0" class="" style="margin-left: 46px;">500元</span></h4>
                                <span class="fl pay-font">備註</span>
                                <span class="fl">
                                    <textarea class="ear-style" style="margin-left: 56px;" rows="3" placeholder="備註"></textarea>
                                </span>
                                <button class="btn-pay">送出訂單</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="search-overlay">
    <a href="#" class="close-search"><img src="./assets/images/close-search.svg" alt=""></a>
    <div class="vertical-alignment-helper text-center">
        <div class="search-wrapper vertical-alignment-center">
            <form class="search-form">
                <input type="text" value="" class="search-text" placeholder="查詢關鍵字...">
                <input type="image" src="./assets/images/search.svg"  />
            </form>
        </div>
    </div>
</div>
<ul class="mobile-footer-nav">
    <li><a href="#">
            <img src="./assets/images/course-mobile.svg" alt="">
            我的課程
        </a>
    </li>
    <li><a href="#" data-offcanvas-toggle="cart">
            <img src="./assets/images/cart-mobile.svg" alt="">
            購物車
        </a>
    </li>
    <li class="dropup">
        <a href="#" data-toggle="dropdown">
            <img src="./assets/images/account-mobile.svg" alt="">
            我的帳戶
        </a>

        <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
            <li><a href="#">我的帳戶</a></li>
            <li><a href="#">購買紀錄</a></li>
            <li><a href="#">登出</a></li>
        </ul>

    </li>
</ul>
<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '298301200659355',
            cookie     : true,
            xfbml      : true,
            version    : 'v2.11'
        });

        FB.AppEvents.logPageView();

    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/zh_TW/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<script src="https://code.jquery.com/jquery-2.1.4.min.js"
        integrity="sha384-R4/ztc4ZlRqWjqIuvf6RX5yb/v90qNGx6fS48N0tRxiGkqveZETq72KgDVJCp2TC
sha256-8WqyJLuWKRBVhxXIL1jBDD7SDxU936oZkCnxQbWwJVw="
        crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="assets/js/jquery.js"><\/script>')</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script src="assets/js/snap.svg.min.js"></script>
<script src="assets/js/classie.min.js?v=<?php echo time(); ?>"></script>
<script src="assets/js/bootstrap.min.js?v=<?php echo time(); ?>"></script>
<script src="assets/js/iptools-jquery-offcanvas.js"></script>
<script src="assets/js/slick.min.js"></script>
<script src="assets/js/jquery.SuperSlide.2.1.1.js"></script>
<script src="assets/js/jquery.starRating.js?v=<?php echo time(); ?>"></script>
<script src="assets/js/jquery.easings.min.js"></script>
<script src="assets/js/scrolloverflow.min.js"></script>
<script src="assets/js/jquery.fullpage.min.js"></script>
<script src="assets/js/main.js?v=<?php echo time(); ?>"></script>
</body>
</html>