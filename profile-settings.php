<body class="profile-body">
<?php include('templates/profile-header.php'); ?>
<?php include('templates/nav-profile.php'); ?>
<div class="profile">
    <div class="full-container section profile-top">
        <img src="./assets/images/user-photo.jpg" class='img-circle' alt="">
        <div class="username">昭渝</div>
        <a href="#" class="logout">登出</a>
    </div>
</div>
<ul class="user-inventory">
    <li>
        <div class="amount">14</div>
        已學習的課程
    </li>
    <li>
        <div class="amount">14</div>
        收藏的課程
    </li>
    <li>
        <div class="amount">14</div>
        關注的導師
    </li>
</ul>

<div class="profile-section container">
    <div class="row">
        <div class="col-sm-4 col-md-push-1 col-md-3 col-lg-push-1 col-lg-2 sidebar">

            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" id="headingOne">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseAccount">
                                <img src="./assets/images/user-icon.svg" alt=""> 我的帳戶
                                <span class="glyphicon glyphicon-triangle-top pull-right"></span>
                            </a>
                        </h4>
                    </div>
                    <div id="collapseAccount" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <a href="/profile.php" >編輯個人資料</a>
                            <a href="#" class="active">帳號設定</a>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                        <h4 class="panel-title">
                            <a href="/profile-course.php">
                                <img src="./assets/images/purchased-icon.svg" alt=""> 購買記錄
                            </a>
                        </h4>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                        <h4 class="panel-title">
                            <a href="/my-courses.php">
                                <img src="./assets/images/book-icon.svg" alt=""> 我的課程
                            </a>
                        </h4>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-sm-8 col-md-push-1 col-md-8 col-lg-push-1 col-lg-8 form-wrapper">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse"  href="#collapsePassword">
                            密碼修改 <img src="./assets/images/pencil.svg" alt="">
                        </a>
                    </h4>
                </div>
                <div id="collapsePassword" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <form class="change-password">
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="changeOldPassword">舊密碼</label>
                                    <input type="password" class="form-control" id="changeOldPassword" name="changeOldPassword">
                                </div>
                                <div class="col-md-6 validation-col"></div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="changeNewPassword">新密碼</label>
                                    <input type="password" class="form-control" id="changeNewPassword" name="changeNewPassword">
                                </div>
                                <div class="col-md-6 validation-col"></div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="changeConfirmPassword">確認密碼</label>
                                    <input type="password" class="form-control" id="changeConfirmPassword" name="changeConfirmPassword">
                                </div>
                                <div class="col-md-6 validation-col"></div>
                            </div>
                            <a href="#" class="reset-form">取消</a>
                            <button type="submit" class="blue-button">完成</button>
                        </form>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" href="#collapseEmail">
                            E-mail修改 <img src="./assets/images/pencil.svg" alt="">
                        </a>
                    </h4>
                </div>
                <div id="collapseEmail" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <form class="change-email">
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="emailOld">舊EMAIL</label>
                                    <input type="email" class="form-control" id="emailOld" name="emailOld">
                                </div>
                                <div class="col-md-6 validation-col"></div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="emailCode">輸入EMAIL驗證碼</label>
                                    <input type="text" class="form-control" id="emailCode" name="emailCode">
                                </div>
                                <div class="col-md-6">
                                    <a href="#" class="blue-button sendcode">
                                        <span class="original">免費獲取驗證碼</span>
                                        <span class="pending" style="display:none;">重新發送(<span class="second"></span>)</span>
                                    </a>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="emailNew">新EMAIL</label>
                                    <input type="email" class="form-control" id="emailNew" name="emailNew">
                                </div>
                                <div class="col-md-6 validation-col"></div>
                            </div>
                            <a href="#" class="reset-form">取消</a>
                            <button type="submit" class="blue-button">完成</button>
                        </form>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" href="#collapsePhone">
                            手機號碼修改 <img src="./assets/images/pencil.svg" alt="">
                        </a>
                    </h4>
                </div>
                <div id="collapsePhone" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <form class="change-phone">
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="phoneOld">舊手機號碼</label>
                                    <input type="text" class="form-control" id="phoneOld" name="phoneOld">
                                </div>
                                <div class="col-md-6 validation-col"></div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="phoneCode">輸入手機驗證碼</label>
                                    <input type="text" class="form-control" id="phoneCode" name="phoneCode">
                                </div>
                                <div class="col-md-6">
                                    <a href="#" class="blue-button sendcode">
                                        <span class="original">免費獲取驗證碼</span>
                                        <span class="pending" style="display:none;">重新發送(<span class="second"></span>)</span>
                                    </a>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="phoneNew">新手機號碼</label>
                                    <input type="text" class="form-control" id="phoneNew" name="phoneNew">
                                </div>
                                <div class="col-md-6 validation-col"></div>
                            </div>
                            <a href="#" class="reset-form">取消</a>
                            <button type="submit" class="blue-button">完成</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="successModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-alignment-center">
            <div class="modal-content text-center">
                <div class="modal-body">
                    <img src="./assets/images/save-success.svg" alt="">
                    <h3>修改成功</h3>
                    <a href="#" data-dismiss="modal" class="blue-button">OK!</a>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include('templates/footer.php'); ?>

<div class="search-overlay">
    <a href="#" class="close-search"><img src="./assets/images/close-search.svg" alt=""></a>
    <div class="vertical-alignment-helper text-center">
        <div class="search-wrapper vertical-alignment-center">
            <form class="search-form">
                <input type="text" value="" class="search-text" placeholder="查詢關鍵字...">
                <input type="image" src="./assets/images/search.svg"  />
            </form>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-2.1.4.min.js"
        integrity="sha384-R4/ztc4ZlRqWjqIuvf6RX5yb/v90qNGx6fS48N0tRxiGkqveZETq72KgDVJCp2TC
sha256-8WqyJLuWKRBVhxXIL1jBDD7SDxU936oZkCnxQbWwJVw="
        crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="assets/js/jquery.js"><\/script>')</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script src="assets/js/bootstrap.min.js?v=<?php echo time(); ?>"></script>
<script src="assets/js/account.js?v=<?php echo time(); ?>"></script>
</body>
</html>