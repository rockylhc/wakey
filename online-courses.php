<body class="profile-body">
<?php include('templates/profile-header.php'); ?>
<?php include('templates/nav-profile.php'); ?>

<div class="full-container section sub-courses-top">
    <div class="container">
        <div class="inline-menu">
            <ul class="nav navbar-nav" >
                <li class="active"><a href="#">全部課程</a></li><!--
            --><li><a href="#">學習中課程</a></li><!--
            --><li><a href="#">已學習課程</a></li><!--
            --><li><a href="free-course.php">免費課程</a></li><!--
            --><li class="pull-right">
                    <a href="collection.php" class=""><img src="./assets/images/course-love-inactive.svg" alt=""> 收藏課程</a>
                </li><!--
            --><li class="pull-right m-l-m-15 pull-right-last">
                    <a href="lecturers.php" class=""><img src="./assets/images/course-lecturer-inactive.svg" alt=""> 關注老師</a>
                </li>
            </ul>
        </div>
    </div>
</div>



<div class="courses-section container">
    <h2>全部課程</h2>
    <div class="row">
        <ul class="pagination col-xs-8 col-sm-5 col-md-4 col-lg-3 text-center">
            <li class="active"><a href="online-courses.php">線上課程</a></li>
            <li><a href="course-live-stream.php">直播課程</a></li>
        </ul>
    </div>
    <ul class="row news-list">
        <?php for ($x = 0; $x <= 12; $x++) { ?>
        <li class="col-xs-12 col-md-6 col-lg-3">
            <a href="#" class="tag">有感生活</a>
            <div class="area">
                <div class="thumb"><a href="#"><img src="./assets/images/post-header.jpg" alt=""></a></div>
                <div class="content">
                    <h3 class="title">在家打造你的咖啡館-遇見我的夏日咖啡 單元1</h3>
                    <div class="row">
                        <div class="col-xs-3 light-grey">募資預購</div>
                        <div class="col-xs-7">
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" style="width: 80%;"></div>
                            </div>
                        </div>
                        <div class="col-xs-2 bright-blue">75%</div>
                    </div>

                    <div class="desc">
                        <div class="post-left">
                            <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg">
                        </div>
                        <div class="post-center">
                            好棒花藝<br>
                            Stella Liu
                        </div>
                        <div class="post-right">
                            <div class="price">$4,500</div>
                        </div>
                    </div>
                    <a href="#" class="yellow-button">閱讀更多</a>
                </div>
            </div>
        </li>
        <?php } ?>
    </ul>

    <div class="row text-center">
        <nav aria-label="Page navigation" class="pagination-wrapper">
            <ul class="pagination">
                <li>
                    <a href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
                <li><a href="#" class="active">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li>
                    <a href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
            </ul>
        </nav>
    </div>

</div>

<?php include('templates/footer.php'); ?>

<div class="search-overlay">
    <a href="#" class="close-search"><img src="./assets/images/close-search.svg" alt=""></a>
    <div class="vertical-alignment-helper text-center">
        <div class="search-wrapper vertical-alignment-center">
            <form class="search-form">
                <input type="text" value="" class="search-text" placeholder="查詢關鍵字...">
                <input type="image" src="./assets/images/search.svg"  />
            </form>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-2.1.4.min.js"
        integrity="sha384-R4/ztc4ZlRqWjqIuvf6RX5yb/v90qNGx6fS48N0tRxiGkqveZETq72KgDVJCp2TC
sha256-8WqyJLuWKRBVhxXIL1jBDD7SDxU936oZkCnxQbWwJVw="
        crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="assets/js/jquery.js"><\/script>')</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script src="assets/js/bootstrap.min.js?v=<?php echo time(); ?>"></script>
<script src="assets/js/jquery.twzipcode.min.js"></script>
<script src="assets/js/account.js?v=<?php echo time(); ?>"></script>
</body>
</html>