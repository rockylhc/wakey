<?php include('templates/header-student-portal.php'); ?>


<div class="videoContainer">
    <div class="cover"><img src="./assets/images/slides/main.jpg" alt=""></div>
    <video id="video" class="tutorial-video" playsinline webkit-playsinline  preload="auto"  controlslist="nodownload" autoplay="autoplay" poster="" data-setup="{}" style="width: 100%; height: 100%;"></video>
</div>

<div class="slide-wrapper" id="slide1">
    <section style="background-image:url(./assets/images/slides/slide1.jpg)">

    </section>

    <section style="background-image:url(./assets/images/slides/slide2.jpg)">

    </section>

    <section style="background-image:url(./assets/images/slides/slide3.jpg)">

    </section>

    <section style="background-image:url(./assets/images/slides/slide4.jpg)">

    </section>
</div>
<div class="slide-wrapper" id="slide2">
    <section style="background-image:url(./assets/images/slides/slideb1.jpg)">

    </section>

    <section style="background-image:url(./assets/images/slides/slideb2.jpg)">

    </section>

    <section style="background-image:url(./assets/images/slides/slideb3.jpg)">

    </section>

</div>
<div class="slide-wrapper" id="slide3">
    <section style="background-image:url(./assets/images/slides/slidec1.jpg)">

    </section>

    <section style="background-image:url(./assets/images/slides/slidec2.jpg)">

    </section>

</div>


<ul id="sticky-icon" class="idle-hide">
    <li class="video-visible">
        <a href="#" class="nav-toggle" data-offcanvas-toggle="video-sidenav" id="toggle-video-nav" data-toggle="tooltip" data-placement="left" title="影片單元">
            <img src="./assets/images/category.svg" class="with-hover" alt="">
        </a>
    </li>
    <li class="slide-visible">
        <a href="#" class="nav-toggle" data-offcanvas-toggle="slide-sidenav" id="toggle-slide-nav" data-toggle="tooltip" data-placement="left" title="選擇講義">
            <img src="./assets/images/presentation.svg" class="with-hover" alt="">
        </a>
    </li>
    <li class="video-visible">
        <a href="#" id="show-slide" data-toggle="tooltip" data-placement="left" title="切換講義">
            <img src="./assets/images/board.svg" class="with-hover" alt="">
        </a>
    </li>
    <li class="slide-visible">
        <a href="#" id="show-video" data-toggle="tooltip" data-placement="left" title="切換影片">
            <img src="./assets/images/player.svg" class="with-hover" alt="">
        </a>
    </li>
    <li id="controls" class="slide-visible">
        <a href="#" id="back">
            <img src="./assets/images/presentation-arrow.svg" alt="">
        </a>
        <p id="count-control"><span id="current"></span>/<span id="count"></span></p>
        <a href="#" id="next">
            <img src="./assets/images/presentation-arrow.svg" alt="">
        </a>
    </li>
</ul>
<div id="video-sidenav" class="offcanvas sidenav idle-hide">
    <div class="side-nav-wrap">
        <header><a href="#" class="show-cover">課程目錄</a></header>
        <h5>單元1：認識咖啡豆</h5>
        <ul>
            <li class="active">
                <a href="#" data-vid="./assets/videos/rabbits.mp4">
                    異國咖啡豆學問大不同
                    <img src="./assets/images/play_20x20.svg" class="pull-right">
                    <span class="duration">1:30:00</span>
                </a>
            </li>
            <li>
                <a href="#" data-vid="./assets/videos/small.mp4">
                    如何品嘗咖啡豆辨別香...
                    <img src="./assets/images/play_20x20.svg" class="pull-right">
                    <span class="duration">1:30:00</span>
                </a>
            </li>
        </ul>
        <h5>單元2：咖啡小學堂</h5>
        <ul>
        <li>
            <a href="#" data-vid="./assets/videos/test.mp4">
                初學者怎樣購買烘焙豆？
                <img src="./assets/images/play_20x20.svg" class="pull-right">
                <span class="duration">1:30:00</span>
            </a>
        </li>
        <li>
            <a href="#" class='disabled'>
                為了一杯難忘的甘醇‎
                <img src="./assets/images/play_20x20.svg" class="pull-right">
                <span class="duration">1:30:00</span>
            </a>
        </li>
        <li>
            <a href="#" class='disabled'>
                為了一杯難忘的甘醇‎
                <img src="./assets/images/play_20x20.svg" class="pull-right">
                <span class="duration">1:30:00</span>
            </a>
        </li>
    </ul>
    </div>
</div>
<div id="slide-sidenav" class="offcanvas sidenav">
    <div class="side-nav-wrap">


        <header>教學講義</header>
        <ul class="handout">
            <li class="active">
                <a href="#" data-slide="slide1">
                    <img src="./assets/images/doc.svg">
                    咖啡學堂小知識
                </a>
            </li>
            <li>
                <a href="#" data-slide="slide2">
                    <img src="./assets/images/doc.svg">
                    如何辨別豆子的差異性

                </a>
            </li>
            <li>
                <a href="#" class='disabled' data-slide="slide3">
                    <img src="./assets/images/doc.svg">
                    咖啡學堂小知識

                </a>
            </li>
            <li>
        </ul>

    </div>
</div>


<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-3.2.1.min.js"><\/script>')</script>
<script src="https://cdn.jsdelivr.net/npm/fastclick@1.0.6/lib/fastclick.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/dzslides.js"></script>
<script src="assets/js/iptools-jquery-offcanvas.js"></script>
<script src="assets/js/jquery.idle.min.js"></script>
<script src="assets/js/iphone-inline-video.js"></script>
<script src="assets/js/student-portal.js"></script>

<script>
    window.ga=function(){ga.q.push(arguments)};ga.q=[];ga.l=+new Date;
    ga('create','UA-XXXXX-Y','auto');ga('send','pageview')
</script>
<script src="https://www.google-analytics.com/analytics.js" async defer></script>
