<body class="homepage-body">
<?php include('templates/homepage-header.php'); ?>
<?php include('templates/nav-homepage.php'); ?>
<div class="homepage">
    <div class="full-container section masthead">
        <div id="masthead-bg">
            <img src="assets/images/masthead-1.jpg" alt="">
            <img src="https://picsum.photos/1200/800?image=989" alt="">
            <img src="https://picsum.photos/1200/800?image=1048" alt="">
        </div>
        <div class="container">
            <img src="./assets/images/big-logo.svg" alt="">
            <h1>Wake up！Learning online, offline, close to your life</h1>
            <h2>拉近線上與線下距離，讓線上學習貼近每個人的真實生活</h2>
        </div>
        <p class="scroll" ><img src="./assets/images/scroll.svg" alt="" style="position:relative;left:1px;"><span></span></p>
    </div>
    <div class="full-container section intro" data-anchor="intro-section">
        <div class="container">
            <img src="./assets/images/big-logo.svg" alt="">
            <h1>「Wakey Wakey」是帶有點俏皮的「醒來吧」的意思。</h1>
            <p>
                我們以Wakey的理念延伸，輕聲喚醒沉睡的人們，希望讓人從內而發的甦醒，因為醒著才能有意識的學習，<br>
                進而深刻的省思以看見萬物的真實。微課取用了Wakey的諧音，也正好符合線上學習可利用零碎時間片段學習的概念，<br>
                相對於實體課程時間短、便利、即時，也象徵著我們雖然微小，但即將帶來改變。我們秉持著「拉近線上與線下距離」的初衷，<br>
                讓線上學習充滿更多可能，結合「互動式直播」和「材料購買」，將各種有趣的課程，以超乎舊有想像的方式帶到大家的生活中～
            </p>
        </div>
    </div>
    <div class="full-container section white-bg service">
        <div class="container">
            <div class="row">
                <h2 class="diagonal yellow">Wakey特色</h2>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="icon-wrapper">
                        <img src="./assets/images/service-1.svg" alt="">
                    </div>

                    <h3>結合材料配送與電子商務</h3>
                    <p>課程材料的配送與上架功能是老師最好的教學輔助，並且使學習變得更加容易，學生不用再自己煩惱該如何準備材料與器具，這樣貼心的細節更拉近線上與線下距離，也讓線上學習增加更多可能性</p>
                </div>
                <div class="col-md-4">
                    <div class="icon-wrapper">
                        <img src="./assets/images/service-2.svg" alt="">
                    </div>
                    <h3>功能最完整的教學直播</h3>
                    <p>Wakey的互動直播系統是專為學習客製，讓即時互動變得更簡單有趣，功能包含直播管理系統，預購與報名機制、金流收付整合、電子商務整合、線上講義觀看、傳送與下載，獎勵問答機制等功能</p>
                </div>
                <div class="col-md-4">
                    <div class="icon-wrapper">
                        <img src="./assets/images/service-3.svg" alt="">
                    </div>
                    <h3>別具巧思的課程企劃 </h3>
                    <p>每一門課程都是客製化。Wakey企畫團隊致力於優質課程的線上化，我們提供老師完整的開課協助，並且喜歡挑戰各種可能性與學習模式，從學習的本質為初衷，希望讓學生看見不一樣的線上課程</p>
                </div>
            </div>
        </div>
    </div>

    <div class="full-container section short-intro orange-pattern">
        <div class="container">
            <div class="row">
                <div class="intro-left col-md-5">
                    <img src="./assets/images/course-orange-banner.png" alt="">
                    <p class="title-txt">
                        線上課程
                        <span class="open-sans">Online Course</span>
                    </p>
                </div>
                <div class="intro-right col-md-7">
                    每一門線上課程都是Wakey的用心企劃<br>
                    隨選隨看打破時間與空間距離<br>
                    搭配線上講義、語音回覆、材料購買<br>
                    各種細緻的功能讓線上學習充滿更多可能
                    <div class="button-wrapper">
                        <a href="#" class="primary btn">更多課程</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="container first-slide" >
            <div class="row">

                <ul class="news-list clearfix homepage-slide">
                    <li class="col-xs-12 col-md-3">
                        <a href="#" class="tag">有感生活</a>
                        <div class="area">
                            <div class="thumb"><a href="#"><img src="./assets/images/post-header.jpg" alt=""></a></div>
                            <div class="content">
                                <h3 class="title">在家打造你的咖啡館-遇見我的夏日咖啡 單元1</h3>
                                <div class="desc">
                                    <div class="post-left">
                                        <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg">
                                    </div>
                                    <div class="post-center">
                                        Seki <br>
                                         如是創意咖啡品牌創始人
                                    </div>
                                    <div class="post-right">
                                        <div class="price">$4,900</div>
                                    </div>
                                </div>
                                <a href="#" class="yellow-button">閱讀更多</a>
                            </div>
                        </div>
                    </li>
                    <li class="col-xs-12 col-md-3">
                        <a href="#" class="tag">有感生活</a>
                        <div class="area">
                            <div class="thumb"><a href="#"><img src="./assets/images/post-header.jpg" alt=""></a></div>
                            <div class="content">
                                <h3 class="title">在家打造你的咖啡館-遇見我的夏日咖啡 單元1</h3>
                                <div class="desc">
                                    <div class="post-left">
                                        <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg">
                                    </div>
                                    <div class="post-center">
                                        Seki <br>
                                         如是創意咖啡品牌創始人
                                    </div>
                                    <div class="post-right">
                                        <div class="price">$4,900</div>
                                    </div>
                                </div>
                                <a href="#" class="yellow-button">閱讀更多</a>
                            </div>
                        </div>
                    </li>
                    <li class="col-xs-12 col-md-3">
                        <a href="#" class="tag">有感生活</a>
                        <div class="area">
                            <div class="thumb"><a href="#"><img src="./assets/images/post-header.jpg" alt=""></a></div>
                            <div class="content">
                                <h3 class="title">在家打造你的咖啡館-遇見我的夏日咖啡 單元1</h3>
                                <div class="desc">
                                    <div class="post-left">
                                        <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg">
                                    </div>
                                    <div class="post-center">
                                        Seki <br>
                                         如是創意咖啡品牌創始人
                                    </div>
                                    <div class="post-right">
                                        <div class="price">$4,900</div>
                                    </div>
                                </div>
                                <a href="#" class="yellow-button">閱讀更多</a>
                            </div>
                        </div>
                    </li>
                    <li class="col-xs-12 col-md-3">
                        <a href="#" class="tag">有感生活</a>
                        <div class="area">
                            <div class="thumb"><a href="#"><img src="./assets/images/post-header.jpg" alt=""></a></div>
                            <div class="content">
                                <h3 class="title">在家打造你的咖啡館-遇見我的夏日咖啡 單元1</h3>
                                <div class="desc">
                                    <div class="post-left">
                                        <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg">
                                    </div>
                                    <div class="post-center">
                                        Seki <br>
                                         如是創意咖啡品牌創始人
                                    </div>
                                    <div class="post-right">
                                        <div class="price">$4,900</div>
                                    </div>
                                </div>
                                <a href="#" class="yellow-button">閱讀更多</a>
                            </div>
                        </div>
                    </li>

                    <li class="col-xs-12 col-md-3">
                        <a href="#" class="tag">有感生活</a>
                        <div class="area">
                            <div class="thumb"><a href="#"><img src="./assets/images/post-header.jpg" alt=""></a></div>
                            <div class="content">
                                <h3 class="title">在家打造你的咖啡館-遇見我的夏日咖啡 單元1</h3>
                                <div class="desc">
                                    <div class="post-left">
                                        <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg">
                                    </div>
                                    <div class="post-center">
                                        Seki <br>
                                         如是創意咖啡品牌創始人
                                    </div>
                                    <div class="post-right">
                                        <div class="price">$4,900</div>
                                    </div>
                                </div>
                                <a href="#" class="yellow-button">閱讀更多</a>
                            </div>
                        </div>
                    </li>
                    <li class="col-xs-12 col-md-3">
                        <a href="#" class="tag">有感生活</a>
                        <div class="area">
                            <div class="thumb"><a href="#"><img src="./assets/images/post-header.jpg" alt=""></a></div>
                            <div class="content">
                                <h3 class="title">在家打造你的咖啡館-遇見我的夏日咖啡 單元1</h3>
                                <div class="desc">
                                    <div class="post-left">
                                        <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg">
                                    </div>
                                    <div class="post-center">
                                        Seki <br>
                                         如是創意咖啡品牌創始人
                                    </div>
                                    <div class="post-right">
                                        <div class="price">$4,900</div>
                                    </div>
                                </div>
                                <a href="#" class="yellow-button">閱讀更多</a>
                            </div>
                        </div>
                    </li>
                    <li class="col-xs-12 col-md-3">
                        <a href="#" class="tag">有感生活</a>
                        <div class="area">
                            <div class="thumb"><a href="#"><img src="./assets/images/post-header.jpg" alt=""></a></div>
                            <div class="content">
                                <h3 class="title">在家打造你的咖啡館-遇見我的夏日咖啡 單元1</h3>
                                <div class="desc">
                                    <div class="post-left">
                                        <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg">
                                    </div>
                                    <div class="post-center">
                                        Seki <br>
                                         如是創意咖啡品牌創始人
                                    </div>
                                    <div class="post-right">
                                        <div class="price">$4,900</div>
                                    </div>
                                </div>
                                <a href="#" class="yellow-button">閱讀更多</a>
                            </div>
                        </div>
                    </li>
                    <li class="col-xs-12 col-md-3">
                        <a href="#" class="tag">有感生活</a>
                        <div class="area">
                            <div class="thumb"><a href="#"><img src="./assets/images/post-header.jpg" alt=""></a></div>
                            <div class="content">
                                <h3 class="title">在家打造你的咖啡館-遇見我的夏日咖啡 單元1</h3>
                                <div class="desc">
                                    <div class="post-left">
                                        <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg">
                                    </div>
                                    <div class="post-center">
                                        Seki <br>
                                         如是創意咖啡品牌創始人
                                    </div>
                                    <div class="post-right">
                                        <div class="price">$4,900</div>
                                    </div>
                                </div>
                                <a href="#" class="yellow-button">閱讀更多</a>
                            </div>
                        </div>
                    </li>

                    <li class="col-xs-12 col-md-3">
                        <a href="#" class="tag">有感生活</a>
                        <div class="area">
                            <div class="thumb"><a href="#"><img src="./assets/images/post-header.jpg" alt=""></a></div>
                            <div class="content">
                                <h3 class="title">在家打造你的咖啡館-遇見我的夏日咖啡 單元1</h3>
                                <div class="desc">
                                    <div class="post-left">
                                        <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg">
                                    </div>
                                    <div class="post-center">
                                        Seki <br>
                                         如是創意咖啡品牌創始人
                                    </div>
                                    <div class="post-right">
                                        <div class="price">$4,900</div>
                                    </div>
                                </div>
                                <a href="#" class="yellow-button">閱讀更多</a>
                            </div>
                        </div>
                    </li>
                    <li class="col-xs-12 col-md-3">
                        <a href="#" class="tag">有感生活</a>
                        <div class="area">
                            <div class="thumb"><a href="#"><img src="./assets/images/post-header.jpg" alt=""></a></div>
                            <div class="content">
                                <h3 class="title">在家打造你的咖啡館-遇見我的夏日咖啡 單元1</h3>
                                <div class="desc">
                                    <div class="post-left">
                                        <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg">
                                    </div>
                                    <div class="post-center">
                                        Seki <br>
                                         如是創意咖啡品牌創始人
                                    </div>
                                    <div class="post-right">
                                        <div class="price">$4,900</div>
                                    </div>
                                </div>
                                <a href="#" class="yellow-button">閱讀更多</a>
                            </div>
                        </div>
                    </li>
                    <li class="col-xs-12 col-md-3">
                        <a href="#" class="tag">有感生活</a>
                        <div class="area">
                            <div class="thumb"><a href="#"><img src="./assets/images/post-header.jpg" alt=""></a></div>
                            <div class="content">
                                <h3 class="title">在家打造你的咖啡館-遇見我的夏日咖啡 單元1</h3>
                                <div class="desc">
                                    <div class="post-left">
                                        <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg">
                                    </div>
                                    <div class="post-center">
                                        Seki <br>
                                         如是創意咖啡品牌創始人
                                    </div>
                                    <div class="post-right">
                                        <div class="price">$4,900</div>
                                    </div>
                                </div>
                                <a href="#" class="yellow-button">閱讀更多</a>
                            </div>
                        </div>
                    </li>
                    <li class="col-xs-12 col-md-3">
                        <a href="#" class="tag">有感生活</a>
                        <div class="area">
                            <div class="thumb"><a href="#"><img src="./assets/images/post-header.jpg" alt=""></a></div>
                            <div class="content">
                                <h3 class="title">在家打造你的咖啡館-遇見我的夏日咖啡 單元1</h3>
                                <div class="desc">
                                    <div class="post-left">
                                        <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg">
                                    </div>
                                    <div class="post-center">
                                        Seki <br>
                                         如是創意咖啡品牌創始人
                                    </div>
                                    <div class="post-right">
                                        <div class="price">$4,900</div>
                                    </div>
                                </div>
                                <a href="#" class="yellow-button">閱讀更多</a>
                            </div>
                        </div>
                    </li>

                    <li class="col-xs-12 col-md-3">
                        <a href="#" class="tag">有感生活</a>
                        <div class="area">
                            <div class="thumb"><a href="#"><img src="./assets/images/post-header.jpg" alt=""></a></div>
                            <div class="content">
                                <h3 class="title">在家打造你的咖啡館-遇見我的夏日咖啡 單元1</h3>
                                <div class="desc">
                                    <div class="post-left">
                                        <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg">
                                    </div>
                                    <div class="post-center">
                                        Seki <br>
                                         如是創意咖啡品牌創始人
                                    </div>
                                    <div class="post-right">
                                        <div class="price">$4,900</div>
                                    </div>
                                </div>
                                <a href="#" class="yellow-button">閱讀更多</a>
                            </div>
                        </div>
                    </li>
                    <li class="col-xs-12 col-md-3">
                        <a href="#" class="tag">有感生活</a>
                        <div class="area">
                            <div class="thumb"><a href="#"><img src="./assets/images/post-header.jpg" alt=""></a></div>
                            <div class="content">
                                <h3 class="title">在家打造你的咖啡館-遇見我的夏日咖啡 單元1</h3>
                                <div class="desc">
                                    <div class="post-left">
                                        <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg">
                                    </div>
                                    <div class="post-center">
                                        Seki <br>
                                         如是創意咖啡品牌創始人
                                    </div>
                                    <div class="post-right">
                                        <div class="price">$4,900</div>
                                    </div>
                                </div>
                                <a href="#" class="yellow-button">閱讀更多</a>
                            </div>
                        </div>
                    </li>
                    <li class="col-xs-12 col-md-3">
                        <a href="#" class="tag">有感生活</a>
                        <div class="area">
                            <div class="thumb"><a href="#"><img src="./assets/images/post-header.jpg" alt=""></a></div>
                            <div class="content">
                                <h3 class="title">在家打造你的咖啡館-遇見我的夏日咖啡 單元1</h3>
                                <div class="desc">
                                    <div class="post-left">
                                        <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg">
                                    </div>
                                    <div class="post-center">
                                        Seki <br>
                                         如是創意咖啡品牌創始人
                                    </div>
                                    <div class="post-right">
                                        <div class="price">$4,900</div>
                                    </div>
                                </div>
                                <a href="#" class="yellow-button">閱讀更多</a>
                            </div>
                        </div>
                    </li>
                    <li class="col-xs-12 col-md-3">
                        <a href="#" class="tag">有感生活</a>
                        <div class="area">
                            <div class="thumb"><a href="#"><img src="./assets/images/post-header.jpg" alt=""></a></div>
                            <div class="content">
                                <h3 class="title">在家打造你的咖啡館-遇見我的夏日咖啡 單元1</h3>
                                <div class="desc">
                                    <div class="post-left">
                                        <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg">
                                    </div>
                                    <div class="post-center">
                                        Seki <br>
                                         如是創意咖啡品牌創始人
                                    </div>
                                    <div class="post-right">
                                        <div class="price">$4,900</div>
                                    </div>
                                </div>
                                <a href="#" class="yellow-button">閱讀更多</a>
                            </div>
                        </div>
                    </li>

                    <li class="col-xs-12 col-md-3">
                        <a href="#" class="tag">有感生活</a>
                        <div class="area">
                            <div class="thumb"><a href="#"><img src="./assets/images/post-header.jpg" alt=""></a></div>
                            <div class="content">
                                <h3 class="title">在家打造你的咖啡館-遇見我的夏日咖啡 單元1</h3>
                                <div class="desc">
                                    <div class="post-left">
                                        <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg">
                                    </div>
                                    <div class="post-center">
                                        Seki <br>
                                         如是創意咖啡品牌創始人
                                    </div>
                                    <div class="post-right">
                                        <div class="price">$4,900</div>
                                    </div>
                                </div>
                                <a href="#" class="yellow-button">閱讀更多</a>
                            </div>
                        </div>
                    </li>
                    <li class="col-xs-12 col-md-3">
                        <a href="#" class="tag">有感生活</a>
                        <div class="area">
                            <div class="thumb"><a href="#"><img src="./assets/images/post-header.jpg" alt=""></a></div>
                            <div class="content">
                                <h3 class="title">在家打造你的咖啡館-遇見我的夏日咖啡 單元1</h3>
                                <div class="desc">
                                    <div class="post-left">
                                        <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg">
                                    </div>
                                    <div class="post-center">
                                        Seki <br>
                                         如是創意咖啡品牌創始人
                                    </div>
                                    <div class="post-right">
                                        <div class="price">$4,900</div>
                                    </div>
                                </div>
                                <a href="#" class="yellow-button">閱讀更多</a>
                            </div>
                        </div>
                    </li>
                    <li class="col-xs-12 col-md-3">
                        <a href="#" class="tag">有感生活</a>
                        <div class="area">
                            <div class="thumb"><a href="#"><img src="./assets/images/post-header.jpg" alt=""></a></div>
                            <div class="content">
                                <h3 class="title">在家打造你的咖啡館-遇見我的夏日咖啡 單元1</h3>
                                <div class="desc">
                                    <div class="post-left">
                                        <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg">
                                    </div>
                                    <div class="post-center">
                                        Seki <br>
                                         如是創意咖啡品牌創始人
                                    </div>
                                    <div class="post-right">
                                        <div class="price">$4,900</div>
                                    </div>
                                </div>
                                <a href="#" class="yellow-button">閱讀更多</a>
                            </div>
                        </div>
                    </li>
                    <li class="col-xs-12 col-md-3">
                        <a href="#" class="tag">有感生活</a>
                        <div class="area">
                            <div class="thumb"><a href="#"><img src="./assets/images/post-header.jpg" alt=""></a></div>
                            <div class="content">
                                <h3 class="title">在家打造你的咖啡館-遇見我的夏日咖啡 單元1</h3>
                                <div class="desc">
                                    <div class="post-left">
                                        <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg">
                                    </div>
                                    <div class="post-center">
                                        Seki <br>
                                         如是創意咖啡品牌創始人
                                    </div>
                                    <div class="post-right">
                                        <div class="price">$4,900</div>
                                    </div>
                                </div>
                                <a href="#" class="yellow-button">閱讀更多</a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="full-container section short-intro second-intro blue-short-pattern">
        <div class="container">
            <div class="row">

                <div class="intro-right col-md-5 col-md-push-6">
                    <img src="./assets/images/live-stream-banner.png" alt="">
                    <p class="title-txt">
                        直播課程
                        <span class="open-sans">Live Course</span>
                    </p>
                </div>
                <div class="intro-left col-md-6 col-md-pull-5">
                    Wakey的直播課程提供最完整的互動系統 <br>
                    整合管理系統、即時互動、金流收付與問答獎勵等功能<br>
                    實現線上學習最大效益拉近線上與線下距離
                    <div class="button-wrapper">
                        <a href="#" class="blue-btn btn">更多課程</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="container second-slide">
            <div class="row">
                <ul class="news-list clearfix homepage-slide">
                    <li class="col-xs-12 col-md-3">
                        <a href="#" class="tag">有感生活</a>
                        <div class="area">
                            <div class="thumb">
                                <a href="#"><img src="./assets/images/post-header.jpg" alt=""></a>
                                <span class="live-tag">直播中</span>
                            </div>
                            <div class="content">
                                <h3 class="title">在家打造你的咖啡館-遇見我的夏日咖啡 單元1</h3>
                                <div class="row">
                                    <div class="col-xs-3 light-grey">募資預購</div>
                                    <div class="col-xs-7">
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" style="width: 80%;"></div>
                                        </div>
                                    </div>
                                    <div class="col-xs-2 bright-blue">75%</div>
                                </div>

                                <div class="desc">
                                    <div class="post-left">
                                        <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg">
                                    </div>
                                    <div class="post-center">
                                        好棒花藝<br>
                                        Stella Liu
                                    </div>
                                    <div class="post-right">
                                        <div class="strikethrough">$5,100</div>
                                        <div class="price">$4,900</div>
                                    </div>
                                </div>
                                <a href="#" class="yellow-button">閱讀更多</a>
                            </div>
                        </div>
                    </li>
                    <li class="col-xs-12 col-md-3">
                        <a href="#" class="tag">有感生活</a>
                        <div class="area">
                            <div class="thumb"><a href="#"><img src="./assets/images/post-header.jpg" alt=""></a></div>
                            <div class="content">
                                <h3 class="title">在家打造你的咖啡館-遇見我的夏日咖啡 單元1</h3>
                                <div class="row">
                                    <div class="col-xs-3 light-grey">募資預購</div>
                                    <div class="col-xs-7">
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" style="width: 80%;"></div>
                                        </div>
                                    </div>
                                    <div class="col-xs-2 bright-blue">75%</div>
                                </div>
                                <div class="desc">
                                    <div class="post-left">
                                        <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg">
                                    </div>
                                    <div class="post-center">
                                        好棒花藝<br>
                                        Stella Liu
                                    </div>
                                    <div class="post-right">
                                        <div class="strikethrough">$5,100</div>
                                        <div class="price">$4,900</div>
                                    </div>
                                </div>
                                <a href="#" class="yellow-button">閱讀更多</a>
                            </div>
                        </div>
                    </li>
                    <li class="col-xs-12 col-md-3">
                        <a href="#" class="tag">有感生活</a>
                        <div class="area">
                            <div class="thumb"><a href="#"><img src="./assets/images/post-header.jpg" alt=""></a></div>
                            <div class="content">
                                <h3 class="title">在家打造你的咖啡館-遇見我的夏日咖啡 單元1</h3>
                                <div class="row">
                                    <div class="col-xs-3 light-grey">募資預購</div>
                                    <div class="col-xs-7">
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" style="width: 80%;"></div>
                                        </div>
                                    </div>
                                    <div class="col-xs-2 bright-blue">75%</div>
                                </div>
                                <div class="desc">
                                    <div class="post-left">
                                        <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg">
                                    </div>
                                    <div class="post-center">
                                        好棒花藝<br>
                                        Stella Liu
                                    </div>
                                    <div class="post-right">
                                        <div class="strikethrough">$5,100</div>
                                        <div class="price">$4,900</div>
                                    </div>
                                </div>
                                <a href="#" class="yellow-button">閱讀更多</a>
                            </div>
                        </div>
                    </li>
                    <li class="col-xs-12 col-md-3">
                        <a href="#" class="tag">有感生活</a>
                        <div class="area">
                            <div class="thumb"><a href="#"><img src="./assets/images/post-header.jpg" alt=""></a></div>
                            <div class="content">
                                <h3 class="title">在家打造你的咖啡館-遇見我的夏日咖啡 單元1</h3>
                                <div class="row">
                                    <div class="col-xs-3 light-grey">募資預購</div>
                                    <div class="col-xs-7">
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" style="width: 80%;"></div>
                                        </div>
                                    </div>
                                    <div class="col-xs-2 bright-blue">75%</div>
                                </div>
                                <div class="desc">
                                    <div class="post-left">
                                        <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg">
                                    </div>
                                    <div class="post-center">
                                        好棒花藝<br>
                                        Stella Liu
                                    </div>
                                    <div class="post-right">
                                        <div class="strikethrough">$5,100</div>
                                        <div class="price">$4,900</div>
                                    </div>
                                </div>
                                <a href="#" class="yellow-button">閱讀更多</a>
                            </div>
                        </div>
                    </li>

                    <li class="col-xs-12 col-md-3">
                        <a href="#" class="tag">有感生活</a>
                        <div class="area">
                            <div class="thumb"><a href="#"><img src="./assets/images/post-header.jpg" alt=""></a></div>
                            <div class="content">
                                <h3 class="title">在家打造你的咖啡館-遇見我的夏日咖啡 單元1</h3>
                                <div class="row">
                                    <div class="col-xs-3 light-grey">募資預購</div>
                                    <div class="col-xs-7">
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" style="width: 80%;"></div>
                                        </div>
                                    </div>
                                    <div class="col-xs-2 bright-blue">75%</div>
                                </div>
                                <div class="desc">
                                    <div class="post-left">
                                        <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg">
                                    </div>
                                    <div class="post-center">
                                        好棒花藝<br>
                                        Stella Liu
                                    </div>
                                    <div class="post-right">
                                        <div class="strikethrough">$5,100</div>
                                        <div class="price">$4,900</div>
                                    </div>
                                </div>
                                <a href="#" class="yellow-button">閱讀更多</a>
                            </div>
                        </div>
                    </li>
                    <li class="col-xs-12 col-md-3">
                        <a href="#" class="tag">有感生活</a>
                        <div class="area">
                            <div class="thumb"><a href="#"><img src="./assets/images/post-header.jpg" alt=""></a></div>
                            <div class="content">
                                <h3 class="title">在家打造你的咖啡館-遇見我的夏日咖啡 單元1</h3>
                                <div class="row">
                                    <div class="col-xs-3 light-grey">募資預購</div>
                                    <div class="col-xs-7">
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" style="width: 80%;"></div>
                                        </div>
                                    </div>
                                    <div class="col-xs-2 bright-blue">75%</div>
                                </div>
                                <div class="desc">
                                    <div class="post-left">
                                        <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg">
                                    </div>
                                    <div class="post-center">
                                        好棒花藝<br>
                                        Stella Liu
                                    </div>
                                    <div class="post-right">
                                        <div class="strikethrough">$5,100</div>
                                        <div class="price">$4,900</div>
                                    </div>
                                </div>
                                <a href="#" class="yellow-button">閱讀更多</a>
                            </div>
                        </div>
                    </li>
                    <li class="col-xs-12 col-md-3">
                        <a href="#" class="tag">有感生活</a>
                        <div class="area">
                            <div class="thumb"><a href="#"><img src="./assets/images/post-header.jpg" alt=""></a></div>
                            <div class="content">
                                <h3 class="title">在家打造你的咖啡館-遇見我的夏日咖啡 單元1</h3>
                                <div class="row">
                                    <div class="col-xs-3 light-grey">募資預購</div>
                                    <div class="col-xs-7">
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" style="width: 80%;"></div>
                                        </div>
                                    </div>
                                    <div class="col-xs-2 bright-blue">75%</div>
                                </div>
                                <div class="desc">
                                    <div class="post-left">
                                        <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg">
                                    </div>
                                    <div class="post-center">
                                        好棒花藝<br>
                                        Stella Liu
                                    </div>
                                    <div class="post-right">
                                        <div class="strikethrough">$5,100</div>
                                        <div class="price">$4,900</div>
                                    </div>
                                </div>
                                <a href="#" class="yellow-button">閱讀更多</a>
                            </div>
                        </div>
                    </li>
                    <li class="col-xs-12 col-md-3">
                        <a href="#" class="tag">有感生活</a>
                        <div class="area">
                            <div class="thumb"><a href="#"><img src="./assets/images/post-header.jpg" alt=""></a></div>
                            <div class="content">
                                <h3 class="title">在家打造你的咖啡館-遇見我的夏日咖啡 單元1</h3>
                                <div class="row">
                                    <div class="col-xs-3 light-grey">募資預購</div>
                                    <div class="col-xs-7">
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" style="width: 80%;"></div>
                                        </div>
                                    </div>
                                    <div class="col-xs-2 bright-blue">75%</div>
                                </div>
                                <div class="desc">
                                    <div class="post-left">
                                        <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg">
                                    </div>
                                    <div class="post-center">
                                        好棒花藝<br>
                                        Stella Liu
                                    </div>
                                    <div class="post-right">
                                        <div class="strikethrough">$5,100</div>
                                        <div class="price">$4,900</div>
                                    </div>
                                </div>
                                <a href="#" class="yellow-button">閱讀更多</a>
                            </div>
                        </div>
                    </li>

                    <li class="col-xs-12 col-md-3">
                        <a href="#" class="tag">有感生活</a>
                        <div class="area">
                            <div class="thumb"><a href="#"><img src="./assets/images/post-header.jpg" alt=""></a></div>
                            <div class="content">
                                <h3 class="title">在家打造你的咖啡館-遇見我的夏日咖啡 單元1</h3>
                                <div class="row">
                                    <div class="col-xs-3 light-grey">募資預購</div>
                                    <div class="col-xs-7">
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" style="width: 80%;"></div>
                                        </div>
                                    </div>
                                    <div class="col-xs-2 bright-blue">75%</div>
                                </div>
                                <div class="desc">
                                    <div class="post-left">
                                        <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg">
                                    </div>
                                    <div class="post-center">
                                        好棒花藝<br>
                                        Stella Liu
                                    </div>
                                    <div class="post-right">
                                        <div class="strikethrough">$5,100</div>
                                        <div class="price">$4,900</div>
                                    </div>
                                </div>
                                <a href="#" class="yellow-button">閱讀更多</a>
                            </div>
                        </div>
                    </li>
                    <li class="col-xs-12 col-md-3">
                        <a href="#" class="tag">有感生活</a>
                        <div class="area">
                            <div class="thumb"><a href="#"><img src="./assets/images/post-header.jpg" alt=""></a></div>
                            <div class="content">
                                <h3 class="title">在家打造你的咖啡館-遇見我的夏日咖啡 單元1</h3>
                                <div class="row">
                                    <div class="col-xs-3 light-grey">募資預購</div>
                                    <div class="col-xs-7">
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" style="width: 80%;"></div>
                                        </div>
                                    </div>
                                    <div class="col-xs-2 bright-blue">75%</div>
                                </div>
                                <div class="desc">
                                    <div class="post-left">
                                        <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg">
                                    </div>
                                    <div class="post-center">
                                        好棒花藝 <br>
                                        Stella Liu
                                    </div>
                                    <div class="post-right">
                                        <div class="strikethrough">$5,100</div>
                                        <div class="price">$4,900</div>
                                    </div>
                                </div>
                                <a href="#" class="yellow-button">閱讀更多</a>
                            </div>
                        </div>
                    </li>
                    <li class="col-xs-12 col-md-3">
                        <a href="#" class="tag">有感生活</a>
                        <div class="area">
                            <div class="thumb"><a href="#"><img src="./assets/images/post-header.jpg" alt=""></a></div>
                            <div class="content">
                                <h3 class="title">在家打造你的咖啡館-遇見我的夏日咖啡 單元1</h3>
                                <div class="row">
                                    <div class="col-xs-3 light-grey">募資預購</div>
                                    <div class="col-xs-7">
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" style="width: 80%;"></div>
                                        </div>
                                    </div>
                                    <div class="col-xs-2 bright-blue">75%</div>
                                </div>
                                <div class="desc">
                                    <div class="post-left">
                                        <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg">
                                    </div>
                                    <div class="post-center">
                                        好棒花藝<br>
                                        Stella Liu
                                    </div>
                                    <div class="post-right">
                                        <div class="strikethrough">$5,100</div>
                                        <div class="price">$4,900</div>
                                    </div>
                                </div>
                                <a href="#" class="yellow-button">閱讀更多</a>
                            </div>
                        </div>
                    </li>
                    <li class="col-xs-12 col-md-3">
                        <a href="#" class="tag">有感生活</a>
                        <div class="area">
                            <div class="thumb"><a href="#"><img src="./assets/images/post-header.jpg" alt=""></a></div>
                            <div class="content">
                                <h3 class="title">在家打造你的咖啡館-遇見我的夏日咖啡 單元1</h3>
                                <div class="row">
                                    <div class="col-xs-3 light-grey">募資預購</div>
                                    <div class="col-xs-7">
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" style="width: 80%;"></div>
                                        </div>
                                    </div>
                                    <div class="col-xs-2 bright-blue">75%</div>
                                </div>
                                <div class="desc">
                                    <div class="post-left">
                                        <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg">
                                    </div>
                                    <div class="post-center">
                                        好棒花藝<br>
                                        Stella Liu
                                    </div>
                                    <div class="post-right">
                                        <div class="strikethrough">$5,100</div>
                                        <div class="price">$4,900</div>
                                    </div>
                                </div>
                                <a href="#" class="yellow-button">閱讀更多</a>
                            </div>
                        </div>
                    </li>

                    <li class="col-xs-12 col-md-3">
                        <a href="#" class="tag">有感生活</a>
                        <div class="area">
                            <div class="thumb"><a href="#"><img src="./assets/images/post-header.jpg" alt=""></a></div>
                            <div class="content">
                                <h3 class="title">在家打造你的咖啡館-遇見我的夏日咖啡 單元1</h3>
                                <div class="row">
                                    <div class="col-xs-3 light-grey">募資預購</div>
                                    <div class="col-xs-7">
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" style="width: 80%;"></div>
                                        </div>
                                    </div>
                                    <div class="col-xs-2 bright-blue">75%</div>
                                </div>
                                <div class="desc">
                                    <div class="post-left">
                                        <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg">
                                    </div>
                                    <div class="post-center">
                                        好棒花藝<br>
                                        Stella Liu
                                    </div>
                                    <div class="post-right">
                                        <div class="strikethrough">$5,100</div>
                                        <div class="price">$4,900</div>
                                    </div>
                                </div>
                                <a href="#" class="yellow-button">閱讀更多</a>
                            </div>
                        </div>
                    </li>
                    <li class="col-xs-12 col-md-3">
                        <a href="#" class="tag">有感生活</a>
                        <div class="area">
                            <div class="thumb"><a href="#"><img src="./assets/images/post-header.jpg" alt=""></a></div>
                            <div class="content">
                                <h3 class="title">在家打造你的咖啡館-遇見我的夏日咖啡 單元1</h3>
                                <div class="row">
                                    <div class="col-xs-3 light-grey">募資預購</div>
                                    <div class="col-xs-7">
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" style="width: 80%;"></div>
                                        </div>
                                    </div>
                                    <div class="col-xs-2 bright-blue">75%</div>
                                </div>
                                <div class="desc">
                                    <div class="post-left">
                                        <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg">
                                    </div>
                                    <div class="post-center">
                                        好棒花藝<br>
                                        Stella Liu
                                    </div>
                                    <div class="post-right">
                                        <div class="strikethrough">$5,100</div>
                                        <div class="price">$4,900</div>
                                    </div>
                                </div>
                                <a href="#" class="yellow-button">閱讀更多</a>
                            </div>
                        </div>
                    </li>
                    <li class="col-xs-12 col-md-3">
                        <a href="#" class="tag">有感生活</a>
                        <div class="area">
                            <div class="thumb"><a href="#"><img src="./assets/images/post-header.jpg" alt=""></a></div>
                            <div class="content">
                                <h3 class="title">在家打造你的咖啡館-遇見我的夏日咖啡 單元1</h3>
                                <div class="row">
                                    <div class="col-xs-3 light-grey">募資預購</div>
                                    <div class="col-xs-7">
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" style="width: 80%;"></div>
                                        </div>
                                    </div>
                                    <div class="col-xs-2 bright-blue">75%</div>
                                </div>
                                <div class="desc">
                                    <div class="post-left">
                                        <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg">
                                    </div>
                                    <div class="post-center">
                                        好棒花藝<br>
                                        Stella Liu
                                    </div>
                                    <div class="post-right">
                                        <div class="strikethrough">$5,100</div>
                                        <div class="price">$4,900</div>
                                    </div>
                                </div>
                                <a href="#" class="yellow-button">閱讀更多</a>
                            </div>
                        </div>
                    </li>
                    <li class="col-xs-12 col-md-3">
                        <a href="#" class="tag">有感生活</a>
                        <div class="area">
                            <div class="thumb"><a href="#"><img src="./assets/images/post-header.jpg" alt=""></a></div>
                            <div class="content">
                                <h3 class="title">在家打造你的咖啡館-遇見我的夏日咖啡 單元1</h3>
                                <div class="row">
                                    <div class="col-xs-3 light-grey">募資預購</div>
                                    <div class="col-xs-7">
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" style="width: 80%;"></div>
                                        </div>
                                    </div>
                                    <div class="col-xs-2 bright-blue">75%</div>
                                </div>
                                <div class="desc">
                                    <div class="post-left">
                                        <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg">
                                    </div>
                                    <div class="post-center">
                                        好棒花藝<br>
                                        Stella Liu
                                    </div>
                                    <div class="post-right">
                                        <div class="strikethrough">$5,100</div>
                                        <div class="price">$4,900</div>
                                    </div>
                                </div>
                                <a href="#" class="yellow-button">閱讀更多</a>
                            </div>
                        </div>
                    </li>

                    <li class="col-xs-12 col-md-3">
                        <a href="#" class="tag">有感生活</a>
                        <div class="area">
                            <div class="thumb"><a href="#"><img src="./assets/images/post-header.jpg" alt=""></a></div>
                            <div class="content">
                                <h3 class="title">在家打造你的咖啡館-遇見我的夏日咖啡 單元1</h3>
                                <div class="row">
                                    <div class="col-xs-3 light-grey">募資預購</div>
                                    <div class="col-xs-7">
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" style="width: 80%;"></div>
                                        </div>
                                    </div>
                                    <div class="col-xs-2 bright-blue">75%</div>
                                </div>
                                <div class="desc">
                                    <div class="post-left">
                                        <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg">
                                    </div>
                                    <div class="post-center">
                                        好棒花藝<br>
                                        Stella Liu
                                    </div>
                                    <div class="post-right">
                                        <div class="strikethrough">$5,100</div>
                                        <div class="price">$4,900</div>
                                    </div>
                                </div>
                                <a href="#" class="yellow-button">閱讀更多</a>
                            </div>
                        </div>
                    </li>
                    <li class="col-xs-12 col-md-3">
                        <a href="#" class="tag">有感生活</a>
                        <div class="area">
                            <div class="thumb"><a href="#"><img src="./assets/images/post-header.jpg" alt=""></a></div>
                            <div class="content">
                                <h3 class="title">在家打造你的咖啡館-遇見我的夏日咖啡 單元1</h3>
                                <div class="row">
                                    <div class="col-xs-3 light-grey">募資預購</div>
                                    <div class="col-xs-7">
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" style="width: 80%;"></div>
                                        </div>
                                    </div>
                                    <div class="col-xs-2 bright-blue">75%</div>
                                </div>
                                <div class="desc">
                                    <div class="post-left">
                                        <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg">
                                    </div>
                                    <div class="post-center">
                                        好棒花藝<br>
                                        Stella Liu
                                    </div>
                                    <div class="post-right">
                                        <div class="strikethrough">$5,100</div>
                                        <div class="price">$4,900</div>
                                    </div>
                                </div>
                                <a href="#" class="yellow-button">閱讀更多</a>
                            </div>
                        </div>
                    </li>
                    <li class="col-xs-12 col-md-3">
                        <a href="#" class="tag">有感生活</a>
                        <div class="area">
                            <div class="thumb"><a href="#"><img src="./assets/images/post-header.jpg" alt=""></a></div>
                            <div class="content">
                                <h3 class="title">在家打造你的咖啡館-遇見我的夏日咖啡 單元1</h3>
                                <div class="row">
                                    <div class="col-xs-3 light-grey">募資預購</div>
                                    <div class="col-xs-7">
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" style="width: 80%;"></div>
                                        </div>
                                    </div>
                                    <div class="col-xs-2 bright-blue">75%</div>
                                </div>
                                <div class="desc">
                                    <div class="post-left">
                                        <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg">
                                    </div>
                                    <div class="post-center">
                                        好棒花藝<br>
                                        Stella Liu
                                    </div>
                                    <div class="post-right">
                                        <div class="strikethrough">$5,100</div>
                                        <div class="price">$4,900</div>
                                    </div>
                                </div>
                                <a href="#" class="yellow-button">閱讀更多</a>
                            </div>
                        </div>
                    </li>
                    <li class="col-xs-12 col-md-3">
                        <a href="#" class="tag">有感生活</a>
                        <div class="area">
                            <div class="thumb"><a href="#"><img src="./assets/images/post-header.jpg" alt=""></a></div>
                            <div class="content">
                                <h3 class="title">在家打造你的咖啡館-遇見我的夏日咖啡 單元1</h3>
                                <div class="row">
                                    <div class="col-xs-3 light-grey">募資預購</div>
                                    <div class="col-xs-7">
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" style="width: 80%;"></div>
                                        </div>
                                    </div>
                                    <div class="col-xs-2 bright-blue">75%</div>
                                </div>
                                <div class="desc">
                                    <div class="post-left">
                                        <img class="media-object img-circle" src="./assets/images/avatar/avatar1.jpg">
                                    </div>
                                    <div class="post-center">
                                        好棒花藝<br>
                                        Stella Liu
                                    </div>
                                    <div class="post-right">
                                        <div class="strikethrough">$5,100</div>
                                        <div class="price">$4,900</div>
                                    </div>
                                </div>
                                <a href="#" class="yellow-button">閱讀更多</a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="full-container section white-bg green-banner">
        <div class="container">
            <div class="row">
                <div class="col-md-4 banner-txt">
                    <h1 class="title-txt">
                        超優師資
                        <span class="open-sans">Our Teachers</span>

                    </h1>
                    <p>
                        Wakey用心尋找具有熱情的老師，<br>
                        並且燃燒自己協助老師規劃課程，<br>
                        期望讓學員隔著時空的距離也能感受到老師們澎湃的靈魂。
                    </p>
                    <a href="#" class="white-btn">更多老師</a>
                </div>
                <div class="col-md-8">

                    <div class="news-list lecturer-slide">
                        <div class="lecturer-card">

                            <div class="area content thumb">
                                <img src="./assets/images/lecturer1.jpg" alt="">

                                <a href="#">
                                </a>
                            </div>
                            <div class="lecture-info">

                                <div class="name">Grace蔡佳穎</div>
                            </div>
                        </div>
                        <div class="lecturer-card">

                            <div class="area content thumb">
                                <img src="./assets/images/lecturer2.jpg" alt="">

                                <a href="#">
                                </a>
                            </div>
                            <div class="lecture-info">

                                <div class="name">Grace蔡佳穎</div>
                            </div>
                        </div>
                        <div class="lecturer-card">

                            <div class="area content thumb">
                                <img src="./assets/images/lecturer3.jpg" alt="">

                                <a href="#">
                                </a>
                            </div>
                            <div class="lecture-info">

                                <div class="name">Grace蔡佳穎</div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="full-container section white-bg">
        <div class="container">
            <div class="row">
                <h2 class="diagonal grey">學員分享</h2>
            </div>

        </div>
    </div>
    <div class="full-container section white-bg testimonial">
        <div class="container-fluid">
            <div class="row bg-row">
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-xs-3">
                            <img src="./assets/images/student-avatar.png" alt="" class="student-avatar">
                        </div>
                        <div class="col-xs-9">
                            <p>
                                有別於台灣一般線上教育教學平台，
                                擁有優質師資和創新的課程內容！
                            </p>
                            <p class="student-name">Ryan</p>
                            <div class="student-education">
                                國立大學- 資工系
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-xs-3">
                            <img src="./assets/images/student-avatar.png" alt="" class="student-avatar">
                        </div>
                        <div class="col-xs-9">
                            <p>
                                影片拍得很優質！
                                微課真的很用心，是我目前線上學習平台最愛之一！
                            </p>
                            <p class="student-name">Adam</p>
                            <div class="student-education">
                                金融服務業
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-xs-3">
                            <img src="./assets/images/student-avatar.png" alt="" class="student-avatar">
                        </div>
                        <div class="col-xs-9">
                            <p>
                                出社會工作後很少能花大量的時間跨領域進修，感謝微課網站推出多元的課程教學
                                讓我在工作之餘能大量吸收知識！
                            </p>
                            <p class="student-name">Joe</p>
                            <div class="student-education">
                                設計總監
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include('templates/footer.php'); ?>
</div>
<ul class="mobile-footer-nav">
    <li><a href="#">
            <img src="./assets/images/course-mobile.svg" alt="">
            我的課程
        </a>
    </li>
    <li><a href="#" data-offcanvas-toggle="cart">
            <img src="./assets/images/cart-mobile.svg" alt="">
            購物車
        </a>
    </li>
    <li class="dropup">
        <a href="#" data-toggle="dropdown">
            <img src="./assets/images/account-mobile.svg" alt="">
            我的帳戶
        </a>

        <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
            <li><a href="#">我的帳戶</a></li>
            <li><a href="#">購買紀錄</a></li>
            <li><a href="#">登出</a></li>
        </ul>

    </li>
</ul>

<div class="modal fade user-modal" id="userModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-alignment-center">
            <div class="modal-content">
                <div class="modal-body">
                    <a href="#" class="close" data-dismiss="modal" aria-hidden="true"><img src="./assets/images/close-login.svg" alt=""></a>
                    <div class="row">
                        <div class="col-sm-6">

                            <div class="login-side">
                                <div class="login-wrapper">
                                    <h2>歡迎來到Wakey</h2>
                                    <p>微課讓線上學習更貼近每個人的真實生活！<br />
                                        利用片段時間學習，累積屬於你的價值！
                                    </p>
                                </div>

                                <div class="password-recovery-wrapper" style="display:none">
                                    <h2>找回密碼 <a href="#" class="close close-recovery" aria-hidden="true"><img src="./assets/images/close-search.svg" alt=""></a></h2>
                                    <form class="password-recovery-form">
                                        <h4>別擔心，我們將會協助您取回密碼。</h4>
                                        <div class="form-group clearfix">
                                            <div class="col-xs-12 col-sm-7 ">
                                                <input type="text" class="form-control" id="recoveryPhone" name="recoveryPhone" placeholder="手機號碼">
                                            </div>
                                            <div class="col-xs-12 col-sm-5  no-pad-left-on-sm">
                                                <a href="#" class="blue-button sendcode">
                                                    <span class="original">發送驗證碼</span>
                                                    <span class="pending" style="display:none;">重新發送(<span class="second"></span>)</span>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="form-group clearfix">
                                            <div class="col-sm-12">
                                                <input type="text" class="form-control" id="recoveryCode" name="recoveryCode" placeholder="輸入驗證碼">
                                            </div>
                                        </div>

                                        <div class="form-group clearfix">
                                            <div class="col-sm-12">
                                                <input type="password" class="form-control" id="recoveryPassword" name="recoveryPassword" placeholder="新密碼">
                                            </div>

                                        </div>

                                        <div class="form-group clearfix">
                                            <div class="col-sm-12">
                                                <input type="password" class="form-control" id="recoveryConfirmPassword" name="recoveryConfirmPassword" placeholder="確認密碼">
                                            </div>
                                        </div>
                                        <button type="submit" class="blue-button">送出</button>
                                    </form>
                                </div>

                            </div>

                        </div>
                        <div class="col-sm-6">
                            <div class="login-form-wrapper">
                                <h2>登入</h2>
                                <form class="login-form">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="loginID" name="loginID" placeholder="手機或電子信箱">
                                    </div>

                                    <div class="form-group">
                                        <input type="password" class="form-control" id="loginPw" name="loginPw" placeholder="密碼">
                                    </div>

                                    <button type="submit" class="blue-button pull-right">登入</button>
                                    <a href="#" class="forget-pw">忘記密碼？</a>
                                </form>
                                <a href="#" class="facebook-login"><img src="./assets/images/facebook-login.svg" alt=""></a>
                                <a href="#" class="google-login"><img src="./assets/images/google-login.svg" alt=""></a>
                                <a href="register.php" class="go-register pull-right">快速註冊</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="search-overlay">
    <a href="#" class="close-search"><img src="./assets/images/close-search.svg" alt=""></a>
    <div class="vertical-alignment-helper text-center">
        <div class="search-wrapper vertical-alignment-center">
            <form class="search-form">
                <input type="text" value="" class="search-text" placeholder="查詢關鍵字...">
                <input type="image" src="./assets/images/search.svg"  />
            </form>
        </div>
    </div>
</div>
<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '298301200659355',
            cookie     : true,
            xfbml      : true,
            version    : 'v2.11'
        });

        FB.AppEvents.logPageView();

    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/zh_TW/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<script src="https://apis.google.com/js/platform.js" async defer></script>
<script src="https://code.jquery.com/jquery-2.1.4.min.js"
        integrity="sha384-R4/ztc4ZlRqWjqIuvf6RX5yb/v90qNGx6fS48N0tRxiGkqveZETq72KgDVJCp2TC
sha256-8WqyJLuWKRBVhxXIL1jBDD7SDxU936oZkCnxQbWwJVw="
        crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="assets/js/jquery.js"><\/script>')</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script src="assets/js/snap.svg.min.js"></script>
<script src="assets/js/classie.min.js?v=<?php echo time(); ?>"></script>
<script src="assets/js/bootstrap.min.js?v=<?php echo time(); ?>"></script>
<script src="assets/js/slick.min.js"></script>
<script src="assets/js/jquery.velocity.js"></script>
<script src="assets/js/jquery.kenburnsy.js"></script>
<script src="assets/js/jquery.SuperSlide.2.1.1.js"></script>
<script src="assets/js/jquery.starRating.js?v=<?php echo time(); ?>"></script>
<script src="assets/js/jquery.easings.min.js"></script>
<script src="assets/js/scrolloverflow.min.js"></script>
<script src="assets/js/jquery.fullpage.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2//2.0.0-beta.2.4/owl.carousel.min.js"></script>
<script src="assets/js/main.js?v=<?php echo time(); ?>"></script>
</body>
</html>